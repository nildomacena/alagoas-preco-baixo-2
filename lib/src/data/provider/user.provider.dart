import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:meta/meta.dart';
import 'package:get/get.dart';

class UserProvider {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;
  List<Estabelecimento> estabelecimentosFavoritos = [];
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final Rx<FirebaseUser> _firebaseUser = Rx<FirebaseUser>();
  final RxBool _admin = RxBool();
  FirebaseUser user;
  UserProvider() {
    _firebaseUser.bindStream(auth.onAuthStateChanged);
    _admin.bindStream(_firebaseUser.map((data) =>
        data != null &&
        data.uid != null &&
        (data.uid == '93PvSjyaDKgPZ2W136z2DIdak372' ||
            data.uid == 'CGqrO2wdOlOLFxpTq368pf2WIPJ3' ||
            data.uid == 'CJGNdnjYC4gbL0zsSIBZcvDnS9h1')));
    auth.currentUser().then((FirebaseUser value) {
      user = value;
    });
    listFirebaseUser();
  }
  Stream<FirebaseUser> get user$ => auth.onAuthStateChanged;
  FirebaseUser get firebaseUser => _firebaseUser.value;

  listFirebaseUser() {
    _firebaseUser.listen((user) {
      print('FirebaseUser: $user');
    });
  }

  Future<void> updateUserInfo() async {
    if (user == null) user = await auth.currentUser();
    QuerySnapshot querySnapshot = await _firestore
        .collection('users')
        .document(user.uid)
        .collection('estabelecimentosFavoritos')
        .getDocuments();
    estabelecimentosFavoritos = querySnapshot.documents
        .map((d) => Estabelecimento.fromFirestore(d))
        .toList();
  }

  bool isAdmin() {
    return _admin.value;
  }

  Future<FirebaseUser> googleSignIn() async {
    //TODO Tratar esse Exception
    GoogleSignInAccount googleUser =
        await _googleSignIn.signIn().catchError((onError) {
      print("Error $onError");
    });
    if (googleUser != null) {
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      AuthResult authResult = await auth.signInWithCredential(credential);
      FirebaseUser user = authResult.user;

      print('await checkDBInfo(user.uid) ${await checkDBInfo(user.uid)}');
      if (await checkDBInfo(user.uid)) {
        return Future.value(user);
      } else {
        await createUserInfo(user);
        await updateUserInfo();
        return Future.value(user);
      }
    } else {
      print('else');
      return Future.value(null);
    }
  }

  Future<bool> checkDBInfo(String uid) async {
    QuerySnapshot snapshot = await this
        ._firestore
        .collection('users')
        .where('uid', isEqualTo: uid)
        .getDocuments();
    return Future.value(snapshot.documents.length > 0);
  }

  createUserInfo(FirebaseUser user) {
    this._firestore.document('users/${user.uid}').setData({
      'email': user.email,
      'uid': user.uid,
      'estabelecimentosFavoritados': [],
      'produtosFavoritados': []
    });
  }

  Future<List<Estabelecimento>> getEstabelecimentosFavoritosAsync() async {
    if (firebaseUser == null) return Future.value([]);
    QuerySnapshot querySnapshot = await _firestore
        .collection('users')
        .document(firebaseUser?.uid)
        .collection('estabelecimentosFavoritos')
        .getDocuments()
        .timeout(Duration(seconds: 10));
    estabelecimentosFavoritos = querySnapshot.documents
        .map((d) => Estabelecimento.fromFirestore(d))
        .toList();
    return estabelecimentosFavoritos;
  }

  Future<List<Estabelecimento>> favoritarEstabelecimento(
      Estabelecimento estabelecimento) async {
    bool adicionou;
    if (estabelecimentosFavoritos.length >= 10) {
      utilService.snackBarErro(
          titulo: 'Limite atingido',
          mensagem:
              'Você atingiu o limite de dez estabelecimentos favoritos. Exclua algum para adicionar outro');
      return getEstabelecimentosFavoritosAsync();
    }
    /**Bloco para testes */
    /* if (estabelecimentosFavoritos
        .where((e) => e.numCNPJ.contains(estabelecimento.numCNPJ))
        .isEmpty) {
      estabelecimentosFavoritos.add(estabelecimento);
      print('adicionou');
    } else {
      estabelecimentosFavoritos
          .removeWhere((e) => e.numCNPJ.contains(estabelecimento.numCNPJ));
      print('removeu');
    }
    print('Favoritar estabelecimento: $estabelecimentosFavoritos');
    return; */
    /**fim dos testes */
    if (estabelecimentosFavoritos
        .where((e) => e.numCNPJ == estabelecimento.numCNPJ)
        .isNotEmpty) {
      await _firestore
          .collection('users')
          .document(user.uid)
          .collection('estabelecimentosFavoritos')
          .document(estabelecimento.numCNPJ)
          .delete();
      adicionou = false;
    } else {
      estabelecimentosFavoritos.add(
          estabelecimento); //Linha adicionada para que o botao de favoritar atualize de imediato
      await _firestore
          .collection('users')
          .document(user.uid)
          .collection('estabelecimentosFavoritos')
          .document(estabelecimento.numCNPJ)
          .setData(estabelecimento.asMap);
      adicionou = true;
    }
    await updateUserInfo();
    return getEstabelecimentosFavoritosAsync();
  }

  void removeEstabelecimentoFavoritoErro(Estabelecimento estabelecimento) {
    estabelecimentosFavoritos
        .removeWhere((e) => e.numCNPJ.contains(estabelecimento.numCNPJ));
  }

  bool checkEstabelecimentoFavorito(Estabelecimento estabelecimento) {
    return estabelecimentosFavoritos
        .where((e) => e.numCNPJ.contains(estabelecimento.numCNPJ))
        .isNotEmpty;
  }

  logout() {
    auth.signOut();
    user = null;
  }
}
