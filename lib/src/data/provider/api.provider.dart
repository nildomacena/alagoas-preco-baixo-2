import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/estabelecimentos_combustivel.dart';
import 'package:aldescontos/src/data/model/produto.model.dart';
import 'package:aldescontos/src/data/model/venda_combustivel.modal.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/model/produto_pesquisado.model.dart';
import 'package:aldescontos/src/data/model/lista_compras_estabelecimento.model.dart';
import 'package:aldescontos/src/data/services/location.service.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart' as getx;

const tokenSefaz = '82505872dbf2c05c502d169bf1c2a1dee28d9174';
const URL_GTINS = 'https://api.cosmos.bluesoft.com.br/gtins/';

class ApiProvider {
  SharedPreferences prefs;
  final Dio dio = Dio();
  Firestore _firestore = Firestore.instance;
  FirebaseAuth _auth = FirebaseAuth.instance;
  List<ProdutoListaCompras> listaCompras = [];
  ApiProvider() {
    initPrefs();
  }

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
    await Future.delayed(Duration(seconds: 2));
    FirebaseUser user = await _auth.currentUser();
    if (user != null) {
      listaCompras = await getListaCompras(user);
    }
  }

  String get tokenGTIN => 'Soa_taO_LD9B9ei0_xZNGA';

  Future<bool> servidoresOnline() async {
    DocumentSnapshot snapshot =
        await _firestore.document('servidores/servidores').get();
    return snapshot.data['online'] ?? true;
  }

  Future<Produto> getProdutoPorCodigoBarras(String codBarras) async {
    try {
      Response response = await dio
          .get(URL_GTINS + codBarras,
              options: Options(contentType: 'application/json', headers: {
                'appToken': tokenSefaz,
                'X-Cosmos-Token': tokenGTIN,
                'User-Agent': 'Cosmos-API-Request'
              }))
          .timeout(Duration(milliseconds: 2500));
      Produto produto = Produto.fromBlueSoft(response.data);
      print('produto::: $produto');
      return produto;
    } catch (e) {
      print('Ocorreu um erro: $e');
    }
  }

  Future<List<Venda>> pesquisarProdutoPorCodigoBarras(String codGetin,
      [LocationData locationData]) async {
    print('codGetin $codGetin');
    if (locationData == null) {
      locationData = await locationService.getLocationData();
    }
    List resposta = [];
    List<Venda> vendas;
    if (locationData == null)
      locationData = LocationData.fromMap(
          {'latitude': -9.6432331, 'longitude': -35.7190686});
    Response response = await dio
        .post(
            'http://api.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosPorCodigoDeBarras',
            data: {
              "codigoDeBarras": codGetin,
              "dias": 3,
              "latitude": locationData.latitude ?? -9.6432331,
              "longitude": locationData.longitude ?? -35.7190686,
              "raio": 15
            },
            options: Options(
                contentType: 'application/json',
                headers: {'appToken': tokenSefaz}))
        .timeout(Duration(seconds: 10));
    resposta = response.data;
    resposta.sort((a, b) {
      if (a['valUnitarioUltimaVenda'] < b['valUnitarioUltimaVenda'])
        return -1;
      else if (a['valUnitarioUltimaVenda'] > b['valUnitarioUltimaVenda'])
        return 1;
      return 0;
    });
    vendas = resposta.map((f) => Venda.fromAPI(f)).toList();
    /**Salva as consultas no SharedPreferences */
    DateTime date = DateTime.now();
    List<String> aux = [];
    if (prefs.getStringList('ultimosProdutosConsultados') != null &&
        prefs.getStringList('ultimosProdutosConsultados').length > 0) {
      bool jaConsultado = false;
      prefs.getStringList('ultimosProdutosConsultados').forEach((p) {
        /**Verifica se o produto já está no SharedPreferences */
        if (p.contains(vendas[0].produto.codGetin)) jaConsultado = true;
      });
      if (!jaConsultado) {
        aux.add(
            '${vendas[0].produto.codGetin}_${vendas[0].produto.dscProduto}*${date.day}/${date.month}/${date.year}');
        prefs.getStringList('ultimosProdutosConsultados').forEach((p) {
          aux.add(p);
        });
        prefs.setStringList('ultimosProdutosConsultados', aux);
      }
    } else {
      prefs.setStringList('ultimosProdutosConsultados', [
        '${vendas[0].produto.codGetin}_${vendas[0].produto.dscProduto}*${date.day}/${date.month}/${date.year}'
      ]);
    }
    // Linha de teste para zerar os produtos consultados prefs.setStringList('ultimosProdutosConsultados', []);
    if (kDebugMode) {
      vendas.forEach((venda) {
        _firestore
            .collection('estabelecimentos')
            .document(venda.estabelecimento.numCNPJ)
            .setData({
          'nomBairro': venda.estabelecimento.nomBairro,
          'nomFantasia': venda.estabelecimento.nomFantasia,
          'nomLogradouro': venda.estabelecimento.nomLogradouro,
          'nomMunicipio': venda.estabelecimento.nomMunicipio,
          'nomRazaoSocial': venda.estabelecimento.nomRazaoSocial,
          'numCNPJ': venda.estabelecimento.numCNPJ,
          'numCep': venda.estabelecimento.numCep,
          'numImovel': venda.estabelecimento.numImovel,
          'numLatitude': venda.estabelecimento.numLatitude,
          'numLongitude': venda.estabelecimento.numLongitude,
          'numTelefone': venda.estabelecimento.numTelefone,
        });
      });
    }
    return vendas;
  }

  Future<List<Venda>> buscarPorDescricao(
      String descricao, LocationData locationData) async {
    List resposta = [];
    List<Venda> vendas;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (locationData == null)
      locationData = LocationData.fromMap({
        'latitude': -9.6432331,
        'longitude': -35.7190686,
        'accuracy': 5.36,
        'speed': 0.0,
        'speedAccuracy': 1.66,
        'heading': 0.0,
        'time': DateTime.now().millisecondsSinceEpoch.toDouble()
      });
    if (prefs.getStringList('pesquisa') == null) {
      prefs.setStringList('pesquisa', []);
    }
    List<String> aux = prefs.getStringList('pesquisa');
    aux.add(descricao);
    prefs.setStringList('pesquisa', aux);
    Response response = await dio
        .post(
            'http://api.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosPorDescricao',
            data: {
              "descricao": descricao,
              "latitude": locationData.latitude ?? -9.6432331,
              "longitude": locationData.longitude ?? -35.7190686,
              "dias": 3,
              "raio": 15
            },
            options: Options(
                contentType: 'application/json',
                headers: {'appToken': tokenSefaz}))
        .timeout(Duration(seconds: 10));

    resposta = response.data;
    resposta.sort((a, b) {
      if (a['valUnitarioUltimaVenda'] < b['valUnitarioUltimaVenda'])
        return -1;
      else if (a['valUnitarioUltimaVenda'] > b['valUnitarioUltimaVenda'])
        return 1;
      return 0;
    });
    vendas = resposta.map((f) => Venda.fromAPI(f)).toList();
    if (kDebugMode) {
      vendas.forEach((venda) {
        _firestore
            .collection('estabelecimentos')
            .document(venda.estabelecimento.numCNPJ)
            .setData({
          'nomBairro': venda.estabelecimento.nomBairro,
          'nomFantasia': venda.estabelecimento.nomFantasia,
          'nomLogradouro': venda.estabelecimento.nomLogradouro,
          'nomMunicipio': venda.estabelecimento.nomMunicipio,
          'nomRazaoSocial': venda.estabelecimento.nomRazaoSocial,
          'numCNPJ': venda.estabelecimento.numCNPJ,
          'numCep': venda.estabelecimento.numCep,
          'numImovel': venda.estabelecimento.numImovel,
          'numLatitude': venda.estabelecimento.numLatitude,
          'numLongitude': venda.estabelecimento.numLongitude,
          'numTelefone': venda.estabelecimento.numTelefone,
        });
      });
    }
    return vendas;
  }

  Future<Venda> pesquisarProdutoPorEstabelecimento(
      String codGetin, Estabelecimento estabelecimento) async {
    List<Venda> vendas;
    Response response = await dio
        .post(
            'http://api.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecoProdutoEmEstabelecimento',
            data: {
              "cnpj": estabelecimento.numCNPJ,
              "codigoBarras": codGetin,
              "dias": 3
            },
            options: Options(
                contentType: 'application/json',
                headers: {'appToken': tokenSefaz}))
        .timeout(Duration(seconds: 10));
    if (response.data.length == 0) return Future.value(null);
    vendas = [Venda.fromAPI(response.data)];
    return vendas.first;
  }

  List<ProdutoPesquisado> getProdutosPesquisados() {
    if (prefs.getStringList('ultimosProdutosConsultados') == null) return [];
    return prefs
        .getStringList('ultimosProdutosConsultados')
        .map((str) => ProdutoPesquisado.fromString(str))
        .toList();
  }

  Future<List<Estabelecimento>> pesquisarEstabelecimentos(
      String pesquisa) async {
    pesquisa = pesquisa.toUpperCase();
    pesquisa = pesquisa.replaceAll(new RegExp(r'Ç'), 'C');
    pesquisa = pesquisa.replaceAll(new RegExp(r'Á'), 'A').toUpperCase();
    pesquisa = pesquisa.replaceAll(new RegExp(r'É'), 'E').toUpperCase();
    //pesquisa = pesquisa.replaceAll(new RegExp(r'Í'), 'I').toUpperCase();
    pesquisa = pesquisa.replaceAll(new RegExp(r'Ó'), 'O').toUpperCase();
    QuerySnapshot querySnapshot = await _firestore
        .collection('estabelecimentos')
        .where('nomFantasia', isGreaterThanOrEqualTo: pesquisa)
        .getDocuments();
    if (querySnapshot.documents.isEmpty) return [];
    List<Estabelecimento> estabelecimentos = querySnapshot.documents
        .map((s) => Estabelecimento.fromFirestore(s))
        .toList();
    estabelecimentos = estabelecimentos
        .where((e) => e.nomFantasia.contains(pesquisa))
        .toList();
    return estabelecimentos;
  }

  Future<List<Estabelecimento>> getEstabelecimentosFavoritos(
      [FirebaseUser onUser]) async {
    List<Estabelecimento> favoritos = [];
    FirebaseUser currentUser =
        onUser ?? await FirebaseAuth.instance.currentUser();
    if (currentUser == null) {
      utilService.snackBarErro(
          titulo: 'Ação necessário',
          mensagem: 'É preciso fazer login, para acessar essa tela');
      return null;
    }
    QuerySnapshot querySnapshot = await _firestore
        .collection('users/${currentUser.uid}/estabelecimentosFavoritos')
        .getDocuments();

    favoritos = querySnapshot.documents
        .map((DocumentSnapshot snap) => Estabelecimento.fromFirestore(snap))
        .toList();
    print('Favoritos getEstabelecimentoFavoritos: $favoritos');
    return favoritos;
  }

  Future<List<Estabelecimento>> deletarEstabelecimentoFavorito(
      Estabelecimento estabelecimento) async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if (user == null) {
      utilService.snackBarErro(
          mensagem: 'É preciso estar logado para acessar essa tela');
      return null;
    }
    await _firestore
        .collection('users')
        .document(user.uid)
        .collection('estabelecimentosFavoritos')
        .document(estabelecimento.numCNPJ)
        .delete();

    return getEstabelecimentosFavoritos(user);
  }

/**Lista de compras */
  Future<List<ProdutoListaCompras>> getListaCompras(
      [FirebaseUser onUser]) async {
    List<ProdutoListaCompras> produtos = [];

    FirebaseUser currentUser =
        onUser ?? await FirebaseAuth.instance.currentUser();
    if (currentUser == null) {
      utilService.snackBarErro(
          titulo: 'Ação necessário',
          mensagem: 'É preciso fazer login, para acessar essa tela');
      return null;
    }
    QuerySnapshot querySnapshot = await _firestore
        .collection('users/${currentUser.uid}/listaCompras')
        .getDocuments();
    if (querySnapshot.documents.isEmpty) return [];
    produtos = querySnapshot.documents
        .map((DocumentSnapshot snap) => ProdutoListaCompras.fromFirestore(snap))
        .toList();
    listaCompras = produtos;
    return produtos;
  }

  Future<List<ProdutoListaCompras>> salvarProdutoListaCompras(
      Venda venda) async {
    FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    if (currentUser == null) {
      throw 'nao-autenticado';
    }
    if (listaCompras.length >= 30) {
      throw 'limite-excedido';
    }
    if (listaCompras
        .where((p) => venda.produto.codGetin.contains(p.codGetin))
        .isNotEmpty) {
      throw 'produto-duplicado';
    }
    _firestore
        .collection('users')
        .document(currentUser.uid)
        .collection('listaCompras')
        .document(venda.produto.codGetin)
        .setData({
      'codGetin': venda.produto.codGetin,
      'dscProduto': venda.produto.dscProduto,
      'valUltimaVenda': venda.valUnitarioUltimaVenda,
      'dataConsulta': venda.dthEmissaoUltimaVenda,
      'estabelecimento': venda.estabelecimento.nomFantasia ??
          venda.estabelecimento.nomRazaoSocial ??
          ''
    }).timeout(Duration(seconds: 8));
    return getListaCompras();
  }

  Future<List<ProdutoListaCompras>> removerProdutoListaCompras(
      String codGetin) async {
    FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    if (currentUser == null) {
      utilService.snackBarErro(
          mensagem:
              'É preciso fazer o login para utilizar essa funcionalidade');
      throw 'nao-autenticado';
    }
    _firestore
        .collection('users')
        .document(currentUser.uid)
        .collection('listaCompras')
        .document(codGetin)
        .delete();
    return getListaCompras();
  }

  deletarProdutoBuscado(ProdutoPesquisado produto) async {
    List<String> aux = [];
    String string = '${produto.codGetin}_${produto.dscProduto}*${produto.data}';
    prefs.getStringList('ultimosProdutosConsultados').forEach((p) {
      if (p != string) aux.add(p);
    });
    return prefs.setStringList('ultimosProdutosConsultados', aux);
  }

  checkAddListaCompras(String codGetin) {
    bool adicionado = false;
    listaCompras.forEach((produto) {
      if (!produto.codGetin.contains('SEM') &&
          produto.codGetin.contains(codGetin)) {
        adicionado = true;
      }
    });
    return adicionado;
  }

  Future<List<Venda>> getListaComprasPorEstabelecimento(
      List<ProdutoListaCompras> lista, Estabelecimento estabelecimento) {
    List<Future<Venda>> futures = [];
    lista.forEach((p) {
      futures
          .add(pesquisarProdutoPorEstabelecimento(p.codGetin, estabelecimento));
    });
    return Future.wait(futures);
  }

  Future<Map<String, dynamic>> calcularPrecoListaCompras(
      List<Estabelecimento> estabelecimentos) async {
    FirebaseUser currentUser = await FirebaseAuth.instance.currentUser();
    List<ProdutoListaCompras> listaCompras;
    List<ListaComprasEstabelecimento> listaComprasEstabelecimentos = [];
    List<ListaComprasEstabelecimento> listaComprasNaoFiltrada = [];
    List<ProdutoListaCompras> produtosEncontradosEmTodos = [];
    List<ProdutoListaCompras> produtosNaoEncontradosEmTodos = [];
    List<ProdutoListaCompras> produtosNaoEncontrados = [];
    List<ProdutoListaCompras> produtosEncontrados = [];
    Map<String, dynamic> map;
    if (currentUser == null) {
      utilService.snackBarErro(
          mensagem:
              'É preciso fazer o login para utilizar essa funcionalidade');
      throw 'nao-autenticado';
    }
    listaCompras = await getListaCompras(currentUser);
    if (listaCompras.isEmpty) {
      utilService.snackBarErro(
          mensagem:
              'Sua lista de compras está fazia\nAdicione algum produto antes de calcular o valor');
      throw 'lista-vazia';
    }

    if (estabelecimentos.isEmpty) {
      utilService.snackBarErro(
          mensagem:
              'Você não selecionou nenhum estabelecimento.\nAdicione algum estabelecimento para calcular o valor');
      throw 'lista-vazia';
    }
    utilService.showAlertCarregando();
    //Pesquisando a lista em cada estabelecimento
    await Future.forEach(estabelecimentos, (Estabelecimento e) async {
      listaComprasEstabelecimentos.add(ListaComprasEstabelecimento(
          estabelecimento: e,
          vendas: await getListaComprasPorEstabelecimento(listaCompras, e)));
    }).timeout(Duration(seconds: 30));
    listaComprasEstabelecimentos.forEach((ListaComprasEstabelecimento e) {
      listaComprasNaoFiltrada.add(ListaComprasEstabelecimento(
          estabelecimento: e.estabelecimento, vendas: e.vendas));
    });
    print('listaComprasNaoFiltrada: $listaComprasNaoFiltrada');
    if (getx.Get.isDialogOpen) getx.Get.back();

    //Checando quais produtos faltaram em algum estabelecimento
    listaCompras.forEach((p) {
      bool faltouEmAlgumEstabelecimento = false;

      listaComprasEstabelecimentos.forEach((estabelecimentos) {
        if (!estabelecimentos.checkProduto(p))
          faltouEmAlgumEstabelecimento = true;
      });
      if (!faltouEmAlgumEstabelecimento) {
        produtosEncontradosEmTodos.add(p);
      } else
        produtosNaoEncontradosEmTodos.add(p);
    });

    //Checa quais produtos existem em algum estabelecimento
    listaCompras.forEach((p) {
      print('${p.dscProduto} - ${p.codGetin.replaceAll('0', '')}');
      bool jaInserido = produtosEncontrados
          .where((element) => element.codGetin.contains(p.codGetin))
          .toList()
          .isNotEmpty;
      listaComprasEstabelecimentos.forEach((e) {
        if (e.checkProduto(p) && (!jaInserido || produtosEncontrados.isEmpty)) {
          produtosEncontrados.add(p);
        }
      });
    });

    listaCompras.forEach((p) {
      bool jaInserido = produtosNaoEncontrados
              .where((element) => element.codGetin.contains(p.codGetin))
              .isNotEmpty &&
          produtosNaoEncontrados.isNotEmpty;
      bool produtoEncontrado = produtosEncontrados
          .where((element) => element.codGetin.contains(p.codGetin))
          .isNotEmpty;
      if (!jaInserido && (!produtoEncontrado)) produtosNaoEncontrados.add(p);
    });
    //Checa quais produtos nao foram encontrados em nenhum estabelecimento
/* 
    listaCompras.forEach((p) {
      if (!produtosEncontrados.contains(p)) produtosNaoEncontrados.add(p);
    }); */

    //Ordenando os itens listados
    listaComprasEstabelecimentos.forEach((estabelecimento) {
      estabelecimento.updateProdutosNaoAchados(produtosNaoEncontradosEmTodos);
    });

    listaComprasEstabelecimentos
        .sort((a, b) => a.getValorTotal().compareTo(b.getValorTotal()));
/*     listaComprasNaoFiltrada.forEach((lce) {
      print('LCE: $lce');
    }); */
    listaComprasNaoFiltrada.forEach((lce) {
      lce.vendas.sort((a, b) {
        print('AB');
        /*   if (produtosNaoEncontradosEmTodos
            .where((p) => p.codGetin.contains(a.produto.codGetin))
            .isNotEmpty) {
          print('nao encontrou');
          return -1;
        }
 */
        return a.produto.codGetin.compareTo(b.produto.codGetin);
      });
    });
    map = {
      'listaComprasEstabelecimentos': listaComprasEstabelecimentos,
      'produtosNaoEncontradosEmTodos': produtosNaoEncontradosEmTodos,
      'listaComprasNaoFiltrada': listaComprasNaoFiltrada,
      'produtosNaoEncontrados': produtosNaoEncontrados
    };
    return map;
    /*    estabelecimentosVendas[];
      Future.wait(futures); */
  }

  String tipoCombustivelPorIndex(int index) {
    switch (index) {
      case 0:
        return TiposCombustivel.GASOLINA_COMUM;
      case 1:
        return TiposCombustivel.GASOLINA_ADITIVADA;
      case 2:
        return TiposCombustivel.ALCOOL;
      case 3:
      //return TiposCombustivel.DIESEL_COMUM;
      case 4:
      //return TiposCombustivel.DIESEL_ADITIVADO;
      case 5:
        return TiposCombustivel.GNV;
    }
  }

  Future<List<EstabelecimentoCombustivel>> buscarPrecosCombustivel(
      [LocationData locationData]) async {
    List<Future<Response>> futureResponses = [];
    List<EstabelecimentoCombustivel> estabelecimentosCombustivel = [];
    print('buscarPrecosCombustivel');
    if (locationData == null) {
      locationData = await locationService.getLocationData();
    }
    for (int i = 0; i <= 5; i++) {
      futureResponses.add(dio
          .post(
              'http://api.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosCombustivel',
              data: {
                "codTipoCombustivel": (i + 1).toString(),
                "dias": 3,
                "latitude": -9.6432331,
                "longitude": -35.7190686,
                "raio": 15
              },
              options: Options(
                  contentType: 'application/json',
                  headers: {'appToken': tokenSefaz}))
          .timeout(Duration(seconds: 10)));
    }
    List<Response> responses = await Future.wait(futureResponses);
    /*   Response response = await dio
        .post(
            'http://api.sefaz.al.gov.br/sfz_nfce_api/api/public/consultarPrecosCombustivel',
            data: {
              "codTipoCombustivel": TiposCombustivel.GASOLINA_COMUM,
              "dias": 3,
              "latitude": -9.6432331,
              "longitude": -35.7190686,
              "raio": 15
            },
            options: Options(
                contentType: 'application/json',
                headers: {'appToken': tokenSefaz}))
        .timeout(Duration(seconds: 10)); */
    int index = -1;
    //Criando uma matriz com lista de combustiveis
    List<List<VendaCombustivel>> matrizCombustiveis =
        responses.map<List<VendaCombustivel>>((response) {
      index++;
      return response.data
          .map<VendaCombustivel>((d) => VendaCombustivel.fromAPI(
              d, (tipoCombustivelPorIndex(index)).toString()))
          .toList();
    }).toList();
    index = -1;

    //Definindo os preços de combustivel por estabelecimento
    matrizCombustiveis.forEach((listaCombustiveis) {
      index++;
      listaCombustiveis.forEach((vendaCombustivel) {
        if (index == 0 && vendaCombustivel.numCNPJ != '20528778000185') {
          EstabelecimentoCombustivel estabelecimentoCombustivel =
              EstabelecimentoCombustivel(
                  estabelecimento: vendaCombustivel.estabelecimento,
                  locationData: locationData);
          estabelecimentoCombustivel.setCombustivel(
              vendaCombustivel, tipoCombustivelPorIndex(index));
          estabelecimentosCombustivel.add(estabelecimentoCombustivel);
        } else {
          EstabelecimentoCombustivel estabelecimentoCombustivel =
              estabelecimentosCombustivel
                      .where((element) => element.estabelecimento.numCNPJ
                          .contains(vendaCombustivel.numCNPJ))
                      .isNotEmpty
                  ? estabelecimentosCombustivel
                      .where((element) => element.estabelecimento.numCNPJ
                          .contains(vendaCombustivel.numCNPJ))
                      .toList()[0]
                  : null;
          if (estabelecimentoCombustivel != null)
            estabelecimentoCombustivel.setCombustivel(
                vendaCombustivel, tipoCombustivelPorIndex(index));
          else {
            estabelecimentoCombustivel = new EstabelecimentoCombustivel(
                estabelecimento: vendaCombustivel.estabelecimento,
                locationData: locationData);
            estabelecimentoCombustivel.setCombustivel(
                vendaCombustivel, tipoCombustivelPorIndex(index));
            estabelecimentosCombustivel.add(estabelecimentoCombustivel);
          }
        }
        //Se for o primeiro da lista, já insere
        /* if (estabelecimentosCombustivel.isEmpty) {
          EstabelecimentoCombustivel estabelecimentoCombustivel =
              EstabelecimentoCombustivel(
                  estabelecimento: vendaCombustivel.estabelecimento);
          estabelecimentoCombustivel.setCombustivel(
              vendaCombustivel, tipoCombustivelPorIndex(index));

          estabelecimentosCombustivel.add(estabelecimentoCombustivel);
        } else if (estabelecimentosCombustivel
            .where((element) =>
                element.estabelecimento.numCNPJ == vendaCombustivel.numCNPJ)
            .isEmpty) {
          EstabelecimentoCombustivel estabelecimentoCombustivel =
              EstabelecimentoCombustivel(
                  estabelecimento: vendaCombustivel.estabelecimento);
          estabelecimentoCombustivel.setCombustivel(
              vendaCombustivel, tipoCombustivelPorIndex(index));
        } */
      });
    });
    //print('estabelecimentosCombustivel: $estabelecimentosCombustivel');
    return estabelecimentosCombustivel;
  }

  /**PROMOÇÕES */

  Future<dynamic> addPromocao(Venda venda) {
    Map data = venda.asMap;
    data['timestamp'] = FieldValue.serverTimestamp();
    print('addPromocao $data');
    return _firestore
        .collection('promocoes')
        .add(data)
        .timeout(Duration(seconds: 20));
  }

  Future<List<Venda>> getPromocoes() async {
    QuerySnapshot querySnapshot =
        await _firestore.collection('promocoes').getDocuments();
    List<Venda> promocoes =
        querySnapshot.documents.map((s) => Venda.fromFirestore(s)).toList();
    return promocoes.where((element) => element.tempoEmHoras <= 30).toList();
  }

  Future<List<Venda>> deletarPromocao(Venda venda) async {
    if (venda.id == null) return getPromocoes();
    await _firestore.collection('promocoes').document(venda.id).delete();
    return getPromocoes();
  }
}
