import 'package:cloud_firestore/cloud_firestore.dart';

class Produto {
  final String codGetin;
  String dscProduto;
  String imagemUrl;

  Produto({this.codGetin, this.dscProduto, this.imagemUrl});

  factory Produto.fromAPI(response) {
    return Produto(
        codGetin: response['codGetin'],
        dscProduto: response['dscProduto'] ?? '');
  }

  factory Produto.fromBlueSoft(response) {
    print('response fromBlueSoft: $response');
    return Produto(
        codGetin: response['gtin'].toString(),
        imagemUrl: response['thumbnail'],
        dscProduto: response['description'] ?? '');
  }

  Map<String, dynamic> get asMap =>
      {'codGetin': codGetin, 'dscProduto': dscProduto, 'imagemUrl': imagemUrl};

  factory Produto.fromFirestore(DocumentSnapshot doc) {
    dynamic data = doc.data;
    return Produto(
        codGetin: data['codGetin'],
        dscProduto: data['dscProduto'] ?? '',
        imagemUrl: data['imagemUrl'] ?? '');
  }
}
