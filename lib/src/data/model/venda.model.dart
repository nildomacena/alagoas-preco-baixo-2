import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Venda {
  String id;
  DateTime timestamp;
  Produto produto;
  final Estabelecimento estabelecimento;
  final String dthEmissaoUltimaVenda;
  final String txtDataUltimaEmissao;
  final double valUnitarioUltimaVenda;
  final double valUltimaVenda;
  final double valMinimoVendido;
  final double valMaximoVendido;

  Venda(
      {this.produto,
      this.estabelecimento,
      this.dthEmissaoUltimaVenda,
      this.valUnitarioUltimaVenda,
      this.valUltimaVenda,
      this.txtDataUltimaEmissao,
      this.valMinimoVendido,
      this.valMaximoVendido,
      this.id,
      this.timestamp}) {
    print('tempoEmDias: $tempoEmHorasFormatado');
  }

  double get tempoEmHoras {
    if (timestamp == null) return double.infinity;
    return (DateTime.now().millisecondsSinceEpoch -
            timestamp.millisecondsSinceEpoch) /
        1000 /
        60 /
        60;
  }

  String get tempoEmHorasFormatado {
    if (tempoEmHoras > 24) {
      return '${(tempoEmHoras / 24).toStringAsFixed(0)} dias';
    } else if (tempoEmHoras >= 1) {
      return '${tempoEmHoras.toStringAsFixed(0)} horas';
    } else
      return '${(tempoEmHoras * 60).toStringAsFixed(0)} minutos';
  }

  factory Venda.fromAPI(response) {
    //TODO um dos valores está vindo como double. Verificar qual
    return Venda(
      produto: Produto.fromAPI(response),
      estabelecimento: Estabelecimento.fromAPI(response),
      dthEmissaoUltimaVenda: response['dthEmissaoUltimaVenda'],
      valMaximoVendido: response['valMaximoVendido'].toDouble(),
      valMinimoVendido: response['valMinimoVendido'].toDouble(),
      valUltimaVenda: response['valUltimaVenda'].toDouble(),
      valUnitarioUltimaVenda: response['valUnitarioUltimaVenda'].toDouble(),
      txtDataUltimaEmissao: response['txtDataUltimaEmissao'],
    );
  }

  factory Venda.fromFirestore(DocumentSnapshot snapshot) {
    dynamic data = snapshot.data;
    //TODO um dos valores está vindo como double. Verificar qual
    return Venda(
        produto: Produto.fromAPI(data['produto']),
        estabelecimento: Estabelecimento.fromAPI(data['estabelecimento']),
        dthEmissaoUltimaVenda: data['dthEmissaoUltimaVenda'],
        valMaximoVendido: data['valMaximoVendido'].toDouble(),
        valMinimoVendido: data['valMinimoVendido'].toDouble(),
        valUltimaVenda: data['valUltimaVenda'].toDouble(),
        valUnitarioUltimaVenda: data['valUnitarioUltimaVenda'].toDouble(),
        txtDataUltimaEmissao: data['txtDataUltimaEmissao'],
        timestamp: data['timestamp']?.toDate() ?? null,
        id: snapshot.documentID);
  }

  Map<String, dynamic> get asMap => {
        'produto': produto.asMap,
        'estabelecimento': estabelecimento.asMap,
        'dthEmissaoUltimaVenda': dthEmissaoUltimaVenda,
        'txtDataUltimaEmissao': txtDataUltimaEmissao,
        'valUnitarioUltimaVenda': valUnitarioUltimaVenda,
        'valUltimaVenda': valUltimaVenda,
        'valMinimoVendido': valMinimoVendido,
        'valMaximoVendido': valMaximoVendido,
      };

  String get horaFormatada {
    String hora = dthEmissaoUltimaVenda.substring(11, 16);
    String dia = dthEmissaoUltimaVenda.substring(8, 10);
    String mes = dthEmissaoUltimaVenda.substring(5, 7);
    String ano = dthEmissaoUltimaVenda.substring(0, 4);
    return '$dia/$mes/$ano - $hora';
  }

  @override
  String toString() {
    return 'Produto: ${produto.dscProduto} - Cód: ${produto.codGetin} - Estabelecimento: ${estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial}';
  }
}
