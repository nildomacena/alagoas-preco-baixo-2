import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/venda_combustivel.modal.dart';
import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';
import 'package:maps_toolkit/maps_toolkit.dart';
import 'package:maps_toolkit/src/spherical_util.dart';

class EstabelecimentoCombustivel {
  final Estabelecimento estabelecimento;
  double distanceBetweenPoints;
  VendaCombustivel _gasolinaComum;
  VendaCombustivel _gasolinaAditivada;
  VendaCombustivel _alcool;
  VendaCombustivel _dieselComum;
  VendaCombustivel _dieselAditivado;
  VendaCombustivel _gnv;

  VendaCombustivel get gasolinaComum => _gasolinaComum;
  set gasolinaComum(value) => _gasolinaComum = value;

  VendaCombustivel get gasolinaAditivada => _gasolinaAditivada;
  set gasolinaAditivada(value) => _gasolinaAditivada = value;
  VendaCombustivel get alcool => _alcool;
  set alcool(value) => _alcool = value;
  VendaCombustivel get dieselComum => _dieselComum;
  set dieselComum(value) => dieselComum = value;
  VendaCombustivel get dieselAditivado => _dieselAditivado;
  set dieselAditivado(value) => _dieselAditivado = value;
  VendaCombustivel get gnv => _gnv;
  set gnv(value) => _gnv = value;

  EstabelecimentoCombustivel(
      {@required this.estabelecimento, @required LocationData locationData}) {
    distanceBetweenPoints = SphericalUtil.computeDistanceBetween(
        LatLng(locationData.latitude, locationData.longitude),
        LatLng(estabelecimento.numLatitude, estabelecimento.numLongitude));
    print('distanceBetweenPoints: $distanceBetweenPoints');
  }

  String get distancia => distanceBetweenPoints < 1000
      ? '${(distanceBetweenPoints).toStringAsFixed(0)} Metros'
      : '${(distanceBetweenPoints / 1000).toStringAsFixed(2)} KM';
  factory EstabelecimentoCombustivel.fromAPI(
      response, LocationData locationData) {
    return EstabelecimentoCombustivel(
        estabelecimento: Estabelecimento.fromAPI(response),
        locationData: locationData);
  }

  void setDistancia(LocationData locationData) {
    distanceBetweenPoints = SphericalUtil.computeDistanceBetween(
        LatLng(locationData.latitude, locationData.longitude),
        LatLng(estabelecimento.numLatitude, estabelecimento.numLongitude));
  }

  void setCombustivel(
      VendaCombustivel vendaCombustivel, String tipoCombustivel) {
    switch (tipoCombustivel) {
      case TiposCombustivel.GASOLINA_COMUM:
        gasolinaComum = vendaCombustivel;
        break;
      case TiposCombustivel.GASOLINA_ADITIVADA:
        gasolinaAditivada = vendaCombustivel;
        break;
      case TiposCombustivel.ALCOOL:
        alcool = vendaCombustivel;
        break;
      case TiposCombustivel.DIESEL_COMUM:
        dieselComum = vendaCombustivel;
        break;
      case TiposCombustivel.DIESEL_ADITIVADO:
        dieselAditivado = vendaCombustivel;
        break;
      case TiposCombustivel.GNV:
        gnv = vendaCombustivel;
        break;
    }
  }

  @override
  String toString() {
    return '${estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial} - Gas. Comum: $gasolinaComum - Gas. Adit.: $gasolinaAditivada - Álcool.: $alcool - GNV: $gnv';
  }
}
