import 'package:flutter/material.dart';

class ProdutoPesquisado {
  final String codGetin;
  final String dscProduto;
  final String data;

  ProdutoPesquisado(
      {@required this.codGetin, @required this.dscProduto, @required this.data})
      : assert(codGetin != null && dscProduto != null && data != null);

  factory ProdutoPesquisado.fromString(String str) {
    return ProdutoPesquisado(
        codGetin: str.substring(0, str.indexOf('_')),
        dscProduto: str.substring(str.indexOf('_') + 1, str.indexOf('*')),
        data: str.substring(str.indexOf('*') + 1));
  }

  @override
  String toString() {
    return 'Código: $codGetin - Descrição: $dscProduto - Data: $data';
  }
}
