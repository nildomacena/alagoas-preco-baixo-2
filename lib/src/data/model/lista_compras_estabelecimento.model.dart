import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:flutter/cupertino.dart';

class ListaComprasEstabelecimento {
  List<Venda> vendas;
  final Estabelecimento estabelecimento;
  ListaComprasEstabelecimento(
      {@required this.estabelecimento, @required this.vendas})
      : assert(estabelecimento != null && vendas != null) {
    vendas.removeWhere((venda) => venda == null);
  }
  double getValorTotal() {
    double soma = 0;
    vendas.forEach((Venda venda) {
      if (venda != null) soma += venda.valUnitarioUltimaVenda;
    });
    return soma;
  }

  int getTotalItensNonNull() {
    int total = 0;
    vendas.forEach((element) {
      if (element != null) total++;
    });
    return total;
  }

  bool checkProduto(ProdutoListaCompras produto) {
    bool achou = false;
    vendas.forEach((v) {
      if (v != null &&
          !v.produto.codGetin.contains('SEM') &&
          v.produto.codGetin.contains(produto.codGetin)) achou = true;
    });
    return achou;
  }

  List<String> toStringProdutosAchados() {
    return vendas.map((v) {
      if (v != null) return v.produto.dscProduto;
    }).toList();
  }

  updateProdutosNaoAchados(List<ProdutoListaCompras> produtosNaoEncontrados) {
    String codProdutosNaoEncontrados = '';
    List<Venda> vendasAux = [];
    int index = 0;
    produtosNaoEncontrados.forEach((p) {
      if (!p.codGetin.contains('SEM')) codProdutosNaoEncontrados += p.codGetin;
    });
    vendas.forEach((v) {
      if (v != null && !codProdutosNaoEncontrados.contains(v.produto.codGetin))
        vendasAux.add(v);
      index++;
    });
    vendas = vendasAux;
  }

  @override
  String toString() {
    return '${estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial} - total de itens achados: ${getTotalItensNonNull()} - ${toStringProdutosAchados()} - Valor total: ${getValorTotal()}';
  }
}
