import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Estabelecimento {
  final String numCNPJ;
  final String nomRazaoSocial;
  final String nomFantasia;
  final String numTelefone;
  final String nomLogradouro;
  final String numImovel;
  final String nomBairro;
  final String nomMunicipio;
  final String numCep;
  final double numLatitude;
  final double numLongitude;

  Estabelecimento(
      {this.numCNPJ,
      this.nomRazaoSocial,
      this.nomFantasia,
      this.numTelefone,
      this.nomLogradouro,
      this.numImovel,
      this.nomBairro,
      this.nomMunicipio,
      this.numCep,
      this.numLatitude,
      this.numLongitude});

  String get nome {
    if (nomFantasia != null && nomFantasia.isNotEmpty)
      return nomFantasia;
    else if (nomRazaoSocial != null && nomRazaoSocial.isNotEmpty)
      return nomRazaoSocial;
    return 'Nome não encontrado';
  }

  factory Estabelecimento.fromAPI(response) {
    return Estabelecimento(
      numCep: response['numCep'],
      numCNPJ: response['numCNPJ'],
      numImovel: response['numImovel'],
      numLatitude: response['numLatitude'],
      numLongitude: response['numLongitude'],
      numTelefone: response['numTelefone'],
      nomMunicipio: response['nomMunicipio'],
      nomLogradouro: response['nomLogradouro'],
      nomBairro: response['nomBairro'],
      nomFantasia: utilService
          .getNomeReal(response['nomFantasia'] ?? response['nomRazaoSocial']),
      nomRazaoSocial: response['nomRazaoSocial'],
    );
  }

  factory Estabelecimento.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return Estabelecimento(
      numCep: data['numCep'],
      numCNPJ: data['numCNPJ'],
      numImovel: data['numImovel'],
      numLatitude: data['numLatitude'],
      numLongitude: data['numLongitude'],
      numTelefone: data['numTelefone'],
      nomMunicipio: data['nomMunicipio'],
      nomLogradouro: data['nomLogradouro'],
      nomBairro: data['nomBairro'],
      nomFantasia: utilService
          .getNomeReal(data['nomFantasia'] ?? data['nomRazaoSocial']),
      nomRazaoSocial: data['nomRazaoSocial'],
    );
  }

  Map<String, dynamic> get asMap => {
        'numCep': numCep ?? '',
        'numCNPJ': numCNPJ,
        'numImovel': numImovel ?? '',
        'numLatitude': numLatitude ?? '',
        'numLongitude': numLongitude ?? '',
        'numTelefone': numTelefone ?? '',
        'nomMunicipio': nomMunicipio ?? '',
        'nomLogradouro': nomLogradouro ?? '',
        'nomBairro': nomBairro ?? '',
        'nomFantasia': nomFantasia ?? '',
        'nomRazaoSocial': nomRazaoSocial ?? '',
      };

  @override
  String toString() {
    return '$nomFantasia -  $numCNPJ';
  }
}
