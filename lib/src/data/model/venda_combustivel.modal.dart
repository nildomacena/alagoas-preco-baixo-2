import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:flutter/cupertino.dart';

abstract class TiposCombustivel {
  static const GASOLINA_COMUM = '1';
  static const GASOLINA_ADITIVADA = '2';
  static const ALCOOL = '3';
  static const DIESEL_COMUM = '4';
  static const DIESEL_ADITIVADO = '5';
  static const GNV = '6';
}

class VendaCombustivel {
  final Estabelecimento estabelecimento;
  final String dthEmissaoUltimaVenda;
  final double valUnitarioUltimaVenda;
  final double valUltimaVenda;
  final String tipoCombustivel;
  final String numCNPJ;
  String get descricao {
    switch (tipoCombustivel) {
      case TiposCombustivel.GASOLINA_COMUM:
        return 'Gasolina Comum';
      case TiposCombustivel.GASOLINA_ADITIVADA:
        return 'Gasolina Aditivada';
      case TiposCombustivel.ALCOOL:
        return 'Álcool';
      case TiposCombustivel.DIESEL_ADITIVADO:
        return 'Diesel Aditivado';
      case TiposCombustivel.DIESEL_COMUM:
        return 'Diesel Comum';
      case TiposCombustivel.GNV:
        return 'GNV';
    }
    return '';
  }

  String get preco => valUltimaVenda.toStringAsFixed(2);

  VendaCombustivel(
      { //this.produto,
      @required this.dthEmissaoUltimaVenda,
      @required this.valUnitarioUltimaVenda,
      @required this.valUltimaVenda,
      @required this.estabelecimento,
      @required this.numCNPJ,
      @required this.tipoCombustivel});

  factory VendaCombustivel.fromAPI(response, String tipoCombustivel) {
    return VendaCombustivel(
        estabelecimento: Estabelecimento.fromAPI(response),
        numCNPJ: response['numCNPJ'],
        dthEmissaoUltimaVenda: response['dthEmissaoUltimaVenda'],
        valUltimaVenda: response['valUltimaVenda'].toDouble(),
        valUnitarioUltimaVenda: response['valUnitarioUltimaVenda'].toDouble(),
        tipoCombustivel: tipoCombustivel);
  }

  @override
  String toString() {
    return 'Descricao: $descricao - Preço: $preco ';
  }
}
