import 'package:aldescontos/src/data/model/produto.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

class ProdutoListaCompras {
  final String codGetin;
  final String dscProduto;
  final double valUltimaVenda;
  final String dataConsulta;
  final String estabelecimento;

  ProdutoListaCompras(
      {@required this.codGetin,
      @required this.dscProduto,
      @required this.valUltimaVenda,
      @required this.dataConsulta,
      @required this.estabelecimento})
      : assert(codGetin != null &&
            dscProduto != null &&
            valUltimaVenda != null &&
            dataConsulta != null &&
            estabelecimento != null);

  String get dataFormatada {
    String hora = dataConsulta.substring(11, 16);
    String dia = dataConsulta.substring(8, 10);
    String mes = dataConsulta.substring(5, 7);
    String ano = dataConsulta.substring(0, 4);
    return '$dia/$mes/$ano - $hora';
  }

  factory ProdutoListaCompras.fromAPI(response) {
    return ProdutoListaCompras(
        codGetin: response['codGetin'],
        dscProduto: response['dscProduto'] ?? '',
        valUltimaVenda: response['valUltimaVenda'],
        dataConsulta: response['dataConsulta'],
        estabelecimento: response['estabelecimento']);
  }

  factory ProdutoListaCompras.fromFirestore(DocumentSnapshot doc) {
    dynamic data = doc.data;
    return ProdutoListaCompras(
        codGetin: data['codGetin'],
        dscProduto: data['dscProduto'] ?? '',
        valUltimaVenda: data['valUltimaVenda'],
        dataConsulta: data['dataConsulta'],
        estabelecimento: data['estabelecimento']);
  }

  @override
  String toString() {
    return '$dscProduto - R\$${valUltimaVenda.toStringAsFixed(2)}';
  }
}
