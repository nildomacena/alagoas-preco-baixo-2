import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class UtilService {
  LocationData locationData;
  String get bannerId => !kReleaseMode
      ? 'ca-app-pub-3940256099942544/6300978111'
      : 'ca-app-pub-8822334834267652/6088349053';
  String get bannerCombustiveisId => !kReleaseMode
      ? 'ca-app-pub-3940256099942544/6300978111'
      : 'ca-app-pub-8822334834267652/7903487047';
  String get bannerEntreListaId => !kReleaseMode
      ? 'ca-app-pub-3940256099942544/6300978111'
      : 'ca-app-pub-8822334834267652/5944878822';
  String get interstitialId => !kReleaseMode
      ? 'ca-app-pub-3940256099942544/1033173712'
      : 'ca-app-pub-8822334834267652/5565125948';
  String get interstitialListaComprasId => !kReleaseMode
      ? 'ca-app-pub-3940256099942544/1033173712'
      : 'ca-app-pub-8822334834267652/9752789178';

  String get interstitialCombustiveisId => !kReleaseMode
      ? 'ca-app-pub-3940256099942544/1033173712'
      : 'ca-app-pub-8822334834267652/7356692134';

  String getNomeReal(String nomFantasia) {
    if (nomFantasia == 'SENDAS DISTRIBUIDORA S/A')
      return 'ASSAÍ ATACADISTA';
    else if (nomFantasia == 'COMPANHIA BRASILEIRA DE DISTRIBUICAO')
      return 'HIPERMERCADO EXTRA';
    else if (nomFantasia == 'ESPECIARYA INDUSTRIA E COMERCIO LTDA')
      return 'PALATO';
    else
      return nomFantasia;
  }

  String formatarCNPJ(String cnpj) {
    String cnpjFormatado;
    try {
      cnpjFormatado =
          '${cnpj.substring(0, 2)}.${cnpj.substring(2, 5)}.${cnpj.substring(5, 8)}/${cnpj.substring(8, 12)}-${cnpj.substring(12, 14)}';
    } catch (e) {
      print('Erro formatação CNPJ: $e');
      cnpjFormatado = '';
    }
    return cnpjFormatado;
  }

  void snackBarErro({String titulo, String mensagem}) {
    Get.snackbar(
      titulo ?? 'Erro',
      mensagem ?? 'Erro durante a operação',
      snackPosition: SnackPosition.TOP,
      backgroundColor: Colors.red,
      colorText: Colors.white,
      margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
      duration: Duration(seconds: 5),
    );
  }

  void snackBar(
      {@required String titulo,
      @required String mensagem,
      SnackPosition snackPosition,
      Function action}) {
    Get.snackbar(
      titulo,
      mensagem,
      snackPosition: snackPosition ?? SnackPosition.TOP,
      backgroundColor: Colors.white,
      //colorText: Colors.white,
      margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
      duration: Duration(seconds: 5),
    );
  }

  showAlert(String titulo, String mensagem,
      {Function action, String actionLabel}) {
    return Get.dialog(
      AlertDialog(
        title: Text(titulo),
        content: Container(
          child: Text(mensagem),
        ),
        actions: [
          TextButton(
              onPressed: () {
                Get.back();
              },
              child: Text('CANCELAR')),
          if (action != null)
            TextButton(onPressed: action, child: Text(actionLabel)),
        ],
      ),
    );
  }

  Future<bool> alertConfirma({String titulo, String mensagem}) async {
    var result = await Get.dialog(AlertDialog(
      title: Text(titulo ?? 'CONFIRMAÇÃO'),
      content: Text(mensagem ?? 'Confirma realmente essa ação?'),
      actions: [
        TextButton(
          child: Text('CONFIRMAR'),
          onPressed: () {
            Get.back(result: true);
          },
        )
      ],
    ));
    return result != null && result;
  }

  void showAlertCarregando([String mensagem]) {
    Get.dialog(
        AlertDialog(
            content: Container(
                height: 80,
                child: Column(children: <Widget>[
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  Text(
                    mensagem ?? 'Fazendo consulta...',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Get.theme.primaryColor),
                  )
                ]))),
        barrierDismissible: false);
  }

  MaterialStateProperty<Color> colorButton(MaterialColor color) {
    return MaterialStateProperty.all<Color>(color);
  }

  Widget columnCarregando([String mensagem]) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircularProgressIndicator(
              /* backgroundColor: Colors.green[800],
            valueColor: AlwaysStoppedAnimation<Color>(Colors.green), */
              ),
          Container(
            margin: EdgeInsets.only(top: 10, right: 10, left: 10),
            child: Text(
              mensagem != null ? mensagem : 'Carregando...',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.blue[800]),
            ),
          )
        ],
      ),
    );
  }

  Future<String> scanBarCode() async {
    String code = await BarcodeScanner.scan();
    if (code[0] == '0') {
      code = code.substring(1);
    }
    if (code.length < 13) throw 'erro-leitura';
    return code;
  }
}

UtilService utilService = UtilService();
