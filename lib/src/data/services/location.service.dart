import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocationService {
  SharedPreferences prefs;
  final Location location = Location();
  LocationData _locationData;
  bool _serviceEnabled;

  LocationService() {}

  Future<LocationData> carregarUltimaLocalizacao() async {
    LocationData locationData;
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
    if (prefs.getString('ult_lat') != null &&
        prefs.getString('ult_lat').isNotEmpty &&
        prefs.getString('ult_lng') != null &&
        prefs.getString('ult_lng').isNotEmpty) {
      locationData = LocationData.fromMap({
        'latitude': double.parse(prefs.getString('ult_lat')),
        'longitude': double.parse(prefs.getString('ult_lng'))
      });
      print('prefs.getString(ult_lat): ${prefs.getString('ult_lat')}');
      //Se acha a última localização, avisa que achou
      Get.snackbar('Localização não encontrada',
          'Utilizando a última localização registrada',
          snackPosition: SnackPosition.BOTTOM,
          duration: Duration(seconds: 5),
          mainButton: TextButton(
              onPressed: () {
                getLocationData();
              },
              child: Text('Tentar Novamente')));
    } else {
      //Senão, carrega a localizacao de maceio
      locationData = LocationData.fromMap({
        'latitude': -9.6432331,
        'longitude': -35.7190686,
        'accuracy': 5.36,
        'speed': 0.0,
        'speedAccuracy': 1.66,
        'heading': 0.0,
        'time': DateTime.now().millisecondsSinceEpoch.toDouble()
      });

      Get.snackbar('Localização não encontrada',
          'Utilizando a parte alta de Maceió como localização',
          snackPosition: SnackPosition.BOTTOM,
          duration: Duration(seconds: 4),
          mainButton: TextButton(
              onPressed: () {
                getLocationData();
              },
              child: Text('Tentar Novamente')));
    }

    print('ultima locationData: $locationData');
    return locationData;
  }

  Future<void> definirUltimaLocalizacao(
      double latitude, double longitude) async {
    if (prefs == null) prefs = await SharedPreferences.getInstance();
    await prefs.setString('ult_lat', _locationData.latitude.toString());
    await prefs.setString('ult_lng', _locationData.longitude.toString());
    return;
  }

  LocationData getLocationMaceio() {
    return LocationData.fromMap({
      'latitude': -9.6432331,
      'longitude': -35.7190686,
      'accuracy': 5.36,
      'speed': 0.0,
      'speedAccuracy': 1.66,
      'heading': 0.0,
      'time': DateTime.now().millisecondsSinceEpoch.toDouble()
    });
  }

  Future<LocationData> getLocationData() async {
    if (kDebugMode) {
      return LocationData.fromMap({
        'latitude': -9.6432331,
        'longitude': -35.7190686,
        'accuracy': 5.36,
        'speed': 0.0,
        'speedAccuracy': 1.66,
        'heading': 0.0,
        'time': DateTime.now().millisecondsSinceEpoch.toDouble()
      });
    }
    print('getLocationData()');
    if (_locationData != null) return _locationData;
    try {
      //return location.getLocation();
      //Verifica se o GPS está ligado
      PermissionStatus permissionStatus = await location.hasPermission();
      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        //Se não estiver ligado, solicitar ao usuário ligar o GPS
        _serviceEnabled = await location.requestService();

        if (!_serviceEnabled) {
          //Se ainda assim o usuário não ligar o GPS, tenta carregar a última localização
          print('_locationData = await carregarUltimaLocalizacao();');
          _locationData = await carregarUltimaLocalizacao();
        }
      }
      //Se o GPS estiver ligado, verificar se possui autorização do usuário
      if (permissionStatus == PermissionStatus.DENIED ||
          permissionStatus == PermissionStatus.DENIED_FOREVER) {
        /* Get.snackbar('Permissão negada',
            'Para utilizar nossos serviços, é necessário conceder permissão para acessar a localização do dispositivo',
            snackPosition: SnackPosition.BOTTOM,
            margin: EdgeInsets.only(bottom: 10, left: 5, right: 5)); */
        permissionStatus = await location.requestPermission();
        if (permissionStatus == PermissionStatus.DENIED ||
            permissionStatus == PermissionStatus.DENIED_FOREVER) {
          Get.snackbar('Erro',
              'Não foi possível encontrar a localização atual do dispositivo. Tentando buscar a última localização registrada.',
              snackPosition: SnackPosition.BOTTOM,
              margin: EdgeInsets.only(bottom: 10, left: 5, right: 5));
          _locationData = await carregarUltimaLocalizacao();
        }
      }
      print('_locationData antes de terminar a funçao: $_locationData');
      //Se o fluxo for normal, busca a localização e pronto
      if (_locationData == null) _locationData = await location.getLocation();
      if (_locationData != null &&
          _locationData.latitude != null &&
          _locationData.longitude != null) {
        definirUltimaLocalizacao(
            _locationData.latitude, _locationData.longitude);
      }
      return _locationData;
    } catch (e) {
      print('Erro: $e');
      Get.snackbar('Erro',
          'Ocorreu um erro ao buscar a localização. Ligue o GPS e Tente novamente mais tarde',
          snackPosition: SnackPosition.BOTTOM,
          margin: EdgeInsets.only(bottom: 10, left: 5, right: 5));
    }

/* try {
      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          _locationData = await carregarUltimaLocalizacao();
          if (_locationData == null)
            Get.snackbar('Erro', 'Erro ao carregar localização',
                snackPosition: SnackPosition.BOTTOM,
                mainButton: TextButton(
                    onPressed: () {
                      getLocation();
                    },
                    child: Text('Tentar Novamente')),
                margin: EdgeInsets.only(bottom: 10));
          else
            Get.snackbar('Localização não encontrada',
                'Utilizando a última localização registrada',
                snackPosition: SnackPosition.BOTTOM,
                mainButton: TextButton(
                    onPressed: () {
                      getLocation();
                    },
                    child: Text('Tentar Novamente')),
                margin: EdgeInsets.only(bottom: 10, left: 5, right: 5));
          carregando = false;
          update();
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != PermissionStatus.granted) {
          return;
        }
      }
} */
  }
}

LocationService locationService = LocationService();
