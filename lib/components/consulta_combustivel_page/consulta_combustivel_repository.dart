import 'package:aldescontos/src/data/model/estabelecimentos_combustivel.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:meta/meta.dart';

class ConsultaCombustivelRepository {
  final ApiProvider apiProvider;

  ConsultaCombustivelRepository({@required this.apiProvider})
      : assert(apiProvider != null);

  Future<List<EstabelecimentoCombustivel>> consultaCombustivel() {
    return apiProvider.buscarPrecosCombustivel();
  }
}
