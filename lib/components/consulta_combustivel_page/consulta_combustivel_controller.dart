import 'package:aldescontos/components/consulta_combustivel_page/consulta_combustivel_repository.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/estabelecimentos_combustivel.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:map_launcher/map_launcher.dart';

class ConsultaCombustivelController extends GetxController {
  List<EstabelecimentoCombustivel> estabelecimentosCombustivel = [];
  List<EstabelecimentoCombustivel> estabelecimentosFiltrados = [];
  final ConsultaCombustivelRepository repository;
  int raio = 15;
  String ordernarPor = 'gasComum';
  bool carregou = false;
  bool erro = false;
  ConsultaCombustivelController({@required this.repository})
      : assert(repository != null);

  @override
  onInit() {
    consultaCombustivel();
    super.onInit();
  }

  definirOrdem(String ordem) {
    ordernarPor = ordem;
    switch (ordem) {
      case 'gasComum':
        estabelecimentosFiltrados.sort((a, b) {
          if (a.gasolinaComum == null) return 1;
          if (b.gasolinaComum == null) return -1;
          return a.gasolinaComum?.valUltimaVenda
              .compareTo(b.gasolinaComum?.valUltimaVenda);
        });
        break;
      case 'gasAditivada':
        estabelecimentosFiltrados.sort((a, b) {
          if (a.gasolinaAditivada == null) return 1;
          if (b.gasolinaAditivada == null) return -1;
          return a.gasolinaAditivada.valUltimaVenda
              .compareTo(b.gasolinaAditivada?.valUltimaVenda);
        });
        break;
      case 'alcool':
        estabelecimentosFiltrados.sort((a, b) {
          if (a.alcool == null) return 1;
          if (b.alcool == null) return -1;
          return a.alcool.valUltimaVenda.compareTo(b.alcool.valUltimaVenda);
        });
        break;
      case 'gnv':
        estabelecimentosFiltrados.sort((a, b) {
          if (a.gnv == null) return 1;
          if (b.gnv == null) return -1;
          return a.gnv.valUltimaVenda.compareTo(b.gnv.valUltimaVenda);
        });
        break;
      case 'distancia':
        estabelecimentosFiltrados.sort((a, b) {
          if (a.distanceBetweenPoints == null) return 1;
          if (b.distanceBetweenPoints == null) return -1;
          return a.distanceBetweenPoints.compareTo(b.distanceBetweenPoints);
        });
        break;
    }
    update();
  }

  consultaCombustivel() async {
    erro = false;
    carregou = false;
    update();
    try {
      estabelecimentosFiltrados =
          estabelecimentosCombustivel = await repository.consultaCombustivel();
      carregou = true;
      print('estabelecimentosCombustivel: $estabelecimentosCombustivel');
      update();
    } catch (e) {
      print('Erro durante a consulta: $e');
      erro = true;
      carregou = true;
      update();
    }
  }

  void verNoMapa(Estabelecimento estabelecimento) async {
    final availableMaps = await MapLauncher.installedMaps;
    double latitude;
    double longitude;
    String keyword =
        estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial;

    latitude = estabelecimento.numLatitude;
    longitude = estabelecimento.numLongitude;
    /*  keyword = keyword +
        ' ${venda.estabelecimento.nomLogradouro}' +
        '${venda.estabelecimento.nomBairro}';
    Response response = await dio.post(
      'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${venda.estabelecimento.numLatitude},${venda.estabelecimento.numLongitude}&radius=10000&type=grocery_or_supermarket&keyword=$keyword&key=AIzaSyAEBDMTgvnQx6mut8Pw_gGsiEmRkLZziu4',
    );
    print(
        '${venda.estabelecimento.numLatitude},${venda.estabelecimento.numLongitude} - ${venda.estabelecimento.nomFantasia}');
    print('response google places $response');
    if (response.data['status'] == 'OK') {
      latitude = response.data['results'][0]['geometry']['location']['lat'];
      longitude = response.data['results'][0]['geometry']['location']['lng'];
    } else {
      latitude = venda.estabelecimento.numLatitude;
      longitude = venda.estabelecimento.numLongitude;
    } */

    await availableMaps.first.showMarker(
      coords: Coords(latitude, longitude),
      title: estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial,
      description: estabelecimento.nomFantasia,
    );
    /* setState(() {
      _selectedChoices = choice;
    }); */
  }

  setRaio(double value) {
    raio = value.toInt();
    print('setRaio: $raio');
    estabelecimentosFiltrados = estabelecimentosCombustivel
        .where((element) => element.distanceBetweenPoints <= value * 1000)
        .toList();
    definirOrdem(ordernarPor);
    update();
  }
}
