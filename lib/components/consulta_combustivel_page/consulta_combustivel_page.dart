import 'package:admob_flutter/admob_flutter.dart';
import 'package:aldescontos/components/consulta_combustivel_page/consulta_combustivel_controller.dart';
import 'package:aldescontos/components/consulta_combustivel_page/custom_widgets/card_estabelecimento.dart';
import 'package:aldescontos/src/data/model/estabelecimentos_combustivel.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:get/get.dart';

class ConsultaCombustivelPage extends StatelessWidget {
  final ConsultaCombustivelController controller = Get.find();

  Widget rowFiltros() {
    ButtonStyle buttonStyle = OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: Colors.teal,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))));
    return GetBuilder<ConsultaCombustivelController>(builder: (_) {
      return Container(
        padding: EdgeInsets.only(top: 10),
        width: Get.width,
        height: 240,
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
                child: Text(
                  'Ordenar por',
                  style: TextStyle(color: Colors.grey),
                )),
            Divider(),
            Wrap(
              alignment: WrapAlignment.center,
              /* scrollDirection: Axis.horizontal,
            shrinkWrap: true, */
              children: [
                Container(
                  height: 30,
                  width: 100,
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: OutlinedButton(
                    style: OutlinedButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: controller.ordernarPor == 'gasComum'
                            ? Colors.teal
                            : Colors.white,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)))),
                    onPressed: () {
                      controller.definirOrdem('gasComum');
                    },
                    child: Text(
                      'GAS. COMUM',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.ordernarPor == 'gasComum'
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ),
                Container(
                  height: 30,
                  width: 100,
                  margin: EdgeInsets.only(left: 5, right: 5),
                  child: OutlinedButton(
                    onPressed: () {
                      controller.definirOrdem('gasAditivada');
                    },
                    style: OutlinedButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor:
                            controller.ordernarPor == 'gasAditivada'
                                ? Colors.teal
                                : Colors.white,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)))),
                    child: Text(
                      'GAS. ADITIVADA',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.ordernarPor == 'gasAditivada'
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ),
                Container(
                  height: 30,
                  width: 100,
                  margin: EdgeInsets.only(
                    left: 5,
                    right: 5,
                  ),
                  child: OutlinedButton(
                    onPressed: () {
                      controller.definirOrdem('gnv');
                    },
                    style: OutlinedButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: controller.ordernarPor == 'gnv'
                            ? Colors.teal
                            : Colors.white,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)))),
                    child: Text(
                      'GNV',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.ordernarPor == 'gnv'
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ),
                Container(
                  height: 30,
                  width: 100,
                  margin: EdgeInsets.only(left: 5, right: 5, top: 10),
                  child: OutlinedButton(
                    onPressed: () {
                      controller.definirOrdem('alcool');
                    },
                    style: OutlinedButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: controller.ordernarPor == 'alcool'
                            ? Colors.teal
                            : Colors.white,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)))),
                    child: Text(
                      'ÁLCOOL',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.ordernarPor == 'alcool'
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ),
                Container(
                  height: 30,
                  width: 100,
                  margin: EdgeInsets.only(left: 5, right: 5, top: 10),
                  child: OutlinedButton(
                    onPressed: () {
                      controller.definirOrdem('distancia');
                    },
                    style: OutlinedButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: controller.ordernarPor == 'distancia'
                            ? Colors.teal
                            : Colors.white,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)))),
                    child: Text(
                      'DISTÂNCIA',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12,
                          color: controller.ordernarPor == 'distancia'
                              ? Colors.white
                              : Colors.black),
                    ),
                  ),
                ),
              ],
            ),
            Container(
                margin: EdgeInsets.only(top: 15),
                child: Text(
                  'Distância: ${controller.raio}KM',
                  style: TextStyle(color: Colors.grey),
                )),
            Container(
              child: FlutterSlider(
                values: [controller.raio.toDouble()],
                step: FlutterSliderStep(step: 1),
                min: 1,
                max: 15,
                tooltip: FlutterSliderTooltip(
                  positionOffset: FlutterSliderTooltipPositionOffset(top: 100),
                  direction: FlutterSliderTooltipDirection.top,
                  textStyle: TextStyle(fontSize: 12, color: Colors.white),
                  boxStyle: FlutterSliderTooltipBox(
                      decoration: BoxDecoration(
                          color: Colors.blueAccent.withOpacity(0.7))),
                ),
                handlerAnimation: FlutterSliderHandlerAnimation(
                    curve: Curves.elasticOut,
                    reverseCurve: Curves.bounceIn,
                    duration: Duration(milliseconds: 500),
                    scale: 1.5),
                onDragCompleted: (handlerIndex, lowerValue, upperValue) {
                  controller.setRaio(lowerValue);
                  print(
                      'handlerIndex: $handlerIndex - lowerValue: $lowerValue - upperValue: $upperValue');
                },
              ),
            )
          ],
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Consulta Combustível'),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            child: GetBuilder<ConsultaCombustivelController>(
              builder: (_) {
                print('build Getbuilder');
                if (!controller.carregou)
                  return utilService
                      .columnCarregando('Carregando Preços dos Combustiveis');
                else if (controller.erro)
                  return Container(
                    width: Get.width,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Ocorreu um erro durante a consulta.',
                          style: TextStyle(fontSize: 16),
                        ),
                        ElevatedButton.icon(
                            icon: Icon(Icons.refresh),
                            onPressed: controller.consultaCombustivel,
                            label: Text('TENTAR NOVAMENTE'))
                      ],
                    ),
                  );
                if (controller.estabelecimentosFiltrados.isEmpty) {
                  return Column(
                    children: [
                      rowFiltros(),
                      Center(
                        child: Text(
                          'Não foram encontrados resultados para esses filtros.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.blue,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  );
                }
                return ListView.builder(
                    itemCount: controller.estabelecimentosFiltrados.length + 2,
                    itemBuilder: (context, index) {
                      if (index == 0) return rowFiltros();
                      if (index ==
                          controller.estabelecimentosFiltrados.length + 1)
                        return Padding(
                          padding: EdgeInsets.all(20),
                        );
                      EstabelecimentoCombustivel estabelecimentoCombustivel =
                          controller.estabelecimentosFiltrados[index - 1];
                      return CardEstabelecimento(
                          estabelecimentoCombustivel, controller);
                      return ListTile(
                        title: Text(
                            estabelecimentoCombustivel.estabelecimento.nome),
                        subtitle: Text(estabelecimentoCombustivel
                                    .gasolinaComum !=
                                null
                            ? 'Gasolina: ${estabelecimentoCombustivel.gasolinaComum.valUnitarioUltimaVenda.toStringAsFixed(2)}'
                            : ''),
                      );
                    });
              },
            ),
          ),
          /* Positioned(
              bottom: 0,
              child: Container(
                width: Get.width,
                child: AdmobBanner(
                  adUnitId: utilService.bannerCombustiveisId,
                  adSize: AdmobBannerSize.FULL_BANNER,
                  onBannerCreated: (controller) {
                    print('onBannerCreated: $controller');
                  },
                ),
              )), */
        ],
      ),
    );
  }
}
