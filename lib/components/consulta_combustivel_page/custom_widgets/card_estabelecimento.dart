import 'package:aldescontos/components/consulta_combustivel_page/consulta_combustivel_controller.dart';
import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_controller.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/estabelecimentos_combustivel.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class CardEstabelecimento extends StatelessWidget {
  final EstabelecimentoCombustivel estabelecimentoCombustivel;
  final ConsultaCombustivelController controller;
  double distanceBetweenPoints;

  bool pesquisa;
  CardEstabelecimento(this.estabelecimentoCombustivel, this.controller,
      {this.pesquisa}) {
    if (pesquisa == null) pesquisa = false;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(bottom: 20, top: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15),
                  height: 50,
                  color: Colors.blue,
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: AutoSizeText(
                    '${estabelecimentoCombustivel.estabelecimento.nomFantasia ?? estabelecimentoCombustivel.estabelecimento.nomRazaoSocial}',
                    textAlign: TextAlign.center,
                    minFontSize: 15,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(top: 4, bottom: 4),
              width: double.infinity,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  if (estabelecimentoCombustivel.gasolinaComum != null)
                    Container(
                      padding: EdgeInsets.all(1),
                      width: Get.width,
                      child: Text(
                          "Gasolina comum: R\$${estabelecimentoCombustivel.gasolinaComum.preco}",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w400)),
                    ),
                  if (estabelecimentoCombustivel.gasolinaAditivada != null)
                    Container(
                      padding: EdgeInsets.all(1),
                      width: Get.width,
                      child: Text(
                          "Gasolina Aditivada: R\$${estabelecimentoCombustivel.gasolinaAditivada.preco}",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w400)),
                    ),
                  if (estabelecimentoCombustivel.alcool != null)
                    Container(
                      padding: EdgeInsets.all(1),
                      width: Get.width,
                      child: Text(
                          "Álcool: R\$${estabelecimentoCombustivel.alcool.preco}",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w400)),
                    ),
                  if (estabelecimentoCombustivel.gnv != null)
                    Container(
                      padding: EdgeInsets.all(1),
                      width: Get.width,
                      child: Text(
                          "GNV: R\$${estabelecimentoCombustivel.gnv.preco}",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w400)),
                    ),
                  /* Divider(),
                  Text(
                      '${estabelecimentoCombustivel.estabelecimento.numTelefone ?? ''}',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w300)),
                  Padding(padding: EdgeInsets.all(1)), */
                  /*  if (!pesquisa)
                    TextButton(
                      child: Text(
                        'PESQUISAR PRODUTO NESSE ESTABELECIMENTO',
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline),
                      ),
                      onPressed: () async {
                        Get.offAllNamed(Routes.INITIAL, arguments: {
                          'estabelecimento':
                              estabelecimentoCombustivel.estabelecimento
                        });
                      },
                    ), */
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
              //height: 85,
              color: Colors.blue[200],
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onLongPress: () {
                        print(
                            estabelecimentoCombustivel.estabelecimento.numCNPJ);
                      },
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AutoSizeText(
                              '${estabelecimentoCombustivel.estabelecimento.nomLogradouro},${estabelecimentoCombustivel.estabelecimento.numImovel ?? ''}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            AutoSizeText(
                              'DISTÂNCIA: ${estabelecimentoCombustivel.distancia}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            if (estabelecimentoCombustivel
                                    .estabelecimento.nomBairro !=
                                null)
                              AutoSizeText(
                                '${estabelecimentoCombustivel.estabelecimento.nomBairro}',
                                minFontSize: 11,
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 5),
                    child: OutlinedButton(
                        child: Text(
                          'VER NO MAPA',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          controller.verNoMapa(
                              estabelecimentoCombustivel.estabelecimento);
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
