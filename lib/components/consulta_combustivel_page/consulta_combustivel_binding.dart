import 'package:aldescontos/components/consulta_combustivel_page/consulta_combustivel_controller.dart';
import 'package:aldescontos/components/consulta_combustivel_page/consulta_combustivel_repository.dart';
import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_controller.dart';
import 'package:get/get.dart';

class ConsultaCombustiveisBinding implements Bindings {
  @override
  void dependencies() {
    print('dependencies homebinding');
    Get.lazyPut<ConsultaCombustivelController>(() =>
        ConsultaCombustivelController(
            repository:
                ConsultaCombustivelRepository(apiProvider: Get.find())));
  }
}
