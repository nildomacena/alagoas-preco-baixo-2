import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_controller.dart';
import 'package:aldescontos/components/custom_widgets/custom_drawer.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'custom_widgets/card_estabelecimento.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EstabelecimentosFavoritosPage extends StatelessWidget {
  final EstabelecimentosFavoritosController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    print('build EstabelecimentosFavoritosPage');
    return WillPopScope(
      onWillPop: () async {
        Get.offAllNamed(Routes.INITIAL);
        return false;
      },
      child: Scaffold(
        //drawer: CustomDrawer(controller: Get.find()),
        appBar: AppBar(
            leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.offAllNamed(Routes.INITIAL);
                }),
            title: Text('Estabelecimentos Favoritos')),
        body: Container(
          child: Stack(
            fit: StackFit.expand,
            children: [
              GetBuilder<EstabelecimentosFavoritosController>(builder: (_) {
                if (_.favoritos.isEmpty) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Você ainda não salvou estabelecimentos favoritos",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 25, right: 25),
                        child: ElevatedButton(
                          child: Text(
                            "CLIQUE AQUI PARA PESQUISAR ESTABELECIMENTOS",
                            textAlign: TextAlign.center,
                          ),
                          onPressed: controller.pesquisarEstabelecimento,
                        ),
                      ),
                    ],
                  );
                }
                return Container(
                  child: ListView.builder(
                      itemCount: controller.favoritos.length + 1,
                      itemBuilder: (context, index) {
                        if (index == controller.favoritos.length)
                          return Container(
                            padding: EdgeInsets.all(20),
                          );
                        return CardEstabelecimento(
                            controller.favoritos[index], controller);
                      }),
                );
              }),
              Positioned(
                bottom: 10,
                child: Container(
                  padding: EdgeInsets.only(left: 25, right: 25),
                  width: Get.width,
                  child: ElevatedButton(
                      child: Text('PESQUISAR ESTABELECIMENTOS'),
                      onPressed: controller.pesquisarEstabelecimento),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
