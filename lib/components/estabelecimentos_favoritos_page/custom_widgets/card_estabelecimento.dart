import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_controller.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class CardEstabelecimento extends StatelessWidget {
  final Estabelecimento estabelecimento;
  final EstabelecimentosFavoritosController controller;
  bool pesquisa;
  CardEstabelecimento(this.estabelecimento, this.controller, {this.pesquisa}) {
    if (pesquisa == null) pesquisa = false;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(bottom: 20, top: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15),
                  height: 50,
                  color: Colors.blue,
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: AutoSizeText(
                    '${estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial}',
                    textAlign: TextAlign.center,
                    minFontSize: 15,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                if (!pesquisa)
                  Positioned(
                    right: 5,
                    child: IconButton(
                        icon: Icon(
                          Icons.delete,
                          color: Colors.red[500],
                        ),
                        onPressed: () async {
                          controller
                              .deletarEstabelecimentoFavorito(estabelecimento);
                          /* print('delete');
                        bool confirma = await showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('Confirmação'),
                                content: Text(
                                    'Deseja realmente excluir o ${estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial}?'),
                                actions: <Widget>[
                                  TextButton(
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                      child: Text('NÃO')),
                                  TextButton(
                                      onPressed: () async {
                                        Navigator.pop(context, true);
                                      },
                                      child: Text('SIM'))
                                ],
                              );
                            });
                        if (confirma) {
                          try {
                            await firebaseService
                                .deletarEstabelecimentoFavorito(
                                    estabelecimento);
                            Toast.show('Estabelecimento removido dos favoritos',
                                context);
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        EstabelecimentosFavoritos(user)));
                          } catch (e) {}
                        } else
                          return; */
                        }),
                  )
              ],
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(top: 4, bottom: 4),
              width: double.infinity,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                      "${estabelecimento.nomLogradouro}${estabelecimento.numImovel != null ? ', ' + estabelecimento.numImovel : ''}",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Padding(padding: EdgeInsets.all(1)),
                  Text('${estabelecimento.numTelefone ?? ''}',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w300)),
                  Padding(padding: EdgeInsets.all(1)),
                  if (!pesquisa)
                    TextButton(
                      child: Text(
                        'PESQUISAR PRODUTO NESSE ESTABELECIMENTO',
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline),
                      ),
                      onPressed: () async {
                        Get.offAllNamed(Routes.INITIAL,
                            arguments: {'estabelecimento': estabelecimento});
                      },
                    ),
                  if (pesquisa)
                    TextButton(
                      child: Text(
                        'SALVAR ESTABELECIMENTO COMO FAVORITO',
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline),
                      ),
                      onPressed: () async {
                        controller.favoritarEstabelecimento(estabelecimento);
                      },
                    ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
              //height: 85,
              color: Colors.blue[200],
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onLongPress: () {
                        print(estabelecimento.numCNPJ);
                      },
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AutoSizeText(
                              '${utilService.formatarCNPJ(estabelecimento.numCNPJ)}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            AutoSizeText(
                              'CEP: ${estabelecimento.numCep}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            if (estabelecimento.nomBairro != null)
                              AutoSizeText(
                                '${estabelecimento.nomBairro}',
                                minFontSize: 11,
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                      icon: Icon(Octicons.location),
                      onPressed: () {
                        controller.verNoMapa(estabelecimento);
                      }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
