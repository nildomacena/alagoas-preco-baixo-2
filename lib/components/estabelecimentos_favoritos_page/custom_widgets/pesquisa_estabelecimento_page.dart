import 'package:aldescontos/components/estabelecimentos_favoritos_page/custom_widgets/card_estabelecimento.dart';
import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PesquisaEstabelecimentosPage extends StatelessWidget {
  final EstabelecimentosFavoritosController controller = Get.find();

  Widget widgetPesquisa() {
    return GetBuilder<EstabelecimentosFavoritosController>(builder: (_) {
      print('build pesquisados: ${_.pesquisados}');

      return Column(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: Card(
              child: TextFormField(
                  maxLines: 1,
                  controller: controller.textEditingController,
                  keyboardType: TextInputType.text,
                  focusNode: controller.focusNode,
                  //autofocus: true,
                  textInputAction: TextInputAction.search,
                  validator: (value) {
                    if (value.isEmpty) return "Campo obrigatório";
                    if (value.length < 4)
                      return "Digite uma pesquisa mais completa";
                  },
                  onFieldSubmitted: controller.onPesquisar,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: controller.resetPesquisa,
                      ),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      hintText: 'Digite o nome do estabelecimento',
                      hintStyle: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.w300),
                      border: UnderlineInputBorder())),
            ),
          ),
          Container(
            width: Get.width,
            padding: EdgeInsets.only(left: 25, right: 25),
            child: ElevatedButton(
              onPressed: controller.onPesquisar,
              child: Text('PESQUISAR'),
            ),
          ),
        ],
      );
    });
  }

  Widget listPesquisados() {
    return GetBuilder<EstabelecimentosFavoritosController>(builder: (_) {
      if (controller.pesquisados != null && controller.pesquisados.isEmpty)
        return Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(15, 30, 15, 0),
                child: Text(
                  "Nenhum resultado para os filtros utilizados",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              Container(
                child: ElevatedButton(
                  child: Text('Pesquisar novamente'),
                  onPressed: controller.resetPesquisa,
                ),
              )
            ],
          ),
        );
      return ListView.builder(
          itemCount: controller.pesquisados.length + 1,
          itemBuilder: (context, index) {
            if (index == 0)
              return Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.only(left: 25, right: 25),
                child: ElevatedButton(
                  onPressed: controller.resetPesquisa,
                  child: Text('LIMPAR PESQUISA'),
                ),
              );
            return CardEstabelecimento(
              controller.pesquisados[index - 1],
              controller,
              pesquisa: true,
            );
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pesquisar Estabelecimentos'),
      ),
      body: Container(child: GetBuilder<EstabelecimentosFavoritosController>(
        builder: (_) {
          print('build pesquisados: ${_.pesquisados}');
          if (_.pesquisados == null)
            return widgetPesquisa();
          else
            return listPesquisados();
        },
      )),
    );
  }
}
