import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:aldescontos/src/data/provider/user.provider.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class EstabelecimentosFavoritosRepository {
  final ApiProvider apiProvider;
  final UserProvider userProvider = Get.find();

  EstabelecimentosFavoritosRepository({@required this.apiProvider})
      : assert(apiProvider != null);

  Future<List<Estabelecimento>> deletarEstabelecimentoFavorito(
      Estabelecimento estabelecimento) {
    return apiProvider.deletarEstabelecimentoFavorito(estabelecimento);
  }

  Future<List<Estabelecimento>> pesquisaEstabelecimentos(String pesquisa) {
    return apiProvider.pesquisarEstabelecimentos(pesquisa);
  }

  Future<List<Estabelecimento>> favoritarEstabelecimento(
      Estabelecimento estabelecimento) {
    return userProvider.favoritarEstabelecimento(estabelecimento);
  }
}
