import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_repository.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:map_launcher/map_launcher.dart';
import 'custom_widgets/pesquisa_estabelecimento_page.dart';

class EstabelecimentosFavoritosController extends GetxController {
  EstabelecimentosFavoritosRepository repository =
      Get.put(EstabelecimentosFavoritosRepository(apiProvider: Get.find()));
  List<Estabelecimento> favoritos;
  List<Estabelecimento> pesquisados;
  TextEditingController textEditingController = TextEditingController();
  FocusNode focusNode = FocusNode();

  @override
  void onInit() {
    super.onInit();
    favoritos = Get.arguments['favoritos'];
  }

  deletarEstabelecimentoFavorito(Estabelecimento estabelecimento) async {
    bool excluir = await Get.dialog(AlertDialog(
      title: Text('Confirmação'),
      content: Text(
          'Deseja realmente excluir o estabelecimento ${estabelecimento.nomFantasia}?'),
      actions: [
        TextButton(
            onPressed: () {
              Get.back(result: false);
            },
            child: Text('CANCELAR')),
        TextButton(
            onPressed: () {
              Get.back(result: true);
            },
            child: Text('CONFIRMAR'))
      ],
    ));
    if (excluir) {
      try {
        utilService.showAlertCarregando('Excluindo...');
        favoritos =
            await repository.deletarEstabelecimentoFavorito(estabelecimento);
        if (Get.isDialogOpen) Get.back();
        utilService.snackBar(
            titulo: 'Sucesso', mensagem: 'Estabelecimento excluído');
        update();
      } catch (e) {
        print('Erro ao excluir estabelecimento: $e');
        if (Get.isDialogOpen) Get.back();
        if (Get.isDialogOpen) Get.back();
        utilService.snackBarErro(mensagem: 'Erro ao excluir estabelecimento');
      }
    }
  }

  pesquisarEstabelecimento() async {
    var result = await Get.to(() => PesquisaEstabelecimentosPage(),
        fullscreenDialog: true);
    if (result != null && result['estabelecimento'] != null)
      utilService.snackBar(
          titulo: 'Estabelecimento salvo',
          mensagem:
              '${result['estabelecimento'].nomFantasia ?? result['estabelecimento'].nomRazaoSocial} foi adicionado à sua lista de favoritos');
  }

  void verNoMapa(Estabelecimento estabelecimento) async {
    final availableMaps = await MapLauncher.installedMaps;
    double latitude;
    double longitude;
    String keyword =
        estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial;

    latitude = estabelecimento.numLatitude;
    longitude = estabelecimento.numLongitude;
    /*  keyword = keyword +
        ' ${venda.estabelecimento.nomLogradouro}' +
        '${venda.estabelecimento.nomBairro}';
    Response response = await dio.post(
      'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${venda.estabelecimento.numLatitude},${venda.estabelecimento.numLongitude}&radius=10000&type=grocery_or_supermarket&keyword=$keyword&key=AIzaSyAEBDMTgvnQx6mut8Pw_gGsiEmRkLZziu4',
    );
    print(
        '${venda.estabelecimento.numLatitude},${venda.estabelecimento.numLongitude} - ${venda.estabelecimento.nomFantasia}');
    print('response google places $response');
    if (response.data['status'] == 'OK') {
      latitude = response.data['results'][0]['geometry']['location']['lat'];
      longitude = response.data['results'][0]['geometry']['location']['lng'];
    } else {
      latitude = venda.estabelecimento.numLatitude;
      longitude = venda.estabelecimento.numLongitude;
    } */

    await availableMaps.first.showMarker(
      coords: Coords(latitude, longitude),
      title: estabelecimento.nomFantasia ?? estabelecimento.nomRazaoSocial,
      description: estabelecimento.nomFantasia,
    );
    /* setState(() {
      _selectedChoices = choice;
    }); */
  }

  onPesquisar([String texto]) async {
    if (textEditingController.text.length < 4) {
      utilService.snackBarErro(mensagem: 'Digite uma pesquisa mais completa');
      return;
    }
    try {
      utilService.showAlertCarregando();
      pesquisados =
          await repository.pesquisaEstabelecimentos(textEditingController.text);
      if (Get.isDialogOpen) Get.back();
      print('pesquisados: $pesquisados');
      update();
    } catch (e) {
      utilService.snackBarErro(mensagem: 'Erro durante a busca');
    }
  }

  favoritarEstabelecimento(Estabelecimento estabelecimento) async {
    try {
      utilService.showAlertCarregando();
      favoritos = await repository.favoritarEstabelecimento(estabelecimento);
      if (Get.isDialogOpen) Get.back();

      Get.back(result: {'estabelecimento': estabelecimento});
      update();
    } catch (e) {
      utilService.snackBarErro(mensagem: 'Erro ao favoritar estabelecimento');
    }
  }

  resetPesquisa() {
    textEditingController.text = '';
    pesquisados = null;
    update();
  }
}
