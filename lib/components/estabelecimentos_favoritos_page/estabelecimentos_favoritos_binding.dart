import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_controller.dart';
import 'package:get/get.dart';

class EstabelecimentosFavoritosBinding implements Bindings {
  @override
  void dependencies() {
    print('dependencies homebinding');
    Get.lazyPut<EstabelecimentosFavoritosController>(
        () => EstabelecimentosFavoritosController());
  }
}
