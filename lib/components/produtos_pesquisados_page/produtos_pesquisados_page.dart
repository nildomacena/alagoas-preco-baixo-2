import 'package:aldescontos/components/produtos_pesquisados_page/produtos_pesquisados_controller.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './produtos_pesquisados_card.dart';

class ProdutosPesquisadosPage extends StatelessWidget {
  final ProdutosPesquisadosController controller;
  ProdutosPesquisadosPage(this.controller);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Produtos Pesquisados'),
      ),
      body: Container(
        child: GetBuilder<ProdutosPesquisadosController>(
          builder: (_) {
            if (_.buscando)
              return Center(
                child: CircularProgressIndicator(),
              );
            if (_.produtosPesquisados.isEmpty) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Você ainda não buscou nenhum produto.\nClique no botão abaixo e faça uma pesquisa!",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: ElevatedButton(
                      child: Text("FAZER PESQUISA"),
                      onPressed: () {
                        Get.offAndToNamed(Routes.INITIAL);
                      },
                    ),
                  ),
                ],
              );
            }
            return ListView.builder(
                itemCount: _.produtosPesquisados.length,
                itemBuilder: (context, index) {
                  return CardProdutoPesquisado(_.produtosPesquisados[index], _);
                });
          },
        ),
      ),
    );
  }
}
