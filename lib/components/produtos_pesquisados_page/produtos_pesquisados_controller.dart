import 'package:aldescontos/components/produtos_pesquisados_page/produtos_pesquisados_repository.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/produto_pesquisado.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';

class ProdutosPesquisadosController extends GetxController {
  bool buscando = true;
  final ProdutosPesquisadosRepository repository =
      Get.put(ProdutosPesquisadosRepository(apiProvider: Get.find()));
  List<ProdutoPesquisado> produtosPesquisados = [];
  ProdutosPesquisadosController();

  @override
  void onInit() {
    super.onInit();
    getProdutosPesquisados();
    print('Produtos pesquisados: ${repository.getProdutosPesquisados()}');
  }

  getProdutosPesquisados() {
    this.produtosPesquisados = repository.getProdutosPesquisados();
    buscando = false;
    update();
  }

  pesquisarProdutoPorCodigoBarras(ProdutoPesquisado produto) async {
    Get.dialog(
        AlertDialog(
          content: Container(
            height: 80,
            child: Column(
              children: <Widget>[
                Center(
                  child: CircularProgressIndicator(),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                ),
                Text(
                  'Fazendo consulta...',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Get.theme.primaryColor),
                )
              ],
            ),
          ),
        ),
        barrierDismissible: false);
    try {
      List<Venda> vendas =
          await repository.pesquisarProdutoPorCodigoBarras(produto.codGetin);
      LocationData locationData = await repository.getLocationData();
      Get.back();
      Get.toNamed(Routes.RESULTADOS, arguments: {
        'vendas': vendas,
        'locationData': locationData,
        'codBarras': produto.codGetin,
      });
    } catch (e) {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem:
              'Os servidores da SEFAZ não responderam. Tente novamente mais tarde mais tarde');
      print('Erro durante a consulta: $e');
    }
  }

  excluirProduto(ProdutoPesquisado produto) async {
    bool result = await Get.dialog(AlertDialog(
      title: Text('Confirmação'),
      content:
          Text('Deseja realmente excluir o produto ${produto.dscProduto}?'),
      actions: <Widget>[
        TextButton(
            onPressed: () {
              Get.back(result: false);
            },
            child: Text('NÃO')),
        TextButton(
            onPressed: () async {
              Get.back(result: true);
            },
            child: Text('SIM'))
      ],
    ));
    if (result == null || !result) return;

    produtosPesquisados = await repository.deletarProdutoPesquisado(produto);
    update();
    utilService.snackBar(
        titulo: 'Pronto!', mensagem: 'Produto excluído da lista');
  }
}
