import 'package:aldescontos/components/produtos_pesquisados_page/produtos_pesquisados_controller.dart';
import 'package:aldescontos/src/data/model/produto_pesquisado.model.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class CardProdutoPesquisado extends StatelessWidget {
  final ProdutoPesquisado produtoPesquisado;
  final ProdutosPesquisadosController controller;
  CardProdutoPesquisado(this.produtoPesquisado, this.controller);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 15, top: 15),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(15),
                  height: 50,
                  color: Colors.blue,
                  width: double.infinity,
                  alignment: Alignment.center,
                  child: AutoSizeText(
                    '${produtoPesquisado.dscProduto}',
                    minFontSize: 15,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Positioned(
                  right: 5,
                  child: IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.red[500],
                      ),
                      onPressed: () {
                        controller.excluirProduto(produtoPesquisado);
                      }),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(top: 4, bottom: 4),
              width: double.infinity,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Nº Código de barras',
                    style: TextStyle(fontSize: 18),
                  ),
                  Padding(padding: EdgeInsets.all(1)),
                  Text('${produtoPesquisado.codGetin}',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Padding(padding: EdgeInsets.all(1)),
                  Text(
                    'Data da pesquisa: ${produtoPesquisado.data}',
                    style: TextStyle(fontSize: 14),
                  ),
                  TextButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'PESQUISAR NOVAMENTE',
                            style: TextStyle(
                                color: Colors.blue,
                                decoration: TextDecoration.underline),
                          ),
                          Padding(
                            padding: EdgeInsets.all(3),
                          ),
                          Icon(
                            Icons.search,
                            color: Colors.blue,
                          )
                        ],
                      ),
                      onPressed: () {
                        controller
                            .pesquisarProdutoPorCodigoBarras(produtoPesquisado);
                      })
                ],
              ),
            ),
            /* Container(
              padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
              //height: 85,
              color: Colors.blue[200],
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onLongPress: () {
                        print(venda.estabelecimento.numCNPJ);
                      },
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AutoSizeText(
                              '${venda.estabelecimento.nomFantasia ?? venda.estabelecimento.nomRazaoSocial}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            AutoSizeText(
                              '${venda.estabelecimento.nomLogradouro}, ${venda.estabelecimento.numImovel ?? ''} - ${venda.estabelecimento.numCep}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            if (venda.estabelecimento.nomBairro != null)
                              AutoSizeText(
                                '${venda.estabelecimento.nomBairro}',
                                minFontSize: 11,
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black),
                              ),
                            if (venda.estabelecimento.numTelefone != null)
                              AutoSizeText(
                                '${venda.estabelecimento.numTelefone}',
                                minFontSize: 11,
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ), 
                ],
              ),
            ),*/
          ],
        ),
      ),
    );
  }
}
