import 'package:aldescontos/components/produtos_pesquisados_page/produtos_pesquisados_controller.dart';
import 'package:aldescontos/components/produtos_pesquisados_page/produtos_pesquisados_repository.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:get/get.dart';

class ProdutosPesquisadosBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProdutosPesquisadosController>(
        () => ProdutosPesquisadosController());
  }
}
