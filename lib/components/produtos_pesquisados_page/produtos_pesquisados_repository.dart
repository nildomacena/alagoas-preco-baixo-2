import 'package:aldescontos/src/data/model/produto_pesquisado.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:aldescontos/src/data/services/location.service.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';

class ProdutosPesquisadosRepository {
  final ApiProvider apiProvider;

  ProdutosPesquisadosRepository({@required this.apiProvider})
      : assert(apiProvider != null);

  List<ProdutoPesquisado> getProdutosPesquisados() {
    return apiProvider.getProdutosPesquisados();
  }

  Future<List<Venda>> pesquisarProdutoPorCodigoBarras(String codGetin) {
    return apiProvider.pesquisarProdutoPorCodigoBarras(codGetin);
  }

  Future<List<ProdutoPesquisado>> deletarProdutoPesquisado(
      ProdutoPesquisado produto) async {
    await apiProvider.deletarProdutoBuscado(produto);
    return getProdutosPesquisados();
  }

  Future<LocationData> getLocationData() {
    return locationService.getLocationData();
  }
}
