import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:aldescontos/src/data/provider/user.provider.dart';
import 'package:aldescontos/src/data/services/location.service.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';

class PromocoesRepository {
  final ApiProvider apiProvider = Get.find();
  final UserProvider userProvider = Get.find();

  Future<List<Venda>> getPromocoes() {
    return apiProvider.getPromocoes();
  }

  Future<LocationData> getLocationData() {
    return locationService.getLocationData();
  }

  Future<List<Venda>> deletarPromocao(Venda venda) {
    return apiProvider.deletarPromocao(venda);
  }

  bool get isAdmin => userProvider.isAdmin();
}
