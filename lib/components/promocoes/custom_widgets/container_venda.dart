import 'package:aldescontos/components/promocoes/promocoes_controller.dart';
import 'package:aldescontos/components/resultados_page/resultados_controller.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:maps_toolkit/maps_toolkit.dart';

class ContainerVenda extends StatelessWidget {
  double distanceBetweenPoints;
  final LocationData locationData;
  final PromocoesController controller;
  Venda venda;

  ContainerVenda(
      {@required this.venda,
      @required this.locationData,
      @required this.controller}) {
    distanceBetweenPoints = SphericalUtil.computeDistanceBetween(
        LatLng(locationData.latitude, locationData.longitude),
        LatLng(venda.estabelecimento.numLatitude,
            venda.estabelecimento.numLongitude));
  }

  Widget pesquisaPorCodigo() {
    bool semCodigo = venda.produto.codGetin == null ||
        venda.produto.codGetin.isEmpty ||
        venda.produto.codGetin.toUpperCase().contains('SEM');
    return GetBuilder<ResultadosController>(builder: (_) {
      /* return Container(
          //margin: EdgeInsets.only(top: 2),
          child: TextButton(
              onPressed: () {
                controller.salvarProdutoListaCompras(venda);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    controller.adicionadoALista
                        ? 'Remover da lista de compras'.toUpperCase()
                        : 'Adicionar à lista de compras'.toUpperCase(),
                    style: TextStyle(
                        color: semCodigo ? Colors.grey : Colors.blue,
                        decoration: TextDecoration.underline),
                  ),
                  /*  Padding(
                    padding: EdgeInsets.all(3),
                  ), */
                  if (!semCodigo) ...{
                    Icon(
                      Icons.list,
                      color: Colors.blue,
                    )
                  }
                ],
              ))); */
      return Container(
          //margin: EdgeInsets.only(top: 2),
          child: TextButton(
              onPressed: () {
                controller.pesquisarProdutoCodBarras(venda);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    semCodigo
                        ? 'Esse produto não possui código cadastrado'
                        : 'Pesquisar pelo código de barras'.toUpperCase(),
                    style: TextStyle(
                        color: semCodigo ? Colors.grey : Colors.blue,
                        decoration: TextDecoration.underline),
                  ),
                  /*  Padding(
                    padding: EdgeInsets.all(3),
                  ), */
                  if (!semCodigo) ...{
                    Icon(
                      Icons.search,
                      color: Colors.blue,
                    )
                  }
                ],
              )));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(bottom: 13, top: 12),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15),
              height: 50,
              color: Colors.blue,
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: AutoSizeText(
                '${venda.produto.dscProduto.toUpperCase()}',
                minFontSize: 15,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(top: 4, bottom: 4),
              width: double.infinity,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Última venda',
                    style: TextStyle(fontSize: 18),
                  ),
                  Padding(padding: EdgeInsets.all(1)),
                  Text('R\$${venda.valUnitarioUltimaVenda.toStringAsFixed(2)}',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Padding(padding: EdgeInsets.all(1)),
                  /* Text(
                    'Data última venda: ${venda.horaFormatada}',
                    style: TextStyle(fontSize: 14),
                  ), */
                  Text(
                    'Promoção cadastrada há ${venda.tempoEmHorasFormatado}',
                    style: TextStyle(fontSize: 18),
                  ),
                  pesquisaPorCodigo()
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
              //height: 85,
              color: Colors.blue[200],
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onLongPress: () {
                        print(venda.estabelecimento.numCNPJ);
                      },
                      child: Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AutoSizeText(
                              '${venda.estabelecimento.nomFantasia ?? venda.estabelecimento.nomRazaoSocial}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),

                            //TODO retirar esse GestureDetector. Só testes
                            Text(
                                'Distância: ${(distanceBetweenPoints / 1000).toStringAsFixed(2)} KM',
                                style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                            AutoSizeText(
                              '${venda.estabelecimento.nomLogradouro}, ${venda.estabelecimento.numImovel ?? ''} - ${venda.estabelecimento.numCep}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            if (venda.estabelecimento.nomBairro != null)
                              AutoSizeText(
                                '${venda.estabelecimento.nomBairro}',
                                minFontSize: 11,
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black),
                              ),
                            if (venda.estabelecimento.numTelefone != null)
                              AutoSizeText(
                                '${venda.estabelecimento.numTelefone}',
                                minFontSize: 11,
                                style: TextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  if (controller.isAdmin)
                    IconButton(
                        icon: Icon(Icons.delete_forever),
                        onPressed: () {
                          controller.deletarPromocao(venda);
                        }),
                  IconButton(
                      icon: Icon(Octicons.location),
                      onPressed: () {
                        controller.verNoMapa(venda);
                      }),
                  /* GetBuilder<ResultadosController>(
                    builder: (_) {
                      return IconButton(
                          icon: Icon(_.checkEstabelecimentoFavorito(
                                  venda.estabelecimento)
                              ? Icons.favorite
                              : Icons.favorite_border),
                          onPressed: _.favoritandoEstabelecimento
                              ? null
                              : () async {
                                  controller.favoritarEstabelecimento(
                                      venda.estabelecimento);
                                });
                    },
                  ) */
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
