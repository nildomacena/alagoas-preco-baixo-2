import 'package:aldescontos/components/promocoes/promocoes_repository.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/services/location.service.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:map_launcher/map_launcher.dart';

class PromocoesController extends GetxController {
  List<Venda> promocoes;
  LocationData locationData;
  bool isAdmin = false;
  final PromocoesRepository repository = Get.find();

  PromocoesController() {
    if (Get.arguments != null) locationData = Get.arguments['locationData'];
  }

  @override
  onInit() async {
    super.onInit();
    initPromocoes();
    print('promocoes: $promocoes');
  }

  initPromocoes() async {
    promocoes = await repository.getPromocoes();
    locationData = await locationService.getLocationData();
    isAdmin = repository.isAdmin;
    print('promocoes: $promocoes');

    update();
  }

  pesquisarProdutoCodBarras(Venda venda) {
    if (venda.produto.codGetin == null || venda.produto.codGetin.isEmpty) {
      utilService.snackBarErro(
          mensagem: 'Esse produto não possui código de barras');
      return;
    }
    Get.back(result: {'codBarras': venda.produto.codGetin});
  }

  deletarPromocao(Venda venda) async {
    print('deletar promocao');
    try {
      bool confirma = await utilService.alertConfirma(
          mensagem:
              'Confirma a exclusão da promoção ${venda.produto.dscProduto}?\nATENÇÃO: Essa operação não pode ser desfeita!');
      if (!confirma) return;
      promocoes = await repository.deletarPromocao(venda);
      update();
    } catch (e) {
      print('Erro ao deletar: $e');
      utilService.snackBarErro(mensagem: 'Erro ao deletar');
    }
  }

  void verNoMapa(Venda venda) async {
    final availableMaps = await MapLauncher.installedMaps;
    double latitude;
    double longitude;
    String keyword = venda.estabelecimento.nomFantasia ??
        venda.estabelecimento.nomRazaoSocial;

    latitude = venda.estabelecimento.numLatitude;
    longitude = venda.estabelecimento.numLongitude;
    await availableMaps.first.showMarker(
      coords: Coords(latitude, longitude),
      title: venda.estabelecimento.nomFantasia ??
          venda.estabelecimento.nomRazaoSocial,
      description: venda.estabelecimento.nomFantasia,
    );
  }
}
