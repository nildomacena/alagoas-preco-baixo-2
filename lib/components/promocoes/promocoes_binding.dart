import 'package:aldescontos/components/promocoes/promocoes_controller.dart';
import 'package:aldescontos/components/promocoes/promocoes_repository.dart';
import 'package:get/get.dart';

class PromocoesBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(PromocoesRepository());
    Get.lazyPut<PromocoesController>(() => PromocoesController());
  }
}
