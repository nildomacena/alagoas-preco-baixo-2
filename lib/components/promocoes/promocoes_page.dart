import 'package:aldescontos/components/promocoes/custom_widgets/container_venda.dart';
import 'package:aldescontos/components/promocoes/promocoes_controller.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PromocoesPage extends StatelessWidget {
  final PromocoesController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Promoções'),
        ),
        body: GetBuilder<PromocoesController>(
          builder: (_) {
            if (controller.promocoes == null || controller.locationData == null)
              return utilService.columnCarregando('Carregando promoções...');
            if (controller.promocoes.isEmpty)
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    alignment: Alignment.center,
                    child: Text(
                      'Por enquanto, não há promoções cadastradas\nMas fique atento, em breve pode aparecer uma!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.blue,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: ElevatedButton(
                          onPressed: () {
                            Get.back();
                          },
                          child: Text('RETORNAR')))
                ],
              );
            return Container(
              child: ListView.builder(
                  itemCount: controller.promocoes.length,
                  itemBuilder: (context, index) {
                    Venda promocao = controller.promocoes[index];
                    return ContainerVenda(
                      controller: controller,
                      venda: promocao,
                      locationData: controller.locationData,
                    );
                  }),
            );
          },
        ));
  }
}
