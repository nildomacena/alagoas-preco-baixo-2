import 'package:aldescontos/components/resultados_page/resultados_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:get/get.dart';

class FiltrosModal extends StatelessWidget {
  final ResultadosController controller = Get.find();
  List<int> fixedValues = [for (var i = 0; i < 10; i += 1) i];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Filtros'),
        ),
        body: Container(
          margin: EdgeInsets.only(bottom: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Limite de distância',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              GetBuilder<ResultadosController>(
                builder: (_) {
                  return Text(
                    'Raio: ${controller.raio}km',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  );
                },
              ),
              GetBuilder<ResultadosController>(builder: (_) {
                return Container(
                  margin: EdgeInsets.only(top: 5),
                  child: FlutterSlider(
                    values: [controller.raio.toDouble()],
                    step: FlutterSliderStep(step: 1),
                    min: 1,
                    max: 15,
                    tooltip: FlutterSliderTooltip(
                      positionOffset:
                          FlutterSliderTooltipPositionOffset(top: 100),
                      direction: FlutterSliderTooltipDirection.top,
                      textStyle: TextStyle(fontSize: 17, color: Colors.white),
                      boxStyle: FlutterSliderTooltipBox(
                          decoration: BoxDecoration(
                              color: Colors.blueAccent.withOpacity(0.7))),
                    ),
                    handlerAnimation: FlutterSliderHandlerAnimation(
                        curve: Curves.elasticOut,
                        reverseCurve: Curves.bounceIn,
                        duration: Duration(milliseconds: 500),
                        scale: 1.5),
                    onDragCompleted: (handlerIndex, lowerValue, upperValue) {
                      controller.setRaio(lowerValue);
                      print(
                          'handlerIndex: $handlerIndex - lowerValue: $lowerValue - upperValue: $upperValue');
                    },
                  ),
                );
              }),
              Container(margin: EdgeInsets.only(top: 10), child: Divider()),
              Container(
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Text(
                  'Ordenar produtos por:',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GetBuilder<ResultadosController>(
                    builder: (_) {
                      return Container(
                        width: 150,
                        child: TextButton(
                          onPressed: () {
                            controller.toggleFiltro('preco');
                          },
                          child: Text(
                            'PREÇO',
                            style: TextStyle(
                                color: controller.preco
                                    ? Colors.white
                                    : Colors.blue),
                          ),
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  controller.preco
                                      ? Colors.blue
                                      : Colors.transparent)),
                        ),
                      );
                    },
                  ),
                  Container(
                    width: 150,
                    child: GetBuilder<ResultadosController>(
                      builder: (_) {
                        return TextButton(
                            onPressed: () {
                              controller.toggleFiltro('distancia');
                            },
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        !controller.preco
                                            ? Colors.blue
                                            : Colors.transparent)),
                            child: Text(
                              'DISTÂNCIA',
                              style: TextStyle(
                                  color: !controller.preco
                                      ? Colors.white
                                      : Colors.blue),
                            ));
                      },
                    ),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 50, 20, 0),
                width: Get.width,
                child: ElevatedButton(
                  onPressed: () {
                    controller.definirFiltros();
                    Get.back();
                  },
                  child: Text('DEFINIR FILTROS'),
                  style: ButtonStyle(),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                width: Get.width,
                child: TextButton(
                  onPressed: () {
                    controller.resetFiltros();
                    Get.back();
                  },
                  child: Text(
                    'CANCELAR',
                    style: TextStyle(color: Colors.red),
                  ),
                  style: ButtonStyle(
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.red[100])),
                ),
              )
            ],
          ),
        ));
  }
}
