import 'package:aldescontos/components/resultados_page/custom_widgets/filtros_modal.dart';
import 'package:aldescontos/components/resultados_page/resultados_repository.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/services/location.service.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:maps_toolkit/maps_toolkit.dart';

class ResultadosController extends GetxController {
  final ResultadosRepository repository = Get.put(
      ResultadosRepository(apiProvider: Get.find(), userProvider: Get.find()));

  ScrollController scrollController = ScrollController();
  TextEditingController tituloPromocaoController = TextEditingController();
  TextEditingController pesquisaTextController = TextEditingController();
  FocusNode focusNode = FocusNode();

  List<Venda> vendas = [];
  List<Venda> vendasFiltradas = [];

  bool chegouNoTopo = true;
  bool filtraPorFavoritos = false;
  final RxBool _chegouNoTopoObs = true.obs;
  bool produtoAdicionadoALista = false;
  bool preco = true;
  bool isAdmin = false;
  bool favoritandoEstabelecimento =
      false; //Serve para desabilitar o botao de favoritar se estiver salvando

  String codBarras;
  String lat;
  String lng;
  int raio = 15;
  bool pesquisaPorDescricao;
  List<Estabelecimento> estabelecimentosFavoritos;
  String precoMedio = '';
  List<ProdutoListaCompras> listaCompras;
  bool adicionadoALista = false;
  LocationData locationData;

  ResultadosController() {
    print('ResultadosController()');
    vendas = Get.arguments['vendas'];
    codBarras = Get.arguments['codBarras'];
    listaCompras = Get.arguments['listaCompras'];
    if (listaCompras != null) adicionadoALista = true;
    locationData =
        Get.arguments['locationData'] ?? locationService.getLocationMaceio();
    vendasFiltradas = Get.arguments['vendas'];
    pesquisaPorDescricao = Get.arguments['pesquisaPorDescricao'] ?? false;
    print('Pesquisa por descrição: $pesquisaPorDescricao');
    initEstabelecimentosFavoritos();
    getLocationData();
    isAdmin = repository.isAdmin;
  }

  bool get chegouNoTopoObs => _chegouNoTopoObs.value;

  @override
  void onInit() {
    filtraVendas();
    print('onInit ResultadosController: ${Get.arguments['vendas']}');
    favoritandoEstabelecimento = false;
    checkTopoScroll();
    if (!pesquisaPorDescricao) calculaPrecoMedio();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    print('onReady resultados controller');
  }

  onInitAsync() async {}

  calculaPrecoMedio() {
    if (vendas == null || vendas.isEmpty) return;
    double soma = 0;
    vendas.forEach((v) {
      soma += v.valUnitarioUltimaVenda;
    });
    precoMedio = (soma / vendas.length).toStringAsFixed(2);
    update();
  }

  setRaio(double value) {
    raio = value.toInt();
    print('setRaio: $raio');
    update();
  }

  toggleFiltraPorFavorito([bool value]) {
    //Verifica se o usuário quer filtrar pelos estabelecimentos favoritos.
    //Caso queira, grava todos os cnpj numa string para ficar mais fácil de comparar
    filtraPorFavoritos = value ?? !filtraPorFavoritos;
    print('estabelecimentos favoritos: $estabelecimentosFavoritos');
    if (estabelecimentosFavoritos.isEmpty) {
      utilService.snackBar(
          titulo: 'Atenção!',
          mensagem:
              'Você não possui estabelecimentos salvos nos favoritos, ou não fez o login no aplicativo');
      update();
      return;
    }
    if (filtraPorFavoritos) {
      String auxStringFavoritos = '';
      estabelecimentosFavoritos.forEach((e) {
        auxStringFavoritos += e.numCNPJ;
      });
      vendasFiltradas = vendas
          .where((v) => auxStringFavoritos.contains(v.estabelecimento.numCNPJ))
          .toList();
    } else {
      vendasFiltradas = vendas;
      definirFiltros();
    }
    update();
  }

  goBack() {
    Get.back(result: {'listaCompras': listaCompras});
    /* if (listaCompras != null) {
      Get.offAndToNamed(
        Routes.LISTA_COMPRAS,
        arguments: {'listaCompras': listaCompras},
      );
    } else {
      Get.offAllNamed(Routes.INITIAL);
    } */
  }

  definirFiltros() {
    //vendasFiltradas = vendas;
    print('definir Filtro raio $raio');
    vendasFiltradas = vendas.where((venda) {
      double distanceBetweenPoints = SphericalUtil.computeDistanceBetween(
          LatLng(locationData.latitude, locationData.longitude),
          LatLng(venda.estabelecimento.numLatitude,
              venda.estabelecimento.numLongitude));
      return (distanceBetweenPoints / 1000) <= raio.toDouble();
    }).toList();
    print('Preço? $preco');
    vendasFiltradas.sort((a, b) {
      if (preco) {
        return a.valUnitarioUltimaVenda.compareTo(b.valUnitarioUltimaVenda);
      } else {
        double distanceA = SphericalUtil.computeDistanceBetween(
            LatLng(locationData.latitude, locationData.longitude),
            LatLng(
                a.estabelecimento.numLatitude, a.estabelecimento.numLongitude));
        double distanceB = SphericalUtil.computeDistanceBetween(
            LatLng(locationData.latitude, locationData.longitude),
            LatLng(
                b.estabelecimento.numLatitude, b.estabelecimento.numLongitude));
        print(
            'distanceA.compareTo(distanceB) ${distanceA.compareTo(distanceB)}');
        return distanceA.compareTo(distanceB);
      }
    });

    update();
  }

  resetFiltros() {
    raio = 15;
    preco = true;
    pesquisaTextController.text = '';
    focusNode.unfocus();
    vendasFiltradas = vendas;
    update();
  }

  checkTopoScroll() {
    scrollController.addListener(() {
      _chegouNoTopoObs.value = chegouNoTopo = scrollController.offset == 0;

      update();
      print('Chegou no topo: $chegouNoTopo');
    });
  }

  toggleFiltro(String filtro) {
    preco = filtro.contains('preco');
    update();
  }

  onFiltro() {
    Get.to(() => FiltrosModal(), fullscreenDialog: true);
  }

  scanCodBarras() {
    Get.offAndToNamed(Routes.INITIAL, arguments: {'scan': true});
  }

  salvarProdutoListaCompras(Venda venda) async {
    try {
      if (adicionadoALista)
        listaCompras =
            await repository.removerProdutoListaCompras(venda.produto.codGetin);
      else
        listaCompras = await repository.salvarProdutoListaCompras(venda);
      adicionadoALista = !adicionadoALista;
      update();
    } catch (e) {
      print('Erro salvarProdutoListaCompras: $e');
      if (e == 'nao-autenticado')
        utilService.snackBarErro(
            mensagem: 'Você precisa fazer login para utilizar esta função');
      else
        utilService.snackBarErro();
    }
  }

  pesquisarProdutoCodBarras(Venda venda) {
    if (venda.produto.codGetin == null || venda.produto.codGetin.isEmpty) {
      utilService.snackBarErro(
          mensagem: 'Esse produto não possui código de barras');
      return;
    }
    Get.back(result: {'codBarras': venda.produto.codGetin});
  }

  getLocationData() async {
    locationData = await repository.getLocationData();
  }

  initEstabelecimentosFavoritos() async {
    estabelecimentosFavoritos =
        await repository.getEstabelecimentosFavoritosAsync();
    print('Estabelecimentos favoritos: $estabelecimentosFavoritos');
    update();
  }

  filtraVendas() {
    List<Venda> vendasAux = [];

    pesquisaTextController.addListener(() async {
      vendasAux = vendas?.where((Venda venda) {
        return venda.produto.dscProduto
                .toUpperCase()
                .contains(pesquisaTextController.text.toUpperCase()) ||
            venda.estabelecimento.nomFantasia
                .toUpperCase()
                .contains(pesquisaTextController.text.toUpperCase()) ||
            venda.estabelecimento.nomRazaoSocial
                .toUpperCase()
                .contains(pesquisaTextController.text.toUpperCase()) ||
            venda.estabelecimento.nomBairro
                .toUpperCase()
                .contains(pesquisaTextController.text.toUpperCase());
      })?.toList();

      if (pesquisaTextController.text.isEmpty) {
        vendasFiltradas = vendas;
        focusNode.requestFocus();
      } else {
        print('Entrou no else');
        vendasFiltradas = vendasAux;
        if (vendasFiltradas.isEmpty) {
          print('Vendas filtradas isempty');
          setFocus();
        }
      }
      /*  print('Vendas filtradas Filtra vendas: $vendasFiltradas'); */
      update();
    });
  }

  setFocus() async {
    //focusNode.unfocus();
    print('setfocus');
    await Future.delayed(Duration(milliseconds: 500));
    if (!focusNode.hasFocus) focusNode.requestFocus();
  }

  void limparTextoPesquisa() {
    pesquisaTextController.text = '';
    definirFiltros();
    focusNode.unfocus();
    update();
  }

  bool checkEstabelecimentoFavorito(Estabelecimento estabelecimento) {
    return repository.checkEstabelecimentoFavorito(estabelecimento);
  }

  Future<dynamic> favoritarEstabelecimento(
      Estabelecimento estabelecimento) async {
    /* 
    TEstes
    repository.favoritarEstabelecimento(estabelecimento);
    update();
    return; */
    if (repository.user == null) {
      bool fazerLogin = await Get.dialog(AlertDialog(
        title: Text('Faça login'),
        content: Text(
            'Para ter acesso a todas as funcionalidades, como cadastrar estabelecimentos favoritos, faça o login com sua conta do Google. É totalmente grátis'),
        actions: <Widget>[
          TextButton(
              onPressed: () async {
                Get.back(result: false);
              },
              child: Text('Cancelar')),
          TextButton(
              onPressed: () async {
                Get.back(result: true);
                /* try {
                  if (await firebaseService.getCurrentUser() == null)
                    await firebaseService.googleSignIn();
                  await firebaseService
                      .favoritarEstabelecimento(venda.estabelecimento);
                  Navigator.of(context).pop();
                  Toast.show(
                      'Estabelecimento adicionado aos favoritos', context);
                } catch (e) {
                  print('Ocorreu um erro durante o login. $e');
  
                  Get.back();
                } */
              },
              child: Text('Fazer login com Google')),
        ],
      ));
      //Se nao quer fazer login, encerra a function
      if (!fazerLogin) return;
      //Se quiser fazer login...
      Get.dialog(AlertDialog(
          content: Container(
        height: 80,
        child: Column(
          children: <Widget>[
            Center(
              child: CircularProgressIndicator(),
            ),
            Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              'Aguarde...',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Get.theme.primaryColor),
            )
          ],
        ),
      )));
      await repository.fazerLogin();
      Get.back(); //Fecha o alert de loading
      //Se fez o login, chama a function de novo (recursividade)
      if (repository.user != null) favoritarEstabelecimento(estabelecimento);
    } else {
      try {
        favoritandoEstabelecimento = true;
        update();
        List<Estabelecimento> favoritos =
            await repository.favoritarEstabelecimento(estabelecimento);
        Get.snackbar(
            'Atualizado',
            favoritos.isNotEmpty
                ? 'Estabelecimento adicionado aos favoritos'
                : 'Estabelecimento removido dos favoritos',
            snackPosition: SnackPosition.BOTTOM,
            margin: EdgeInsets.only(bottom: 10, left: 5, right: 5));
        favoritandoEstabelecimento = false;

        update();
      } catch (e) {
        Get.snackbar('Erro', 'Erro ao carregar localização',
            snackPosition: SnackPosition.BOTTOM,
            colorText: Colors.white,
            backgroundColor: Colors.red,
            margin: EdgeInsets.only(bottom: 10));
        repository.removeEstabelecimentoFavoritoErro(estabelecimento);
        favoritandoEstabelecimento = false;
        update();
      }
    }
    estabelecimentosFavoritos =
        await repository.getEstabelecimentosFavoritosAsync();
  }

  //TODO retirar a function de logout
  logout() {
    repository.logout();
  }

  /*  List<Venda> get vendasFiltradas => _vendasFiltradas.value;
  set vendasFiltradas(value) => _vendasFiltradas.value = value; */

  void verNoMapa(Venda venda) async {
    final availableMaps = await MapLauncher.installedMaps;
    double latitude;
    double longitude;
    String keyword = venda.estabelecimento.nomFantasia ??
        venda.estabelecimento.nomRazaoSocial;

    latitude = venda.estabelecimento.numLatitude;
    longitude = venda.estabelecimento.numLongitude;
    /*  keyword = keyword +
        ' ${venda.estabelecimento.nomLogradouro}' +
        '${venda.estabelecimento.nomBairro}';
    Response response = await dio.post(
      'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${venda.estabelecimento.numLatitude},${venda.estabelecimento.numLongitude}&radius=10000&type=grocery_or_supermarket&keyword=$keyword&key=AIzaSyAEBDMTgvnQx6mut8Pw_gGsiEmRkLZziu4',
    );
    print(
        '${venda.estabelecimento.numLatitude},${venda.estabelecimento.numLongitude} - ${venda.estabelecimento.nomFantasia}');
    print('response google places $response');
    if (response.data['status'] == 'OK') {
      latitude = response.data['results'][0]['geometry']['location']['lat'];
      longitude = response.data['results'][0]['geometry']['location']['lng'];
    } else {
      latitude = venda.estabelecimento.numLatitude;
      longitude = venda.estabelecimento.numLongitude;
    } */

    await availableMaps.first.showMarker(
      coords: Coords(latitude, longitude),
      title: venda.estabelecimento.nomFantasia ??
          venda.estabelecimento.nomRazaoSocial,
      description: venda.estabelecimento.nomFantasia,
    );
    /* setState(() {
      _selectedChoices = choice;
    }); */
  }

  Future<dynamic> addToPromocao(Venda venda) async {
    if (!repository.isAdmin) return Future.value();
    bool result = await Get.dialog(AlertDialog(
      title: Text('Adicionar a promoções'),
      content: Container(
        child: TextField(
          controller: tituloPromocaoController,
          decoration: InputDecoration(hintText: 'Titulo'),
        ),
      ),
      actions: [
        TextButton(
          child: Text('CONFIRMAR'),
          onPressed: () {
            Get.back(result: true);
          },
        )
      ],
    ));
    if (result == null) return;
    print('result addToPromocao: $result');
    await repository.addPromocao(venda, tituloPromocaoController.text);
    tituloPromocaoController.text = '';
  }
}
