import 'package:admob_flutter/admob_flutter.dart';
import 'package:aldescontos/components/resultados_page/custom_widgets/container_venda.dart';
import 'package:aldescontos/components/resultados_page/resultados_controller.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class ResultadosPage extends StatelessWidget {
  String codBarras;
  final ResultadosController controller = Get.find();

  Widget linhaFiltros() {
    return Container(
        height: 50,
        //height: 100,
        //color: Colors.red,
        width: Get.width,
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Row(
          children: [
            Expanded(
              child: Card(
                  child: GetBuilder<ResultadosController>(
                      builder: (_) => TextFormField(
                          maxLines: 1,
                          controller: controller.pesquisaTextController,
                          autofocus: false,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.search,
                          validator: (value) {
                            if (value.isEmpty) return "Campo obrigatório";
                            if (value.length < 4)
                              return "Digite uma pesquisa mais completa";
                          },
                          onFieldSubmitted: (texto) {
                            controller.focusNode.unfocus();
                          },
                          focusNode: controller.focusNode,
                          decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              hintText: 'Filtrar resultado',
                              suffixIcon: IconButton(
                                icon: Icon(Icons.clear),
                                iconSize: 15,
                                onPressed: controller.limparTextoPesquisa,
                              ),
                              hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300),
                              border: UnderlineInputBorder())))),
            ),
            Container(
              margin: EdgeInsets.only(right: 10, left: 10),
              child: ElevatedButton(
                onPressed: () {
                  controller.onFiltro();
                },
                child: Text('FILTROS'),
              ),
            ),
            GetBuilder<ResultadosController>(
              builder: (_) {
                if (controller.raio == 15 &&
                    controller.pesquisaTextController.text.isEmpty &&
                    controller.preco) return Container();
                return Container(
                  margin: EdgeInsets.only(right: 10, left: 10),
                  child: TextButton(
                    onPressed: () {
                      controller.resetFiltros();
                    },
                    child: Text(
                      'LIMPAR\nFILTROS',
                      style: TextStyle(color: Colors.red, fontSize: 12),
                    ),
                    style: ButtonStyle(
                        overlayColor:
                            MaterialStateProperty.all<Color>(Colors.red[100])),
                  ),
                );
              },
            )
          ],
        ));
  }

  Widget infoGerais() {
    TextStyle textStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.w600);
    return GetBuilder<ResultadosController>(
      builder: (_) {
        //Se a pesquisa é pela descricao, não há necessidade de mostrar as informações gerais
        if (controller.pesquisaPorDescricao) return Container();
        return Container(
          width: Get.width,
          height: 72,
          child: Column(
            children: [
              Divider(),
              Text(
                'Itens encontrados: ${controller.vendas.length}',
                style: textStyle,
              ),
              Text(
                'Preço medio: R\$${controller.precoMedio}',
                style: textStyle,
              ),
              Divider()
            ],
          ),
        );
      },
    );
  }

  Widget switchFiltrarPorFavoritos() {
    return GetBuilder<ResultadosController>(builder: (_) {
      return Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Switch(
              value: _.filtraPorFavoritos,
              onChanged: _.toggleFiltraPorFavorito,
            ),
            GestureDetector(
              onTap: _.toggleFiltraPorFavorito,
              child: AutoSizeText(
                'Filtrar por estabelecimentos favoritos?',
                maxLines: 1,
                minFontSize: 14,
                maxFontSize: 16,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
              ),
            )
          ],
        ),
      );
    });
  }

/**TODO RESOLVER  */
  @override
  Widget build(BuildContext context) {
    print('BUILD PAGE');
    return WillPopScope(
      onWillPop: () async {
        controller.goBack();
        return false;
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text('${controller.vendas.first.produto.dscProduto}'),
            leading: IconButton(
                icon: Icon(Icons.arrow_back), onPressed: controller.goBack),
          ),
          body: Container(
            alignment: Alignment.center,
            child: Stack(
              fit: StackFit.expand,
              children: [
                GetBuilder<ResultadosController>(
                  builder: (_) {
                    if (controller.vendasFiltradas.isEmpty)
                      return Column(
                        children: [
                          linhaFiltros(),
                          switchFiltrarPorFavoritos(),
                          Container(
                            margin: EdgeInsets.fromLTRB(15, 30, 15, 0),
                            child: Text(
                              "Nenhum resultado para os filtros utilizados",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            ),
                          ),
                        ],
                      );

                    return ListView.builder(
                      itemCount: controller.vendasFiltradas.length + 4,
                      itemBuilder: (context, index) {
                        if (index == 0) {
                          return linhaFiltros();
                        }
                        if (index == 1) {
                          return switchFiltrarPorFavoritos();
                        }
                        if (index == 2) {
                          return infoGerais();
                        }

                        if (index == controller.vendasFiltradas.length + 3) {
                          //Se for o último item exibir um padding
                          return Padding(
                            padding: EdgeInsets.all(50),
                          );
                        } else {
                          return ContainerVenda(
                              venda: controller.vendasFiltradas[index - 3],
                              locationData: controller.locationData,
                              controller: controller);
                        }
                      },
                    );
                  },
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    width: Get.width,
                    child: AdmobBanner(
                      adUnitId: utilService.bannerEntreListaId,
                      adSize: AdmobBannerSize.FULL_BANNER,
                      onBannerCreated: (controller) {
                        print('onBannerCreated: $controller');
                      },
                    ),
                  ),
                ),
                Positioned(
                  bottom: 60,
                  right: 10,
                  child: FloatingActionButton(
                    onPressed: controller.scanCodBarras,
                    child: Icon(Ionicons.md_barcode),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}

/** Usando Animated container
 Container(
          alignment: Alignment.center,
          child: Container(
              padding: EdgeInsets.only(bottom: 0),
              child: Stack(
              mainAxisAlignment: MainAxisAlignment.start,
                fit: StackFit.expand,
                children: <Widget>[
                  Obx(() {
                    return AnimatedContainer(
                      duration: Duration(milliseconds: 400),
                      padding: controller.chegouNoTopoObs
                          ? EdgeInsets.only(top: 60)
                          : EdgeInsets.only(top: 0),
                      child: controller.vendasFiltradas.isEmpty
                          ? Container(
                              margin: EdgeInsets.fromLTRB(15, 30, 15, 0),
                              child: Text(
                                "Nenhum resultado para os filtros utilizados",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            )
                          : ListView.builder(
                              controller: controller.scrollController,
                              itemCount: controller.vendasFiltradas.length + 1,
                              itemBuilder: (BuildContext context, int index) {
                                if (index ==
                                    controller.vendasFiltradas.length) {
                                  //Se for o último item exibir um padding
                                  return Padding(
                                    padding: EdgeInsets.all(50),
                                  );
                                }
                                return ContainerVenda(
                                    venda: controller.vendasFiltradas[index],
                                    locationData: controller.locationData,
                                    controller: controller);
                              }),
                    );
                  }),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      width: Get.width,
                      child: AdmobBanner(
                        adUnitId: utilService.bannerEntreListaId,
                        adSize: AdmobBannerSize.FULL_BANNER,
                        onBannerCreated: (controller) {
                          print('onBannerCreated: $controller');
                        },
                      ),
                    ),
                  ),
                  GetBuilder<ResultadosController>(builder: (_) {
                    return Positioned(
                      top: 0,
                      right: 0,
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 400),
                        height: controller.chegouNoTopo ? 50 : 0,
                        //height: 100,
                        //color: Colors.red,
                        width: Get.width,
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Card(
                                child: TextFormField(
                                    maxLines: 1,
                                    controller:
                                        controller.pesquisaTextController,
                                    autofocus: false,
                                    keyboardType: TextInputType.text,
                                    textInputAction: TextInputAction.search,
                                    validator: (value) {
                                      if (value.isEmpty)
                                        return "Campo obrigatório";
                                      if (value.length < 4)
                                        return "Digite uma pesquisa mais completa";
                                    },
                                    onFieldSubmitted: (texto) {
                                      controller.focusNode.unfocus();
                                    },
                                    focusNode: controller.focusNode,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.fromLTRB(
                                            20.0, 15.0, 20.0, 15.0),
                                        hintText: 'Filtrar resultado',
                                        suffixIcon: IconButton(
                                          icon: Icon(Icons.clear),
                                          iconSize: 15,
                                          onPressed: () {
                                            controller.pesquisaTextController
                                                .text = '';
                                          },
                                        ),
                                        hintStyle: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w300),
                                        border: UnderlineInputBorder())),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(right: 10, left: 10),
                              child: ElevatedButton(
                                onPressed: () {
                                  controller.onFiltro();
                                },
                                child: Text('FILTROS'),
                              ),
                            ),
                            GetBuilder<ResultadosController>(
                              builder: (_) {
                                if (controller.raio == 15 &&
                                    controller
                                        .pesquisaTextController.text.isEmpty &&
                                    controller.preco) return Container();
                                return Container(
                                  margin: EdgeInsets.only(right: 10, left: 10),
                                  child: TextButton(
                                    onPressed: () {
                                      controller.resetFiltros();
                                    },
                                    child: Text(
                                      'LIMPAR\nFILTROS',
                                      style: TextStyle(
                                          color: Colors.red, fontSize: 12),
                                    ),
                                    style: ButtonStyle(
                                        overlayColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.red[100])),
                                  ),
                                );
                              },
                            )
                          ],
                        ),
                      ),
                    );
                  }),
                  Positioned(
                    bottom: 60,
                    right: 10,
                    child: FloatingActionButton(
                      onPressed: controller.scanCodBarras,
                      child: Icon(Ionicons.md_barcode),
                    ),
                  ),
                ],
              )),
        ),
 */
