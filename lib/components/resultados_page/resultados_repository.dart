import 'dart:async';

import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:aldescontos/src/data/provider/user.provider.dart';
import 'package:aldescontos/src/data/services/location.service.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';

class ResultadosRepository {
  final ApiProvider apiProvider;
  final UserProvider userProvider;
  FirebaseUser get user => userProvider.user;

  ResultadosRepository(
      {@required this.apiProvider, @required this.userProvider})
      : assert(apiProvider != null && userProvider != null);

  List<Estabelecimento> getEstabelecimentosFavoritos() {
    return userProvider.estabelecimentosFavoritos;
  }

  Future<List<Estabelecimento>> getEstabelecimentosFavoritosAsync() {
    return userProvider.getEstabelecimentosFavoritosAsync();
  }

  Future<void> fazerLogin() {
    return userProvider.googleSignIn();
  }

  Future<List<Estabelecimento>> favoritarEstabelecimento(
      Estabelecimento estabelecimento) {
    return userProvider.favoritarEstabelecimento(estabelecimento);
  }

  bool checkEstabelecimentoFavorito(Estabelecimento estabelecimento) {
    return userProvider.checkEstabelecimentoFavorito(estabelecimento);
  }

  //Remove o estabelecimento adicionado localmente antes de ir para o firebase em caso de erro
  void removeEstabelecimentoFavoritoErro(Estabelecimento estabelecimento) {
    return userProvider.removeEstabelecimentoFavoritoErro(estabelecimento);
  }

  Future<LocationData> getLocationData() {
    return locationService.getLocationData();
  }

  Future<List<ProdutoListaCompras>> salvarProdutoListaCompras(Venda venda) {
    return apiProvider.salvarProdutoListaCompras(venda);
  }

  Future<List<ProdutoListaCompras>> removerProdutoListaCompras(
      String codGetin) {
    return apiProvider.removerProdutoListaCompras(codGetin);
  }

  bool checkAddListaCompras(String codGetin) {
    return apiProvider.checkAddListaCompras(codGetin);
  }

  bool get isAdmin => userProvider.isAdmin();

  Future<dynamic> addPromocao(Venda venda, String titulo) async {
    if (titulo != null && titulo.isNotEmpty) venda.produto.dscProduto = titulo;
    print('venda.asMap ${venda.asMap}');
    try {
      await apiProvider.addPromocao(venda);
      utilService.snackBar(
          titulo: 'Adicionado', mensagem: 'Promoção adicionada');
      return;
    } on TimeoutException {
      print('timeout exception');
      utilService.snackBarErro(mensagem: 'Operação demorou demais.');
    } catch (e) {
      print('Erro: $e');
      utilService.snackBarErro();
    }
  }
/*   calcularPrecoListaCompras(List<Estabelecimento> estabelecimentos) {
    apiProvider.calcularPrecoListaCompras(estabelecimentos);
  } */

  logout() {
    userProvider.logout();
  }
}
