import 'package:aldescontos/components/home_page/home_controller.dart';
import 'package:aldescontos/components/home_page/home_repository.dart';
import 'package:aldescontos/components/resultados_page/resultados_controller.dart';
import 'package:get/get.dart';

class ResultadosBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ResultadosController>(() => ResultadosController());
  }
}
