import 'package:admob_flutter/admob_flutter.dart';
import 'package:aldescontos/components/custom_widgets/custom_drawer.dart';
import 'package:aldescontos/components/home_page/home_controller.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  final HomeController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: CustomDrawer(
        controller: controller,
      ),
      appBar: AppBar(
          title: GestureDetector(
        //Apenas para testes
        child: Text('Alagoas Preço Baixo'),
        onDoubleTap: () {
          controller.scanCodBarras('7896496917716');
        },
        onLongPress: () {
          controller.buscarPrecoCombustivel();
        },
      )),
      body: Container(
        child: Stack(
          fit: StackFit.expand,
          children: [
            GetBuilder<HomeController>(
              //init: HomeController(),
              builder: (_) {
                if (_.splashScreen)
                  return Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          child: Text(
                            "Buscando localização...",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                  );
                if (_.locationData == null) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Para acessar o sistema é preciso ativar o GPS.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        ElevatedButton(
                          child: Text("Clique aqui para ativar o GPS"),
                          onPressed: () {
                            _.getLocation();
                          },
                        ),
                      ],
                    ),
                  );
                } else if (!_.digitarCod && !_.digitarDescricao) {
                  return Stack(
                    fit: StackFit.expand,
                    children: [
                      Container(
                        width: double.infinity,
                        child: ListView(
                          /*   mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center, */
                          children: <Widget>[
                            Container(
                              height: Get.height - 120,
                              margin: EdgeInsets.fromLTRB(15, 0, 30, 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    height: 50,
                                    width: 280,
                                    child: ElevatedButton(
                                        // color: Colors.blue,
                                        child: Text(
                                          'ESCANEAR CÓDIGO DE BARRAS',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        onPressed: () {
                                          controller.scanCodBarras();
                                        }),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    height: 50,
                                    width: 280,
                                    child: ElevatedButton(
                                      //color: Colors.blue,
                                      child: Text(
                                        'DIGITAR CÓDIGO DE BARRAS',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      onPressed: _.toggleDigitar
                                      /*  lerCodigoBarras(tipoPesquisa: 'digitar');
                                        print("digitar"); */
                                      ,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20, bottom: 0),
                                    height: 50,
                                    width: 280,
                                    child: ElevatedButton(
                                      //color: Colors.blue,
                                      child: Text(
                                        'PESQUISAR PELA DESCRIÇÃO',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      onPressed: _.toggleDigitar,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20, bottom: 0),
                                    height: 50,
                                    width: 280,
                                    child: ElevatedButton(
                                      //color: Colors.blue,
                                      child: Text(
                                        'MONTAR LISTA DE COMPRAS',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      onPressed: _.irParaListaCompras,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    height: 50,
                                    width: 280,
                                    child: ElevatedButton(
                                      //color: Colors.blue,
                                      child: Text(
                                        'BUSCAR ESTABELECIMENTOS',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      onPressed: _.irParaListaEstabelecimentos,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    height: 55,
                                    width: 280,
                                    child: Stack(
                                      fit: StackFit.expand,
                                      children: [
                                        ElevatedButton(
                                          //color: Colors.blue,
                                          child: Text(
                                            'CONSULTAR PREÇOS COMBUSTÍVEIS',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white),
                                          ),
                                          onPressed: () {
                                            controller.goToCombustiveis();
                                          }
                                          /*  lerCodigoBarras(tipoPesquisa: 'digitar');
                                            print("digitar"); */
                                          ,
                                        ),
                                        Positioned(
                                          top: 5,
                                          left: 0,
                                          child: Container(
                                              alignment: Alignment.center,
                                              height: 15,
                                              width: 50,
                                              decoration: BoxDecoration(
                                                  color: Colors.red,
                                                  border: Border.all(
                                                    color: Colors.red[500],
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20))),
                                              child: Text('Novidade',
                                                  style: TextStyle(
                                                      fontSize: 10,
                                                      color: Colors.white))),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(40),
                            )
                          ],
                        ),
                      ),
                      if (_.estabelecimento != null) ...{
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Column(
                            children: [
                              Text(
                                "Você está pesquisando produtos no estabelecimento ${_.estabelecimento.nomFantasia}",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                              ElevatedButton(
                                child: Text("CANCELAR"),
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.red)),
                                onPressed: () {
                                  _.resetEstabelecimento();
                                },
                              ),
                            ],
                          ),
                        )
                      },
                    ],
                  );
                }
                if (_.digitarCod || _.digitarDescricao) {
                  return Container(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Card(
                            child: TextFormField(
                                maxLines: 1,
                                controller: _.textEditingController,
                                keyboardType: TextInputType.text,
                                focusNode: _.focusNode,
                                //autofocus: true,
                                textInputAction: TextInputAction.search,
                                validator: (value) {
                                  if (value.isEmpty) return "Campo obrigatório";
                                  if (value.length < 4)
                                    return "Digite uma pesquisa mais completa";
                                },
                                onFieldSubmitted: _.onPesquisaDigitada,
                                decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                      icon: Icon(Icons.clear),
                                      onPressed: controller.limparTextoPesquisa,
                                    ),
                                    contentPadding: EdgeInsets.fromLTRB(
                                        20.0, 15.0, 20.0, 15.0),
                                    hintText:
                                        'Digite o código de barras ou a descrição do produto',
                                    hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                    border: UnderlineInputBorder())),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: _.resetTipoPesquisa,
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.red)),
                                child: Text('CANCELAR'),
                              ),
                              Padding(
                                padding: EdgeInsets.all(5),
                              ),
                              ElevatedButton(
                                  onPressed: _.onPesquisaDigitada,
                                  child: Text('PESQUISAR')),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }
              },
            ),
            Positioned(
                bottom: 0,
                child: Container(
                  width: Get.width,
                  child: AdmobBanner(
                    adUnitId: utilService.bannerId,
                    adSize: AdmobBannerSize.FULL_BANNER,
                    onBannerCreated: (controller) {
                      print('onBannerCreated: $controller');
                    },
                  ),
                )),
            Positioned(
              bottom: 60,
              right: 10,
              child: GetBuilder<HomeController>(
                builder: (_) {
                  if (controller.splashScreen)
                    return Container(); // Se ainda tiver buscando localização, nao retorna nada
                  return FloatingActionButton(
                    onPressed: controller.scanCodBarras,
                    child: Icon(Ionicons.md_barcode),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
