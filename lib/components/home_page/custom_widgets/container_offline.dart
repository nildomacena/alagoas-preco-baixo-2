import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContainerOffline extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      height: 160,
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            width: Get.width,
            color: Colors.black,
            child: Text(
              'AVISO!',
              style: TextStyle(
                  fontSize: 30,
                  color: Colors.yellow,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                'Os servidores da SEFAZ estão Offline 😓\nInfelizmente, para o aplicativo funcionar corretamente precisamos que os serviços da SEFAZ sejam restabelecidos',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w300,
                ),
              ))
        ],
      ),
    );
  }
}
