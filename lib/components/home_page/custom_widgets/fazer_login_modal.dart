import 'package:aldescontos/components/home_page/home_controller.dart';
import 'package:auth_buttons/auth_buttons.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FazerLoginModal extends StatelessWidget {
  final HomeController controller = Get.find();
  TextStyle style = TextStyle(fontSize: 18, fontWeight: FontWeight.w400);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('LOGIN'),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 100, bottom: 100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              margin: EdgeInsets.only(bottom: 10),
              child: Text(
                  'Para poder ter acesso a todas as funcionalidades do app, como salvar lista de compras, visualizar produtos pesquisados e salvar estabelecimentos como favoritos, é preciso fazer login.',
                  textAlign: TextAlign.justify,
                  style: style),
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              margin: EdgeInsets.only(bottom: 10),
              child: Text(
                'É simples, rápido e prático. Basta clicar no botão abaixo:',
                style: style,
              ),
            ),
            Expanded(
              child: Container(),
            ),
            GoogleAuthButton(
              darkMode: false,
              height: 40,
              width: 240,
              text: "Login com Google",
              onPressed: () async {
                FirebaseUser user = await controller.loginComGoogle();
                if (user != null) Get.back(result: {'user': user});
              },
            ),
            Expanded(
              child: Container(),
            )
          ],
        ),
      ),
    );
  }
}
