import 'package:aldescontos/components/home_page/home_controller.dart';
import 'package:aldescontos/components/home_page/home_repository.dart';
import 'package:get/get.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    print('dependencies homebinding');
    Get.lazyPut<HomeController>(() => HomeController(), fenix: true);
  }
}
