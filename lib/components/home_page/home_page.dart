import 'package:admob_flutter/admob_flutter.dart';
import 'package:aldescontos/components/custom_widgets/custom_drawer.dart';
import 'package:aldescontos/components/home_page/custom_widgets/container_offline.dart';
import 'package:aldescontos/components/home_page/home_controller.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  final HomeController controller = Get.find();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget logo() {
    return Container(
      height: 120,
      child: Column(
        children: [
          Expanded(
            child: GestureDetector(
              child: Image.asset(
                'assets/icon/icone.png',
                fit: BoxFit.cover,
              ),
              onLongPress: () {
                controller.scanCodBarras('7896213005177');
              },
            ),
          ),
          /*  Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              'ALAGOAS PREÇO BAIXO',
              style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(1.0, 1.0),
                      blurRadius: 2.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                    Shadow(
                      offset: Offset(1, 1),
                      blurRadius: 1.0,
                      color: Color.fromARGB(125, 0, 0, 255),
                    ),
                  ],
                  color: Get.theme.primaryColor),
              textAlign: TextAlign.center,
            ),
          ) */
        ],
      ),
    );
  }

  Widget botaoPesquisaDigitada() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      margin: EdgeInsets.only(bottom: 10),
      child: Material(
        elevation: 4,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: InkWell(
          onTap: () {
            controller.toggleDigitar();
          },
          child: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            height: 50,
            /* width: 280, */
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Digite a descrição ou o código de barras do produto',
                    textAlign: TextAlign.left,
                    maxLines: 2,
                    style: TextStyle(fontSize: 19, fontWeight: FontWeight.w300),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Icon(
                    FlutterIcons.search1_ant,
                    color: Colors.black,
                    size: 30,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget botaoPesquisaCodBarraS() {
    return Container(
      height: 50,
      /*  width: 280, */
      margin: EdgeInsets.only(bottom: 10),
      child: Material(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          elevation: 5,
          child: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            splashColor: Colors.white38,
            onTap: () {
              controller.scanCodBarras();
            },
            child: Container(
              child: Ink(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 5),
                      child: Text('Buscar por código de barras',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Icon(
                        FlutterIcons.md_barcode_ion,
                        color: Colors.white,
                        size: 25,
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    border: Border.all(color: Colors.transparent),
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
            ),
          )),
    );
  }

  Widget rowListaCombustiveis() {
    return Container(
      width: Get.width,
      height: 100,
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.blue,
              ),
              child: Material(
                child: InkWell(
                  onTap: controller.irParaListaCompras,
                  child: Ink(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.local_grocery_store_outlined,
                            size: 40, color: Colors.white),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 10),
                          child: AutoSizeText(
                            'Lista de compras',
                            maxLines: 2,
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(padding: EdgeInsets.all(10)),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.blue,
              ),
              child: Material(
                child: InkWell(
                  onTap: controller.goToCombustiveis,
                  child: Ink(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(FontAwesome5Solid.gas_pump,
                            size: 30, color: Colors.white),
                        Container(
                          width: double.infinity,
                          margin: EdgeInsets.only(top: 10),
                          child: AutoSizeText(
                            'Consultar preços dos combustíveis',
                            maxLines: 2,
                            maxFontSize: 20,
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget containerEstabelecimentoPesquisado() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Text(
            "Você está pesquisando produtos no estabelecimento ${controller.estabelecimento.nomFantasia}",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          ElevatedButton(
            child: Text("CANCELAR"),
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.red)),
            onPressed: () {
              controller.resetEstabelecimento();
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: CustomDrawer(
        controller: controller,
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        leading: IconButton(
          icon: new Icon(
            Icons.menu,
            color: Colors.blue,
            size: 30,
          ),
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
        ),
        /* title: GestureDetector(
            //Apenas para testes
            /*  child: Text('Alagoas Preço Baixo'), */
            onDoubleTap: () {
              controller.scanCodBarras('7896496917716');
            },
            onLongPress: () {
              controller.buscarPrecoCombustivel();
            },
          ) */
      ),
      body: Container(
        child: Stack(
          fit: StackFit.expand,
          children: [
            GetBuilder<HomeController>(
              //init: HomeController(),
              builder: (_) {
                if (_.splashScreen)
                  return Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: CircularProgressIndicator(),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          child: Text(
                            "Buscando localização...",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                  );
                if (_.locationData == null) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Para acessar o sistema é preciso ativar o GPS.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        ElevatedButton(
                          child: Text("Clique aqui para ativar o GPS"),
                          onPressed: () {
                            _.getLocation();
                          },
                        ),
                      ],
                    ),
                  );
                } else if (!_.digitarCod && !_.digitarDescricao) {
                  return Stack(
                    fit: StackFit.expand,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 15, right: 15),
                        width: double.infinity,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: Get.height - 120,
                              //margin: EdgeInsets.fromLTRB(15, 0, 30, 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  if (_.estabelecimento != null) ...{
                                    containerEstabelecimentoPesquisado()
                                  },
                                  if (_.estabelecimento == null) ...{
                                    logo(),
                                  },
                                  Divider(),
                                  botaoPesquisaDigitada(),
                                  botaoPesquisaCodBarraS(),
                                  rowListaCombustiveis(),
                                  if (!controller.servidoresOnline)
                                    ContainerOffline()
                                ],
                              ),
                            ),
                            /*   Container(
                              padding: EdgeInsets.all(40),
                            ) */
                          ],
                        ),
                      ),
                      /* if (_.estabelecimento != null) ...{
                        containerEstabelecimentoPesquisado()
                      }, */
                    ],
                  );
                }
                if (_.digitarCod || _.digitarDescricao) {
                  return Container(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Card(
                            child: TextFormField(
                                maxLines: 1,
                                controller: _.textEditingController,
                                keyboardType: TextInputType.text,
                                focusNode: _.focusNode,
                                //autofocus: true,
                                textInputAction: TextInputAction.search,
                                validator: (value) {
                                  if (value.isEmpty) return "Campo obrigatório";
                                  if (value.length < 4)
                                    return "Digite uma pesquisa mais completa";
                                },
                                onFieldSubmitted: _.onPesquisaDigitada,
                                decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                      icon: Icon(Icons.clear),
                                      onPressed: controller.limparTextoPesquisa,
                                    ),
                                    contentPadding: EdgeInsets.fromLTRB(
                                        20.0, 15.0, 20.0, 15.0),
                                    hintText:
                                        'Digite o código de barras ou a descrição do produto',
                                    hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                    border: UnderlineInputBorder())),
                          ),
                        ),
                        Container(
                          width: double.infinity,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: _.resetTipoPesquisa,
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Colors.red)),
                                child: Text('CANCELAR'),
                              ),
                              Padding(
                                padding: EdgeInsets.all(5),
                              ),
                              ElevatedButton(
                                  onPressed: _.onPesquisaDigitada,
                                  child: Text('PESQUISAR')),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }
              },
            ),
            Positioned(
                bottom: 0,
                child: Container(
                  width: Get.width,
                  child: AdmobBanner(
                    adUnitId: utilService.bannerId,
                    adSize: AdmobBannerSize.FULL_BANNER,
                    onBannerCreated: (controller) {
                      print('onBannerCreated: $controller');
                    },
                  ),
                )),
            Positioned(
              bottom: 60,
              right: 10,
              child: GetBuilder<HomeController>(
                builder: (_) {
                  if (controller.splashScreen)
                    return Container(); // Se ainda tiver buscando localização, nao retorna nada
                  return FloatingActionButton(
                    onPressed: controller.scanCodBarras,
                    child: Icon(Ionicons.md_barcode),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
