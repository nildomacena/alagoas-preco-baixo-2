import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:aldescontos/src/data/provider/user.provider.dart';
import 'package:aldescontos/src/data/services/location.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';

class HomeRepository {
  final ApiProvider apiProvider;
  final UserProvider userProvider;

  HomeRepository({@required this.apiProvider, @required this.userProvider})
      : assert(apiProvider != null && userProvider != null);

  Future<LocationData> getLocation() {
    return locationService.getLocationData().timeout(Duration(seconds: 4000));
  }

  LocationData getLocationMaceio() {
    return locationService.getLocationMaceio();
  }

  Future<bool> servidoresOnline() {
    return apiProvider.servidoresOnline();
  }

  Future<List<Estabelecimento>> get getEstabelecimentosFavoritos =>
      apiProvider.getEstabelecimentosFavoritos();

  Future<List<Venda>> pesquisarPorCodBarras(String cod,
      [LocationData locationData]) {
    return apiProvider.pesquisarProdutoPorCodigoBarras(cod, locationData);
  }

  Future<List<Venda>> buscarPorDescricao(String descricao,
      [LocationData locationData]) {
    return apiProvider.buscarPorDescricao(descricao, locationData);
  }

  Future<Venda> pesquisarProdutoPorEstabelecimento(
      String codGetin, Estabelecimento estabelecimento) {
    return apiProvider.pesquisarProdutoPorEstabelecimento(
        codGetin, estabelecimento);
  }

  Future<Produto> getProdutoPorCodigoBarras(String codBarras) {
    return apiProvider.getProdutoPorCodigoBarras(codBarras);
  }

  Future<List<ProdutoListaCompras>> getListaCompras() {
    return apiProvider.getListaCompras();
  }

  buscarPrecosCombustivel([LocationData locationData]) {
    apiProvider.buscarPrecosCombustivel();
  }

  /**AUTENTICAÇÃO */
  Future<void> logout() {
    return userProvider.logout();
  }

  Future<FirebaseUser> loginComGoogle() {
    return userProvider.googleSignIn();
  }

  Stream<FirebaseUser> get user$ => userProvider.user$;
}
