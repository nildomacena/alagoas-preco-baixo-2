import 'dart:async';
import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:aldescontos/components/home_page/home_repository.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:dio/dio.dart';
import 'custom_widgets/fazer_login_modal.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeController extends GetxController {
  TextEditingController textEditingController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> keyAutoComplete =
      GlobalKey<AutoCompleteTextFieldState<String>>();
  FocusNode focusNode = FocusNode();
  bool digitarCod = false;
  bool digitarDescricao = false;
  bool bannerCriado = false;
  int contador = 0;
  bool splashScreen = true;
  bool servidoresOnline = true;
  final Rx<FirebaseUser> _user = Rx<FirebaseUser>();
  SharedPreferences prefs;
  AdmobBanner admobBanner;
  List<String> sugestoes = ['teste', 'aaabbb', 'bbbbbb'];
  AdmobInterstitial interstitialAd = AdmobInterstitial(
    adUnitId: utilService.interstitialId,
  );
  AdmobInterstitial interstitialCombustiveisAd = AdmobInterstitial(
    adUnitId: utilService.interstitialCombustiveisId,
  );

  Estabelecimento estabelecimento;
  final HomeRepository repository = Get.put<HomeRepository>(
      HomeRepository(apiProvider: Get.find(), userProvider: Get.find()),
      permanent: true,
      tag: 'HomeRepository');
  final Rx<LocationData> _locationData = Rx<LocationData>();
  LocationData get locationData => _locationData.value;
  FirebaseUser get user => _user.value;

  HomeController() {
    getLocation();
    _user.bindStream(repository.user$);
  }

  @override
  onInit() async {
    super.onInit();
    print('Get.arguments: ${Get.arguments}');
    if (Get.arguments != null) {
      estabelecimento = Get.arguments['estabelecimento'];
      if (Get.arguments['scan'] != null && Get.arguments['scan']) {
        //Se vier um código de barras pelos argumentos, fazer logo a pesquisa
        scanCodBarras(Get.arguments['codBarras']);
      }
    }
    interstitialAd.load();
    interstitialCombustiveisAd.load();
  }

  @override
  onReady() async {
    super.onReady();
    initPromises();
    /* _locationData.value = utilService.locationData;
    if (_locationData.value == null)
      Future.delayed(Duration(seconds: 4)).then((value) {
        splashScreen = false;
        update();
      }); */
  }

  initPromises() async {
    servidoresOnline = await repository.servidoresOnline();
    if (!servidoresOnline) print('SERVIDORES OFFLINE');
    update();
  }

  getLocation() async {
    try {
      _locationData.value = await repository.getLocation();
      splashScreen = false;
      prefs = await SharedPreferences.getInstance();
      sugestoes = prefs.getStringList('pesquisa') ?? [];
      update();
    } catch (e) {
      _locationData.value = repository.getLocationMaceio();
      UtilService().snackBarErro(
          mensagem:
              'Erro ao carregar localização. Exibindo localização de Maceió');
    }
  }

  toggleDigitar([bool value]) {
    digitarCod = value ?? !digitarCod;
    if (digitarCod) focusNode.requestFocus();
    update();
  }

  limparTextoPesquisa() {
    textEditingController.text = '';
  }

  checkInterstitial() async {
    bool interstitialLoaded = await interstitialAd.isLoaded;
    print('interstitialAd.isLoaded: &$interstitialLoaded -- $contador');
    if (interstitialLoaded && contador != 0 && contador % 4 == 0) {
      print('myInterstitial.isLoaded()');
      interstitialAd.show();
      contador = 0;
    } else if (!interstitialLoaded) {
      print('else myInterstitial.notLoaded');
      interstitialAd.load();
    }
    contador++;
  }

  scanCodBarras([String cod]) async {
    checkInterstitial();
    String codigo = cod;
    List<Venda> vendas = [];
    try {
      if (codigo == null) codigo = await utilService.scanBarCode(); //Testes
      utilService.showAlertCarregando();
      print('Código: $codigo');
      //Se possui estabelecimento, pesquisar por estabelecimento
      vendas = estabelecimento != null
          ? [
              await repository.pesquisarProdutoPorEstabelecimento(
                  codigo, estabelecimento)
            ]
          : await repository.pesquisarPorCodBarras(codigo, locationData);
      if (Get.isDialogOpen) Get.back();
      if (vendas.isEmpty || vendas.first == null) {
        utilService.showAlert('Sem resultados',
            'Não foram registradas vendas para esse item nos últimos 3 dias.');
        return;
      } else {
        irParaResultados(vendas);
      }
    } on RangeError {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          titulo: 'Sem resultados',
          mensagem:
              'Não foram registradas vendas para esse item nos últimos 3 dias.');
    } on FormatException {
      print('Cancelou leitura');
      if (Get.isDialogOpen) Get.back();
      return;
    } on SocketException {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          titulo: 'Sem resultados',
          mensagem:
              'Os servidores da SEFAZ demoraram demais para responder. Tente novamente mais tarde.');
    } on DioError {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          titulo: 'Sem resultados',
          mensagem:
              'Os servidores da SEFAZ demoraram demais para responder. Tente novamente mais tarde.');
    } on TimeoutException {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem:
              'Os servidores da SEFAZ demoraram demais para responder. Tente novamente mais tarde.');
    } catch (e) {
      print('Erro ao escanear: $e');
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem:
              'Os servidores da SEFAZ demoraram demais para responder. Tente novamente mais tarde.');
    }
  }

  onPesquisaDigitada([String texto]) async {
    List<Venda> vendas = [];
    bool pesquisaDescricao;
    focusNode.unfocus();
    if (textEditingController.text.length < 3) {
      utilService.snackBarErro(
          titulo: 'Dados incompletos',
          mensagem: 'Digite uma descrição completa');
      return;
    }
    contador = 4; //Linha para mostrar o interstitial
    checkInterstitial();
    utilService.showAlertCarregando();
    bool isNumber =
        RegExp(r'^[0-9]+$').hasMatch(texto ?? textEditingController.text);
    if (isNumber) {
      try {
        pesquisaDescricao = false;
        vendas = (estabelecimento == null
                ? await repository.pesquisarPorCodBarras(
                    texto ?? textEditingController.text, locationData)
                : await repository.pesquisarProdutoPorEstabelecimento(
                    texto ?? textEditingController.text, estabelecimento)) ??
            [];
      } catch (e) {
        print('Erro: $e');
        if (Get.isDialogOpen) Get.back();
      }
    } else {
      if (estabelecimento != null) {
        if (Get.isDialogOpen) Get.back();
        utilService.snackBarErro(
            mensagem:
                'Pesquisa pela descrição só é permitida através do código de barras');
        return;
      }
      vendas =
          await pesquisarPelaDescricao(texto ?? textEditingController.text) ??
              [];
      pesquisaDescricao = true;
    }
    if (Get.isDialogOpen) Get.back();
    if (vendas.isEmpty)
      utilService.snackBarErro(
          titulo: 'Sem resultados',
          mensagem:
              'Não foram registradas vendas para esse item nos últimos 3 dias.');
    else
      irParaResultados(vendas, pesquisaDescricao);
  }

  irParaResultados(List<Venda> vendas, [bool pesquisaPorDescricao]) async {
    Produto produto;
    List<Venda> aux = vendas;
    vendas = [];
    digitarCod = digitarDescricao = false;
    limparTextoPesquisa();
    update();
    produto =
        await repository.getProdutoPorCodigoBarras(aux.first.produto.codGetin);
    print('Produto: $produto');
    if (produto != null) {
      aux.forEach((venda) {
        venda.produto = produto;
      });
    }
    /*  produto = await repository
        .getProdutoPorCodigoBarras(vendas.first.produto.codGetin);
    print('produto irParaResultados: $produto'); */
    dynamic result = await Get.toNamed(Routes.RESULTADOS, arguments: {
      'vendas': aux,
      'locationData': locationData,
      'pesquisaPorDescricao': pesquisaPorDescricao ?? false
    });
    print('result Routes.RESULTADOS: $result');
    if (result != null &&
        (result['codBarras'] != null || result['scan'] != null)) {
      scanCodBarras(result['codBarras']);
    }
  }

  irParaListaCompras() async {
    var result;
    if (user == null) {
      await Get.to(() => FazerLoginModal(), fullscreenDialog: true);
    }
    if (result != null || user != null) {
      goToListaCompras();
    }
  }

  irParaListaEstabelecimentos() async {
    var result;
    if (user == null) {
      await Get.to(() => FazerLoginModal(), fullscreenDialog: true);
    }
    if (result != null || user != null) {
      goToEstabelecimentosFavoritos();
    }
  }

  resetTipoPesquisa() {
    digitarCod = digitarDescricao = false;
    textEditingController.text = '';
    update();
  }

  Future<List<Venda>> pesquisarPelaDescricao(String descricao) async {
    List<Venda> vendas = [];
    try {
      vendas = await repository.buscarPorDescricao(descricao, locationData);
      print('vendas descrição: $vendas');
      return vendas;
    } on TimeoutException {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem:
              'Os servidores da SEFAZ demoraram demais para responder. Tente novamente mais tarde.');
    } catch (e) {
      print('Erro: $e');
    }
    return [];
  }

  resetLocation() async {
    //funçao para testes
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _locationData.value = null;
    await prefs.setString('ult_lat', null);
    await prefs.setString('ult_lng', null);
    update();
  }

  resetEstabelecimento() {
    estabelecimento = null;
    update();
  }

  goToPromocoes() async {
    var result = await Get.toNamed(Routes.PROMOCOES,
        arguments: {'locationData': locationData});
    if (result != null &&
        result['codBarras'] != null &&
        result['codBarras'].isNotEmpty) scanCodBarras('7896213005177');
  }

  goToEstabelecimentosFavoritos() async {
    utilService.showAlertCarregando();
    try {
      List<Estabelecimento> favoritos =
          await repository.getEstabelecimentosFavoritos;
      if (favoritos != null /* && favoritos.isNotEmpty */) {
        if (Get.isDialogOpen) Get.back();
        Get.offAllNamed(Routes.ESTABELECIMENTOS_FAVORITOS,
            arguments: {'favoritos': favoritos});
      } else if (favoritos.isEmpty) {
        if (Get.isDialogOpen) Get.back();

        utilService.snackBar(
            titulo: 'Não encontrado',
            mensagem: 'Você não possui estabelecimentos favoritos');
      }
    } catch (e) {
      print('Erro ao consultar favoritos: $e');
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem: 'Erro ao consultar estabelecimentos favoritos');
    }
  }

  goToListaCompras() async {
    utilService.showAlertCarregando();
    try {
      List<ProdutoListaCompras> listaCompras =
          await repository.getListaCompras();
      if (Get.isDialogOpen) Get.back();
      print('favoritos != null && favoritos.isNotEmpty');
      Get.back();
      Get.toNamed(Routes.LISTA_COMPRAS,
          arguments: {'listaCompras': listaCompras});
    } catch (e) {
      print('Erro ao consultar favoritos: $e');
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem: 'Erro ao consultar estabelecimentos favoritos');
    }
  }

  goToCombustiveis() async {
    interstitialCombustiveisAd.show();
    Get.toNamed(Routes.COMBUSTIVEL);
  }

  buscarPrecoCombustivel() {
    print('buscarPrecoCombustivel');
    repository.buscarPrecosCombustivel();
  }

  Future<FirebaseUser> loginComGoogle() {
    try {
      return repository.loginComGoogle();
    } catch (e) {
      print('Cancelou login: $e');
    }
  }

  logout() {
    repository.logout();
    Get.offAllNamed(Routes.INITIAL);
  }
}
