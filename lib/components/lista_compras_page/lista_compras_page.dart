import 'package:aldescontos/components/lista_compras_page/lista_compras_controller.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'custom_widgets/item_lista_compras.dart';

class ListaComprasPage extends StatelessWidget {
  final ListaComprasController controller = Get.find();

  Widget cardAviso() {
    //nao esta retornando o widget porque está apresentando a snackbar com o aviso
    return Container();
    return Card(
      elevation: 10,
      child: Container(
        height: 130,
        padding: EdgeInsets.only(top: 20, left: 10, right: 10),
        child: Stack(
          fit: StackFit.expand,
          children: [
            Column(
              children: [
                Text(
                  "Aqui você pode pesquisar o valor da lista de compras em seus estabelecimentos favoritos.\nUma mão na roda para quem quer economizar!",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.w300, fontSize: 17),
                ),
                TextButton(onPressed: () {}, child: Text('FECHAR AVISO'))
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /* floatingActionButton: FloatingActionButton(
        onPressed: controller.scanCodBarras,
        child: Icon(Ionicons.md_barcode),
      ), */
      appBar: AppBar(
        title: Text('Lista de Compras'),
      ),
      body: Container(
        child: Stack(
          fit: StackFit.expand,
          children: [
            GetBuilder<ListaComprasController>(builder: (_) {
              if (controller.produtos.isEmpty)
                return Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(15, 30, 15, 20),
                      child: Text(
                        "Você não possui nenhum produto na sua lista\nClique no botão abaixo para adicionar",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: controller.scanCodBarras,
                        child: Text('Escanear código de barras'))
                  ],
                );
              return ListView.builder(
                  itemCount: controller.produtos.length + 2,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return Column(
                        children: [cardAviso(), Divider()],
                      );
                    }
                    if (index == controller.produtos.length + 1)
                      return Padding(
                        padding: EdgeInsets.all(30),
                      );
                    return ItemListaCompras(
                      controller: controller,
                      produto: controller.produtos[index - 1],
                    );
                  });
            }),
            Positioned(
                bottom: 10,
                child: Container(
                  alignment: Alignment.center,
                  height: 70,
                  width: Get.width,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: ElevatedButton.icon(
                            style: ButtonStyle(
                                backgroundColor:
                                    utilService.colorButton(Colors.green),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                ))),
                            icon: Icon(
                              Ionicons.md_barcode,
                              size: 20,
                            ),
                            label: Text('Adicionar itens'),
                            onPressed: controller.scanCodBarras,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: ElevatedButton.icon(
                            style: ButtonStyle(
                                backgroundColor:
                                    utilService.colorButton(Colors.red),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                ))),
                            icon: Icon(
                              Ionicons.md_calculator,
                              size: 20,
                            ),
                            label: AutoSizeText(
                              'Calcular valores',
                              maxLines: 1,
                            ),
                            onPressed:
                                controller.openModalSelecionaEstabelecimento,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
