import 'package:aldescontos/components/lista_compras_page/lista_compras_controller.dart';
import 'package:aldescontos/src/data/model/lista_compras_estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResultadoListaCompras extends StatelessWidget {
  final List<ListaComprasEstabelecimento> listaComprasEstabelecimentos;
  final List<ListaComprasEstabelecimento> listaComprasNaoFiltrada;
  final List<ProdutoListaCompras> itensNaoEncontradosEmTodosEstabs;
  final List<ProdutoListaCompras> itensNaoEncontradosEmNenhumEstab;
  ListaComprasController controller = Get.find();
  ResultadoListaCompras(
      {this.listaComprasEstabelecimentos,
      this.listaComprasNaoFiltrada,
      this.itensNaoEncontradosEmTodosEstabs,
      this.itensNaoEncontradosEmNenhumEstab});

  Widget linhaInfoGerais() {
    String dscItensNaoEncontradosEmTodos = '';
    if (itensNaoEncontradosEmTodosEstabs.isEmpty) return Container();
    itensNaoEncontradosEmTodosEstabs.forEach((item) {
      dscItensNaoEncontradosEmTodos += item.dscProduto + '\n';
    });
    String dscItensNaoEncontrados = '';
    itensNaoEncontradosEmNenhumEstab.forEach((item) {
      dscItensNaoEncontrados += item.dscProduto + '\n';
    });
    //dscItensNaoEncontrados.substring(0, dscItensNaoEncontrados.length - 2);

    return GetBuilder<ListaComprasController>(
      builder: (_) {
        if (!_.exibirListaProdutosNaoEncontrados)
          return Container(
            child: TextButton(
                onPressed: _.toggleExibirListaProdutosNaoEncontrados,
                child: Text('EXIBIR PRODUTOS NÃO ENCONTRADOS')),
          );
        return Container(
          margin: EdgeInsets.all(10),
          child: Card(
            child: Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(top: 15),
                            child: Text(
                              !controller.exibirListaFiltrados
                                  ? itensNaoEncontradosEmNenhumEstab.isEmpty
                                      ? ''
                                      : 'Os seguintes produtos não foram encontrados em nenhum estabelecimento selecionado'
                                  : 'Os seguintes produtos não foram encontrados em todos estabelecimentos:',
                              maxLines: 2,
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: _.toggleExibirListaProdutosNaoEncontrados,
                          icon: Icon(
                            Icons.close,
                            size: 18,
                          ),
                        )
                      ],
                    ),
                  ),
                  Text(
                    !controller.exibirListaFiltrados
                        ? dscItensNaoEncontrados
                        : dscItensNaoEncontradosEmTodos,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                  Divider(),
                  Container(
                      child: AutoSizeText(
                    controller.exibirListaFiltrados
                        ? 'EXIBINDO TODOS OS ITENS ENCONTRADOS.'
                        : 'EXIBINDO APENAS ITENS ENCONTRADOS EM TODOS OS ESTABELECIMENTOS',
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  )),
                  ElevatedButton(
                      onPressed: _.toggleExibirListaCompleta,
                      child: Text('ALTERNAR')),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget linhaDescricaoPreco(Venda venda) {
    bool menorPreco;
    bool maiorPreco;
    double auxMenorPreco = double.infinity;
    double auxMaiorPreco = double.negativeInfinity;
    listaComprasNaoFiltrada.forEach((c) {
      c.vendas.forEach((v) {
        if (v.valUnitarioUltimaVenda < auxMenorPreco &&
            venda.produto.codGetin == v.produto.codGetin)
          auxMenorPreco = v.valUnitarioUltimaVenda;
        if (v.valUnitarioUltimaVenda > auxMaiorPreco &&
            venda.produto.codGetin == v.produto.codGetin)
          auxMaiorPreco = v.valUnitarioUltimaVenda;
      });
    });
    menorPreco = venda.valUnitarioUltimaVenda == auxMenorPreco;
    maiorPreco = venda.valUnitarioUltimaVenda == auxMaiorPreco;

    return Container(
      //color: menorPreco ? Colors.green : Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Container(
              child: AutoSizeText(
                '${venda.produto.dscProduto.substring(0, venda.produto.dscProduto.length <= 25 ? venda.produto.dscProduto.length : 25)} ',
                textAlign: TextAlign.right,
                maxLines: 1,
                style: TextStyle(
                    fontWeight: menorPreco || maiorPreco
                        ? FontWeight.bold
                        : FontWeight.normal,
                    fontSize: 16,
                    color: menorPreco
                        ? Colors.blue
                        : maiorPreco
                            ? Colors.red
                            : Colors.black),
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 3, right: 3),
              child: AutoSizeText(
                '--------------------------------------',
                maxLines: 1,
                style: TextStyle(
                    fontWeight: menorPreco || maiorPreco
                        ? FontWeight.bold
                        : FontWeight.normal,
                    fontSize: 16,
                    color: menorPreco
                        ? Colors.blue
                        : maiorPreco
                            ? Colors.red
                            : Colors.black),
              ),
            ),
          ),
          Container(
            child: AutoSizeText(
              'R\$${venda.valUnitarioUltimaVenda.toStringAsFixed(2)}',
              style: TextStyle(
                  fontWeight: menorPreco || maiorPreco
                      ? FontWeight.bold
                      : FontWeight.normal,
                  fontSize: 16,
                  color: menorPreco
                      ? Colors.blue
                      : maiorPreco
                          ? Colors.red
                          : Colors.black),
            ),
          )
        ],
      ),
    );
  }

  Widget tileComprasEstabelecimento(ListaComprasEstabelecimento compras) {
    return GetBuilder<ListaComprasController>(builder: (_) {
      return ExpandablePanel(
          header: ListTile(
            title: AutoSizeText(
              '${compras.estabelecimento.nomFantasia ?? compras.estabelecimento.nomRazaoSocial} - Valor total: R\$${compras.getValorTotal().toStringAsFixed(2)}',
              maxLines: 1,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            subtitle: Text('Itens encontrados: ${compras.vendas.length}'),
          ),
          collapsed: Container(),
          expanded: Container(
            margin: EdgeInsets.only(left: 15, right: 15, bottom: 20, top: 10),
            width: Get.width,
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: compras.vendas.length,
                itemBuilder: (context, index) {
                  Venda venda = compras.vendas[index];
                  return linhaDescricaoPreco(venda);
                }),
          ));
    });
  }

  Widget rowLegenda() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      height: 45,
      width: Get.width,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(bottom: 10),
            child: Text('LEGENDA'),
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 5),
                height: 10,
                width: 10,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
              ),
              Container(
                child: AutoSizeText(
                  'Menor preço encontrado',
                  style: TextStyle(fontSize: 12),
                  maxLines: 1,
                ),
              ),
              Expanded(child: Container()),
              Container(
                height: 10,
                width: 10,
                margin: EdgeInsets.only(right: 5),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.red),
              ),
              Container(
                child: AutoSizeText(
                  'Maior preço encontrado',
                  style: TextStyle(fontSize: 12),
                  maxLines: 1,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

//Lista com produtos encontrados em quaisquer estabelecimentos
  Widget listaResultadosGeral() {
    return ListView.builder(
        itemCount: listaComprasNaoFiltrada.length + 2,
        itemBuilder: (context, index) {
          if (index == 0) {
            return linhaInfoGerais();
          }
          if (index == 1) return rowLegenda();
          ListaComprasEstabelecimento comprasEstabelecimento =
              listaComprasNaoFiltrada[index - 2];
          return tileComprasEstabelecimento(comprasEstabelecimento);
        });
  }

//Lista com produtos encontrados em todos os estabelecimentos
  Widget listaResultadoFiltrado() {
    if (listaComprasEstabelecimentos.isEmpty ||
        listaComprasEstabelecimentos == null)
      return Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Text(
              "Os produtos da sua lista não foram encontrados em todos os estabelecimentos selecionados",
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          Divider(),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Text(
              'Nenhum produto na sua lista de compras foi encontrado em todos os estabelecimentos.\nVocê pode clicar no botão abaixo para mostrar os itens encontrados em quaisquer estabelecimento selecionado.',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 20),
            ),
          ),
          Container(
            child: ElevatedButton(
              child: Text('ALTERNAR'),
              onPressed: () {
                controller.toggleExibirListaCompleta();
              },
            ),
          )
        ],
      );
    return ListView.builder(
        itemCount: listaComprasNaoFiltrada.length + 1,
        itemBuilder: (context, index) {
          if (index == 0) {
            return linhaInfoGerais();
          }
          ListaComprasEstabelecimento comprasEstabelecimento =
              listaComprasEstabelecimentos[index - 1];
          return tileComprasEstabelecimento(comprasEstabelecimento);
        });
  }

  @override
  Widget build(BuildContext context) {
    bool listaVazia = listaComprasEstabelecimentos == null ||
        (listaComprasEstabelecimentos.isEmpty &&
            listaComprasNaoFiltrada.isEmpty);
    print('listaVazia: $listaVazia');
    return Scaffold(
      appBar: AppBar(
        title: Text('Resultados'),
      ),
      body: Container(child: GetBuilder<ListaComprasController>(builder: (_) {
        if (listaVazia)
          return Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  "Os produtos da sua lista não foram encontrados em todos os estabelecimentos selecionados",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              Divider(),
              Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Text(
                  'Para que possamos exibir uma lista precisa, o aplicativo só mostra os itens que foram encontrados em todos os estabelecimentos\n',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.w300, fontSize: 20),
                ),
              ),
              Container(
                child: ElevatedButton(
                  child: Text('VOLTAR'),
                  onPressed: () {
                    Get.back();
                  },
                ),
              )
            ],
          );
        if (!controller.exibirListaFiltrados) {
          return listaResultadosGeral();
        }
        if (controller.exibirListaFiltrados) {
          return listaResultadoFiltrado();
        }
        /* return ListView.builder(
            itemCount: listaVazia ? 1 : listaComprasNaoFiltrada.length + 1,
            itemBuilder: (context, index) {
              if (listaVazia) {
                return Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Text(
                        "Os produtos da sua lista não foram encontrados em todos os estabelecimentos selecionados",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    Divider(),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Text(
                        'Para que possamos exibir uma lista precisa, o aplicativo só mostra os itens que foram encontrados em todos os estabelecimentos\n',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 20),
                      ),
                    ),
                    Container(
                      child: ElevatedButton(
                        child: Text('VOLTAR'),
                        onPressed: () {
                          Get.back();
                        },
                      ),
                    )
                  ],
                );
              }
              if (index == 0) {
                return linhaInfoGerais();
              }
              ListaComprasEstabelecimento comprasEstabelecimento =
                  controller.exibirListaFiltrados
                      ? listaComprasNaoFiltrada[index - 1]
                      : listaComprasEstabelecimentos[index - 1];

              return tileComprasEstabelecimento(comprasEstabelecimento);
            }); */
      })),
    );
  }
}
