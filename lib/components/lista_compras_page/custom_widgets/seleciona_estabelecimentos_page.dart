import 'package:aldescontos/components/lista_compras_page/lista_compras_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class SelecionaEstabelecimentoModal extends StatelessWidget {
  final ListaComprasController controller;
  SelecionaEstabelecimentoModal(this.controller);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Selecione os estabelecimentos'),
      ),
      body: Container(
        child: Stack(
          fit: StackFit.expand,
          children: [
            ListView.builder(
                itemCount: controller.estabelecimentosFavoritos.length,
                itemBuilder: (context, index) {
                  return GetBuilder<ListaComprasController>(
                    init: ListaComprasController(),
                    builder: (_) {
                      return CheckboxListTile(
                        value: controller.checkboxSelecionados[index],
                        title: Text(controller
                                .estabelecimentosFavoritos[index].nomFantasia ??
                            controller.estabelecimentosFavoritos[index]
                                .nomRazaoSocial),
                        onChanged: (value) {
                          controller.selectEstabelecimento(
                              controller.estabelecimentosFavoritos[index],
                              value);
                        },
                      );
                    },
                  );
                }),
            Positioned(
                bottom: 10,
                child: Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  width: Get.width,
                  height: 50,
                  child: GetBuilder<ListaComprasController>(
                    builder: (_) {
                      return ElevatedButton.icon(
                        onPressed:
                            controller.estabelecimentosSelecionados.isEmpty
                                ? null
                                : controller.calcularPrecoListaCompras,
                        icon: Icon(Ionicons.md_calculator),
                        label: Text(
                            controller.estabelecimentosSelecionados.isEmpty
                                ? 'SELECIONE ALGUM ESTABELECIMENTO'
                                : 'REALIZAR PESQUISA'),
                      );
                    },
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
