import 'package:aldescontos/components/lista_compras_page/lista_compras_controller.dart';
import 'package:aldescontos/components/resultados_page/resultados_controller.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:location/location.dart';
import 'package:maps_toolkit/maps_toolkit.dart';

class ItemListaCompras extends StatelessWidget {
  double distanceBetweenPoints;
  final ListaComprasController controller;
  final ProdutoListaCompras produto;

  ItemListaCompras({@required this.produto, @required this.controller});

  Widget pesquisaPorCodigo() {
    bool semCodigo = produto.codGetin == null ||
        produto.codGetin.isEmpty ||
        produto.codGetin.toUpperCase().contains('SEM');
    return GetBuilder<ListaComprasController>(builder: (_) {
      return Container(
          height: 40,
          //margin: EdgeInsets.only(top: 2),
          child: TextButton.icon(
            onPressed: () {
              controller.scanCodBarras(produto.codGetin, true);
            },
            label: AutoSizeText(
              'Pesquisar em todos estabelecimentos',
              maxLines: 1,
              minFontSize: 8,
            ),
            icon: Icon(
              Icons.search,
              color: Colors.blue,
            ),
          ));
    });
  }

  Widget botaoExcluir() {
    return Container(
        //margin: EdgeInsets.only(top: 2),
        height: 40,
        child: TextButton.icon(
          onPressed: () {
            controller.removerProduto(produto.codGetin);
          },
          label: AutoSizeText(
            'Excluir da lista',
            maxLines: 1,
            minFontSize: 8,
            style: TextStyle(color: Colors.red),
          ),
          icon: Icon(
            Icons.delete,
            color: Colors.red,
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.only(bottom: 13, top: 12),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15),
              height: 50,
              color: Colors.blue,
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: AutoSizeText(
                '${produto.dscProduto}',
                minFontSize: 15,
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(top: 4, bottom: 4),
              width: double.infinity,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Última venda',
                    style: TextStyle(fontSize: 18),
                  ),
                  Padding(padding: EdgeInsets.all(1)),
                  Text('R\$${produto.valUltimaVenda.toStringAsFixed(2)}',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Padding(padding: EdgeInsets.all(1)),
                  Text(
                    'Data da consulta: ${produto.dataFormatada}',
                    style: TextStyle(fontSize: 14),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text(
                      'Estabelecimento: ${produto.estabelecimento}',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  pesquisaPorCodigo(),
                  botaoExcluir()
                ],
              ),
            ),

            /* Container(
              padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
              //height: 85,
              color: Colors.blue[200],
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          AutoSizeText(
                            '${produto.estabelecimento ?? produto.estabelecimento}',
                            minFontSize: 11,
                            style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.w300,
                                color: Colors.black),
                          ),
                          Text(
                              'Distância: ${(distanceBetweenPoints / 1000).toStringAsFixed(2)} KM',
                              style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black)),
                          AutoSizeText(
                            '${venda.estabelecimento.nomLogradouro}, ${venda.estabelecimento.numImovel ?? ''} - ${venda.estabelecimento.numCep}',
                            minFontSize: 11,
                            style: TextStyle(
                                fontSize: 11,
                                fontWeight: FontWeight.w300,
                                color: Colors.black),
                          ),
                          if (venda.estabelecimento.nomBairro != null)
                            AutoSizeText(
                              '${venda.estabelecimento.nomBairro}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                          if (venda.estabelecimento.numTelefone != null)
                            AutoSizeText(
                              '${venda.estabelecimento.numTelefone}',
                              minFontSize: 11,
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                        ],
                      ),
                    ),
                  ),
                  IconButton(
                      icon: Icon(Octicons.location),
                      onPressed: () {
                        controller.verNoMapa(venda);
                      }),
                  GetBuilder<ResultadosController>(
                    builder: (_) {
                      return IconButton(
                          icon: Icon(_.checkEstabelecimentoFavorito(
                                  venda.estabelecimento)
                              ? Icons.favorite
                              : Icons.favorite_border),
                          onPressed: _.favoritandoEstabelecimento
                              ? null
                              : () async {
                                  controller.favoritarEstabelecimento(
                                      venda.estabelecimento);
                                });
                    },
                  )
                ],
              ),
            ), */
          ],
        ),
      ),
    );
  }
}
