import 'package:get/get.dart';
import 'lista_compras_controller.dart';

class ListaComprasBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ListaComprasController>(() => ListaComprasController());
  }
}
