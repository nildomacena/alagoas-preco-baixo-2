import 'dart:async';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:aldescontos/components/lista_compras_page/custom_widgets/resultado_lista_page.dart';
import 'package:aldescontos/components/resultados_page/resultados_controller.dart';
import 'package:aldescontos/components/resultados_page/resultados_page.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/lista_compras_estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'lista_compras_repository.dart';
import 'custom_widgets/seleciona_estabelecimentos_page.dart';

class ListaComprasController extends GetxController {
  final ListaComprasRepository repository = Get.put(ListaComprasRepository(
      apiProvider: Get.find(), userProvider: Get.find()));
  List<ProdutoListaCompras> produtos = [];
  List<Estabelecimento> estabelecimentosFavoritos = [];
  List<Estabelecimento> estabelecimentosSelecionados = [];
  List<bool> checkboxSelecionados = [];
  bool exibirListaProdutosNaoEncontrados = true;
  bool exibirListaFiltrados = false;
  AdmobInterstitial interstitialAd = AdmobInterstitial(
    adUnitId: utilService.interstitialListaComprasId,
  );

  @override
  onInit() {
    super.onInit();
    if (Get.arguments['listaCompras'] != null) {
      produtos = Get.arguments['listaCompras'];
    }
    interstitialAd.load();
  }

  @override
  onReady() {
    Get.snackbar('ATENÇÃO',
        'Aqui você pode pesquisar o valor da lista de compras em seus estabelecimentos favoritos. Uma mão na roda para quem quer economizar',
        snackPosition: SnackPosition.TOP,
        //colorText: Colors.white,
        margin: EdgeInsets.only(top: 30, left: 5, right: 5),
        duration: Duration(seconds: 5),
        isDismissible: true,
        backgroundColor: Colors.white,
        mainButton: TextButton(
            onPressed: () {
              if (Get.isSnackbarOpen) Get.back();
            },
            child: Text('FECHAR')));
  }

  scanCodBarrasbkp([String cod, bool pesquisaNosEstabelecimentos]) async {
    cod = '7896496917716';
    String codigo = cod;
    List<Venda> vendas = [];
    Get.toNamed(Routes.RESULTADOS,
        arguments: {'vendas': <Venda>[], 'codBarras': cod});
    return;
    try {
      vendas = await repository.pesquisarPorCodBarras(codigo);
      Get.toNamed(Routes.RESULTADOS,
          arguments: {'vendas': vendas, 'codBarras': cod});
    } on RangeError {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          titulo: 'Sem resultados',
          mensagem:
              'Não foram registradas vendas para esse item nos últimos 3 dias.');
    } on FormatException {
      print('Cancelou leitura');
      if (Get.isDialogOpen) Get.back();
      return;
    } catch (e) {
      print('Erro ao escanear: $e');
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem: 'Erro ao escanear o codigo\nTente novamente mais tarde');
    }
  }

  scanCodBarras([String cod, bool pesquisaNosEstabelecimentos]) async {
    if (kDebugMode) cod = '7896496917716';
    String codigo = cod;
    List<Venda> vendas = [];

    try {
      if (codigo == null) codigo = await utilService.scanBarCode(); //Testes
      utilService.showAlertCarregando();

      //Se possui estabelecimento, pesquisar por estabelecimento
      vendas = await repository.pesquisarPorCodBarras(codigo);
      if (Get.isDialogOpen) Get.back();

      if (pesquisaNosEstabelecimentos != null &&
          pesquisaNosEstabelecimentos &&
          vendas.isNotEmpty) {
        dynamic result = await Get.toNamed(Routes.RESULTADOS, arguments: {
          'vendas': vendas,
          'codBarras': vendas.first.produto.codGetin,
          'listaCompras': produtos
        });
        print('Result Routes.RESULTADOS: $result');
        if (result != null && result['listaCompras'] != null) {
          produtos = result['listaCompras'];
        }
        return;
      } else if (vendas.isEmpty) {
        utilService.showAlert('Sem resultados',
            'Não foram registradas vendas para esse item nos últimos 3 dias.');
        return;
      } else {
        //Ordena a lista para pegar o mais barato
        vendas.sort((a, b) => a.valUltimaVenda.compareTo(b.valUltimaVenda));
        bool confirma = await utilService.showAlert('Confirmação',
            'Deseja realmente inserir o item ${vendas.first.produto.dscProduto}?',
            actionLabel: 'INSERIR', action: () {
          Get.back(result: true);
        });
        print('confirma $confirma');
        if (confirma != null && confirma) {
          utilService.showAlertCarregando();
          produtos = await repository.salvarProdutoListaCompras(vendas.first);
          update();
          if (Get.isDialogOpen) Get.back();
          print('Produtos: $produtos');
        }
      }
    } on TimeoutException {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          titulo: 'Tempo excedido',
          mensagem:
              'Houve algum problema durante a consulta. Tente novamente mais tarde.');
    } on RangeError {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          titulo: 'Sem resultados',
          mensagem:
              'Não foram registradas vendas para esse item nos últimos 3 dias.');
    } on FormatException {
      print('Cancelou leitura');
      if (Get.isDialogOpen) Get.back();
      return;
    } catch (e) {
      print('Erro ao escanear: $e');
      if (Get.isDialogOpen) Get.back();
      if (e == 'produto-duplicado') {
        utilService.snackBarErro(
            mensagem: 'Esse produto já consta em sua lista de compras');
      } else if (e == 'limite-excedido') {
        utilService.snackBarErro(
            mensagem:
                'Você atingiu o limite de 30 produtos a serem salvos na lista de compras');
      } else if (e == 'nao-autenticado')
        utilService.snackBarErro(
            mensagem:
                'É preciso fazer o login para utilizar essa funcionalidade');
      else
        utilService.snackBarErro(
            mensagem: 'Erro ao escanear o codigo\nTente novamente mais tarde');
    }
  }

  toggleExibirListaProdutosNaoEncontrados() {
    exibirListaProdutosNaoEncontrados = !exibirListaProdutosNaoEncontrados;
    print(
        'exibirListaProdutosNaoEncontrados $exibirListaProdutosNaoEncontrados');
    update();
  }

  removerProduto(String codGetin) async {
    var result = await Get.dialog(AlertDialog(
      title: Text('Confirmação'),
      content: Text('Deseja realmente excluir o item do carrinho?'),
      actions: [
        TextButton(
          child: Text('CANCELAR'),
          onPressed: () {
            Get.back();
          },
        ),
        TextButton(
          child: Text('EXCLUIR'),
          onPressed: () {
            Get.back(result: {'excluir': true});
          },
        )
      ],
    ));
    if (result != null && result['excluir'])
      try {
        produtos = await repository.removerProdutoListaCompras(codGetin);
        utilService.snackBar(titulo: 'Removido', mensagem: 'Produto removido');
        update();
      } catch (e) {
        utilService.snackBarErro(mensagem: 'Erro durante a operação');
        print('Erro: $e');
      }
  }

  adicionarItemListaCompras(Venda venda) {}

  openModalSelecionaEstabelecimento() async {
    estabelecimentosFavoritos = await repository.getEstabelecimentosFavoritos();
    checkboxSelecionados = estabelecimentosFavoritos.map((e) => false).toList();
    estabelecimentosSelecionados = [];
    print('estabelecimentosFavoritos: $estabelecimentosFavoritos');
    if (estabelecimentosFavoritos == null ||
        estabelecimentosFavoritos.isEmpty) {
      utilService.snackBarErro(
          mensagem:
              'Você não possui nenhum estabelecimento favorito. Pesquise seus produtos e salve seus estabelecimentos preferidos');
      return;
    }
    dynamic result = await Get.to(
        () => SelecionaEstabelecimentoModal(Get.find()),
        fullscreenDialog: true);
    print('result openModalSelecionaEstabelecimento: $result');
    update();
  }

  selectEstabelecimento(Estabelecimento estabelecimento, bool value) {
    int indexOf = estabelecimentosSelecionados.indexOf(estabelecimento);
    if (!value) {
      estabelecimentosSelecionados.removeAt(indexOf);
    } else
      estabelecimentosSelecionados.add(estabelecimento);
    checkboxSelecionados[estabelecimentosFavoritos.indexOf(estabelecimento)] =
        value;
    update();
  }

  bool checkEstabelecimentoSelecionado(Estabelecimento estabelecimento) {
    return estabelecimentosSelecionados.indexOf(estabelecimento) >= 0;
  }

  realizarPesquisa() {
    print('estabelecimentosSelecionados: $estabelecimentosSelecionados');
  }

  calcularPrecoListaCompras() async {
    List<ListaComprasEstabelecimento> listaComprasEstabelecimentos;
    List<ListaComprasEstabelecimento> listaComprasNaoFiltrada;
    List<ProdutoListaCompras> produtosNaoEncontradosEmTodos;
    List<ProdutoListaCompras> produtosNaoEncontrados = [];
    print('calcularPrecoListaCompras');
    try {
      //interstitialAd.show();
      Map result = await repository
          .calcularPrecoListaCompras(estabelecimentosSelecionados);
      listaComprasEstabelecimentos = result['listaComprasEstabelecimentos'];
      produtosNaoEncontradosEmTodos = result['produtosNaoEncontradosEmTodos'];
      listaComprasNaoFiltrada = result['listaComprasNaoFiltrada'];
      produtosNaoEncontrados = result['produtosNaoEncontrados'];

      print('result listaComprasNaoFiltrada: $listaComprasNaoFiltrada');
      print(
          'result listaComprasEstabelecimentos: $listaComprasEstabelecimentos');
      if (produtosNaoEncontradosEmTodos.length == produtos.length)
        listaComprasEstabelecimentos = [];
      Get.back();
      Get.to(
          () => ResultadoListaCompras(
              listaComprasEstabelecimentos: listaComprasEstabelecimentos,
              itensNaoEncontradosEmTodosEstabs: produtosNaoEncontradosEmTodos,
              itensNaoEncontradosEmNenhumEstab: produtosNaoEncontrados,
              listaComprasNaoFiltrada: listaComprasNaoFiltrada),
          fullscreenDialog: true);
    } catch (e) {
      if (Get.isDialogOpen) Get.back();
      utilService.snackBarErro(
          mensagem:
              'Erro durante a consulta. Os servidores da SEFAZ não responderam. Tente novamente mais tarde mais tarde.');
      print('Erro lista de compras $e');
    }
  }

  toggleExibirListaCompleta() {
    exibirListaFiltrados = !exibirListaFiltrados;
    print('exibirListaFiltrados $exibirListaFiltrados');
    update();
  }
}
