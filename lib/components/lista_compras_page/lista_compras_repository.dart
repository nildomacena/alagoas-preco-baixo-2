import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/lista_compras_estabelecimento.model.dart';
import 'package:aldescontos/src/data/model/produto_lista_compras.model.dart';
import 'package:aldescontos/src/data/model/venda.model.dart';
import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:aldescontos/src/data/provider/user.provider.dart';
import 'package:location/location.dart';
import 'package:meta/meta.dart';

class ListaComprasRepository {
  final ApiProvider apiProvider;
  final UserProvider userProvider;

  ListaComprasRepository(
      {@required this.apiProvider, @required this.userProvider})
      : assert(apiProvider != null && userProvider != null);

  Future<List<Venda>> pesquisarPorCodBarras(String cod,
      [LocationData locationData]) {
    return apiProvider.pesquisarProdutoPorCodigoBarras(cod, locationData);
  }

  Future<List<ProdutoListaCompras>> salvarProdutoListaCompras(Venda venda) {
    return apiProvider.salvarProdutoListaCompras(venda);
  }

  Future<Map<String, dynamic>> calcularPrecoListaCompras(
      List<Estabelecimento> estabelecimentos) async {
    Map result = await apiProvider.calcularPrecoListaCompras(estabelecimentos);
    return result;
  }

  Future<List<ProdutoListaCompras>> removerProdutoListaCompras(
      String codGetin) {
    return apiProvider.removerProdutoListaCompras(codGetin);
  }

  Future<List<Estabelecimento>> getEstabelecimentosFavoritos() {
    return userProvider.getEstabelecimentosFavoritosAsync();
  }
}
