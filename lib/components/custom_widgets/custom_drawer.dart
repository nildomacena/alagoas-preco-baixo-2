import 'package:aldescontos/components/home_page/home_controller.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:aldescontos/src/data/model/estabelecimento.model.dart';
import 'package:aldescontos/src/data/services/util.service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:auth_buttons/auth_buttons.dart';

class CustomDrawer extends StatelessWidget {
  final HomeController controller;
  CustomDrawer({@required this.controller}) : assert(controller != null);

  Widget headerLogin() {
    return Container(
      height: 180,
      child: DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.blue,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Faça login para salvar seus dados',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Colors.white),
              ),
              GoogleAuthButton(
                darkMode: false,
                height: 40,
                width: 240,
                text: "Login com Google",
                onPressed: () async {
                  controller.loginComGoogle();
                  /*  try {
                                  var onUser =
                                      await firebaseService.googleSignIn();
                                  Navigator.of(context).pop();

                                  if (onUser != null)
                                    setState(() {
                                      user = onUser;
                                    });
                                } catch (e) {
                                  print('Erro: $e');
                                } */
                },
              )
            ],
          )),
    );
  }

  Widget headerUser() {
    return Container(
      width: 500,
      height: 180,
      child: Stack(
        fit: StackFit.expand,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(controller.user.displayName),
            accountEmail: Text(controller.user.email),
            currentAccountPicture: CircleAvatar(
              backgroundImage: NetworkImage(controller.user.photoUrl),
            ),
          ),
          Positioned(
            bottom: 20,
            right: 20,
            child: IconButton(
                icon: Icon(
                  Icons.exit_to_app,
                  color: Colors.white,
                  size: 20,
                ),
                onPressed: () async {
                  await controller.logout();
                  Get.back();
                }),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Obx(() {
        return Container(
          child: Stack(
            fit: StackFit.expand,
            children: [
              ListView(
                padding: EdgeInsets.zero,
                children: [
                  if (controller.user == null) headerLogin(),
                  if (controller.user != null) headerUser(),
                  ListTile(
                    title: Text('Pesquisa por descrição'),
                    onTap: () {
                      Get.back();
                      controller.toggleDigitar(true);
                    },
                  ),
                  ListTile(
                    title: Text('Promoções'),
                    onTap: controller.goToPromocoes,
                  ),
                  if (controller.user != null)
                    ListTile(
                      title: Text('Estabelecimentos Favoritos'),
                      onTap: controller.goToEstabelecimentosFavoritos,
                    ),
                  if (controller.user !=
                      null) // retirar o kReleaseMode quando acabar os testes
                    ListTile(
                      title: Text('Lista de Compras'),
                      onTap: controller.goToListaCompras,
                    ),
                  if (controller.user != null)
                    ListTile(
                      title: Text('Produtos consultados por você'),
                      onTap: () {
                        Get.back();
                        Get.toNamed(Routes.PRODUTOS_PESQUISADOS);
                      },
                    ),
                  ListTile(
                    title: Text('Sobre o app'),
                    onTap: () {
                      Get.back();
                      Get.toNamed(Routes.SOBRE);
                      /* Navigator.pop(context);
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => SobrePage())); */
                    },
                  ),
                ],
              )
            ],
          ),
        );

        if (controller.user == null)
          return Container(
            child: Stack(
              children: <Widget>[
                ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    DrawerHeader(
                      decoration: BoxDecoration(
                        color: Colors.blue,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Faça login para salvar seus dados',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          GoogleAuthButton(
                            darkMode: false,
                            height: 40,
                            width: 240,
                            text: "Login com Google",
                            onPressed: () async {
                              /*  try {
                                var onUser =
                                    await firebaseService.googleSignIn();
                                Navigator.of(context).pop();

                                if (onUser != null)
                                  setState(() {
                                    user = onUser;
                                  });
                              } catch (e) {
                                print('Erro: $e');
                              } */
                            },
                          )
                        ],
                      ),
                    ),
                    ListTile(
                      title: Text('Pesquisa por descrição'),
                      onTap: () {
                        /*   Navigator.pop(context);
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                PesquisaDescricaoPage())); */
                      },
                    ),
                    ListTile(
                      title: Text('Sobre o app'),
                      onTap: () {
                        /* Navigator.pop(context);
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => SobrePage())); */
                      },
                    ),
                    /* ListTile(
                            title: Text('Item 1'),
                            onTap: () {
                              // Update the state of the app.
                              // ...
                            },
                          ),
                          ListTile(
                            title: Text('Item 2'),
                            onTap: () {
                              // Update the state of the app.
                              // ...
                            },
                          ), */
                  ],
                ),
                Positioned(
                  bottom: 15,
                  right: 15,
                  child: TextButton(
                      onPressed: () {},
                      child: Text('Entre em contato com o desenvolvedor')),
                ),
              ],
            ),
          );
        return ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Stack(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text(controller.user.displayName),
                  accountEmail: Text(controller.user.email),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: NetworkImage(controller.user.photoUrl),
                  ),
                ),
                Positioned(
                  bottom: 20,
                  right: 20,
                  child: IconButton(
                      icon: Icon(
                        Icons.exit_to_app,
                        color: Colors.white,
                        size: 20,
                      ),
                      onPressed: () async {
                        await controller.logout();
                        Get.back();
                      }),
                ),
              ],
            ),
            ListTile(
              title: Text('Estabelecimentos Favoritos'),
              onTap: () {
                /* Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        EstabelecimentosFavoritos(snapshot.data))); */
              },
            ),
            ListTile(
              title: Text('Produtos consultados por você'),
              onTap: () {
                /*  Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        ProdutosConsultadosPage())); */
              },
            ),
            ListTile(
              title: Text('Pesquisa por descrição'),
              onTap: () {
                /*  Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        PesquisaDescricaoPage())); */
              },
            ),
            ListTile(
              title: Text('Sobre o app'),
              onTap: () {
                /*  Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => SobrePage())); */
              },
            ),
            ListTile(
              title: Text('Contato com o Desenvolvedor'),
              onTap: () {},
            ),
          ],
        );
      }),
    );
  }
}
