import 'package:admob_flutter/admob_flutter.dart';
import 'package:aldescontos/bindings/initial_binding.dart';
import 'package:aldescontos/components/home_page/home_page.dart';
import 'package:aldescontos/routes/app_pages.dart';
import 'package:aldescontos/routes/app_routes.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  // Initialize without device test ids.
  await InitialBinding().dependencies();
  Admob.initialize(testDeviceIds: ['6ba86225-3191-4cd3-bf05-9c89e41ee93b']);
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  if (!kReleaseMode) await _firebaseMessaging.subscribeToTopic('teste');
  _firebaseMessaging.getToken().then((value) => print('token: $value'));
  /* AwesomeNotifications().initialize('resource://drawable/res_app_icon', [
    NotificationChannel(
        channelKey: 'basic_channel',
        channelName: 'Basic notifications',
        channelDescription: 'Notification channel for basic tests',
        defaultColor: Color(0xFF9D50DD),
        ledColor: Colors.white)
  ]);
  await Future.delayed(Duration(seconds: 1));
  _firebaseMessaging.configure(
      onBackgroundMessage: _firebaseMessagingBackgroundHandler); */
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: InitialBinding(),
      initialRoute: Routes.INITIAL,
      getPages: AppPages.routes,
      title: 'Alagoas Preço Baixo ',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  SplashScreen() {
    Future.delayed(Duration(seconds: 2))
        .then((value) => Get.off(() => HomePage()));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Alagoas Preço Baixo'),
            Center(
              child: CircularProgressIndicator(),
            )
          ],
        ),
      ),
    );
  }
}
