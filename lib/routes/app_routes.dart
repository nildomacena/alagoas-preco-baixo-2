abstract class Routes {
  static const INITIAL = '/';
  static const CONSULTA_ITENS = '/consulta_itens';
  static const ITENS_CONSULTADOS = '/itens_consultados';
  static const RESULTADOS = '/resultados';
  static const PESQUISA_DESCRICAO = '/pesquisa_descricao';
  static const PRODUTOS_PESQUISADOS = '/produtos_pesquisados';
  static const ESTABELECIMENTOS_FAVORITOS = '/estabelecimentos_favoritos';
  static const PESQUISA_ESTABELECIMENTOS = '/pesquisa_estabelecimentos';
  static const SOBRE = '/sobre';
  static const LISTA_COMPRAS = '/lista_compras';
  static const COMBUSTIVEL = '/combustivel';
  static const PROMOCOES = '/promocoes';
}
