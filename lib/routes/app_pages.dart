import 'package:aldescontos/components/consulta_combustivel_page/consulta_combustivel_binding.dart';
import 'package:aldescontos/components/consulta_combustivel_page/consulta_combustivel_page.dart';
import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_binding.dart';
import 'package:aldescontos/components/estabelecimentos_favoritos_page/estabelecimentos_favoritos_page.dart';
import 'package:aldescontos/components/home_page/home_binding.dart';
import 'package:aldescontos/components/home_page/home_page.dart';
import 'package:aldescontos/components/produtos_pesquisados_page/produtos_pesquisados_binding.dart';
import 'package:aldescontos/components/produtos_pesquisados_page/produtos_pesquisados_page.dart';
import 'package:aldescontos/components/promocoes/promocoes_binding.dart';
import 'package:aldescontos/components/promocoes/promocoes_page.dart';
import 'package:aldescontos/components/resultados_page/resultados_binding.dart';
import 'package:aldescontos/components/resultados_page/resultados_page.dart';
import 'package:aldescontos/components/lista_compras_page/lista_compras_page.dart';
import 'package:aldescontos/components/lista_compras_page/lista_compras_binding.dart';

import 'package:aldescontos/routes/app_routes.dart';
import 'package:get/get.dart';

class AppPages {
  static final routes = [
    GetPage(
        name: Routes.INITIAL, page: () => HomePage(), binding: HomeBinding()),
    (GetPage(
        name: Routes.RESULTADOS,
        page: () => ResultadosPage(),
        binding: ResultadosBinding())),
    GetPage(
        name: Routes.PRODUTOS_PESQUISADOS,
        page: () => ProdutosPesquisadosPage(Get.find()),
        binding: ProdutosPesquisadosBinding()),
    GetPage(
        name: Routes.ESTABELECIMENTOS_FAVORITOS,
        page: () => EstabelecimentosFavoritosPage(),
        binding: EstabelecimentosFavoritosBinding()),
    GetPage(
        name: Routes.LISTA_COMPRAS,
        page: () => ListaComprasPage(),
        binding: ListaComprasBinding()),
    GetPage(
        name: Routes.COMBUSTIVEL,
        page: () => ConsultaCombustivelPage(),
        binding: ConsultaCombustiveisBinding()),
    GetPage(
        name: Routes.PROMOCOES,
        page: () => PromocoesPage(),
        binding: PromocoesBinding()),
    /*GetPage(
        name: Routes.PESQUISA_DIGITAR_CODBARRAS,
        page: () => PesquisaDigitarCodPage(Get.find()),
        binding: PesquisaDigitarCodBarrasBinding()),
    
    GetPage(
        name: Routes.PESQUISA_DESCRICAO,
        page: () => PesquisaDescricaoPage(controller: Get.find()),
        binding: PesquisaDescricaoBinding()),
    GetPage(name: Routes.SOBRE, page: () => SobrePage()), */
  ];
}
