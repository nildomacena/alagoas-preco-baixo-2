import 'package:aldescontos/src/data/provider/api.provider.dart';
import 'package:aldescontos/src/data/provider/user.provider.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InitialBinding implements Bindings {
  @override
  Future<void> dependencies() async {
    Get.put(ApiProvider(), permanent: true);
    Get.put(UserProvider(), permanent: true);
  }
}
